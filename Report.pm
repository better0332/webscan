package Report;
use strict;
use threads;
use threads::shared;
use bytes;#can't remove!!
use web;
use XML::Simple;

sub new
{
	my $type = shift;
	my $class = ref($type) || $type;
	my $this = {};
	bless($this, $class);
	$this->initialize(@_);
	return $this;
}

sub initialize
{
	my $this = shift;
	my %param = @_;

	my $xml = new XML::Simple();
	$this->{define} = $xml->XMLin("./define.xml", keyattr=>"id");
	$this->setReportXSL($param{XSL});
	$this->{scan_info} = {};
	$this->{server_info} = {};
	$this->{issue} = shared_clone({});
	$this->{tested} = shared_clone({});
}

sub setReportXSL
{
	my $this = shift;
	my $xsl = shift;
	$this->{xsl} = defined($xsl)?$xsl:'report.xsl';
}

sub print_report
{
	my $this = shift;
	my $file = shift;
	my $type = shift;
	open(my $fp, '>', $file) or die "Can not create file: $file\n";
	print $fp "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	print $fp "<?xml-stylesheet type=\"text/xsl\" href=\"$this->{xsl}\"?>\n";
	print $fp "<report title=\"海康威视网站漏洞扫描报告\">\n";
	$this->print_information($fp);
	{lock $this->{issue};
	$this->print_summary($fp);
	$this->print_details($fp, $type);
	}
	print $fp "</report>\n";
	close($fp);
}

sub print_information
{
	my $this = shift;
	my $fp = shift;
	print $fp "<information title=\"基本信息\">\n";
	print $fp "<scan_info title=\"扫描信息\">\n";
	print $fp "<item title=\"扫描网址\">".text2html($this->{scan_info}{website})."</item>\n";
	print $fp "<item title=\"指纹检测\">".text2html($this->{scan_info}{cms})."</item>\n" if ($this->{scan_info}{cms} ne '');
	print $fp "<item title=\"开始时间\">".text2html($this->{scan_info}{start_time})."</item>\n";
	print $fp "<item title=\"结束时间\">".text2html($this->{scan_info}{end_time})."</item>\n";
	print $fp "<item title=\"扫描用时\">".text2html($this->{scan_info}{scan_time})."</item>\n";
	print $fp "</scan_info>\n";
	print $fp "<server_info title=\"服务器信息\">\n";
	print $fp "<item title=\"主机域名\">".text2html($this->{server_info}{hostname})."</item>\n";
	print $fp "<item title=\"主机IP\">".text2html($this->{server_info}{ip})."</item>\n";
	print $fp "<item title=\"服务器版本\">".text2html($this->{server_info}{server_verion})."</item>\n";
	print $fp "<item title=\"脚本引擎\">".text2html($this->{server_info}{script_engine})."</item>\n";
	print $fp "</server_info>\n";
	print $fp "</information>\n";
}

sub print_summary
{
	my $this = shift;
	my $fp = shift;
	my ($count, $high, $medium, $low) = (0, 0, 0, 0);
	my $issue_stat = "<issue_stat title=\"网站漏洞类型统计\">\n";
	foreach my $id (reverse sort keys(%{$this->{issue}}))
	{
		$count = keys(%{$this->{issue}{$id}});
		my $name = $this->{define}{issue_def}{issue}{$id}{name};
		my $risk_id = substr($id, 0, 1);
		my $risk_name = '';
		if ($risk_id eq '3')
		{
			$high += $count;
			$risk_name = "高";
		}elsif ($risk_id eq '2')
		{
			$medium += $count;
			$risk_name = "中";
		}elsif ($risk_id eq '1')
		{
			$low += $count;
			$risk_name = "低";
		}
		$issue_stat .= "<item title=\"$name\" risk=\"$risk_name\">$count</item>\n";
	}
	$issue_stat .= "</issue_stat>\n";

	my $risk_stat = "<risk_stat title=\"网站安全风险统计\">\n";
	$risk_stat .= "<item title=\"高风险\">$high</item>\n";
	$risk_stat .= "<item title=\"中风险\">$medium</item>\n";
	$risk_stat .= "<item title=\"低风险\">$low</item>\n";
	$risk_stat .= "</risk_stat>\n";

	my $server_risk_id = ($high>0)?3:(($medium>0)?2:1);
	my $server_risk_name = $this->{define}{server_risk_def}{server_risk}{$server_risk_id}{name};
	my $server_risk_description = $this->{define}{server_risk_def}{server_risk}{$server_risk_id}{description};
	my $evaluation = "<evaluation title=\"网站安全风险评估\">\n";
	$evaluation .= "<risk title=\"风险级别\">".text2html($server_risk_name)."</risk>\n";
	$evaluation .= "<description title=\"安全建议\">".text2html($server_risk_description)."</description>\n";
	$evaluation .= "</evaluation>\n";

	print $fp "<summary title=\"报告摘要\">\n";
	print $fp $evaluation;
	print $fp $risk_stat;
	print $fp $issue_stat;
	print $fp "</summary>\n";
}

sub print_details
{
	my $this = shift;
	my $fp = shift;
	my $type = shift;
	print $fp "<details title=\"详细报告\">\n";
	foreach my $id (reverse sort keys(%{$this->{issue}}))
	{
		my $name = $this->{define}{issue_def}{issue}{$id}{name};

		print $fp "<issue title=\"安全漏洞：".text2html($name)."\">\n";
		my $description = $this->{define}{issue_def}{issue}{$id}{description};
		my $impact = $this->{define}{issue_def}{issue}{$id}{impact};
		my $recommendation = $this->{define}{issue_def}{issue}{$id}{recommendation};
		my $risk_id = substr($id, 0, 1);
		my $risk_name = ($risk_id eq '3')?'高':(($risk_id eq '2')?'中':'低');
		
		my $introduction = "<introduction title=\"漏洞资料\">\n";
		$introduction .= "<item title=\"名称\">".text2html($name)."</item>\n";
		$introduction .= "<item title=\"风险\">".text2html($risk_name)."</item>\n";
		$introduction .= "<item title=\"描述\">".text2html($description)."</item>\n";
		$introduction .= "<item title=\"影响\">".text2html($impact)."</item>\n";
		$introduction .= "<item title=\"建议\">".text2html($recommendation)."</item>\n";
		$introduction .= "</introduction>\n";
		print $fp $introduction;

		print $fp "<affected_items title=\"漏洞涉及的对象\">\n";
		foreach my $url (sort keys(%{$this->{issue}{$id}}))
		{
			my $request = $this->{issue}{$id}{$url}{request};
			$request=~s/[\x00-\x08\x0b-\x0c\x0e-\x1f]//g; #delete invaldate xml character
			my $response = $this->{issue}{$id}{$url}{response};
			$response=~s/[\x00-\x08\x0b-\x0c\x0e-\x1f]//g; #delete invaldate xml character
			my $analyze = $this->{issue}{$id}{$url}{analyze};
			my $issue_case = "<case>\n";
			$issue_case .= "<url title=\"对象\">".text2html($url)."</url>\n";
			$issue_case .= "<request title=\"请求\">".text2html($request)."</request>\n" if ($type eq 'expert');
			$issue_case .= "<response title=\"响应\">".text2html($response)."</response>\n" if ($type eq 'expert');
			$issue_case .= "<analyze title=\"分析\">".text2html($analyze)."</analyze>\n";
			$issue_case .= "</case>\n";
			print $fp $issue_case;
		}
		print $fp "</affected_items>\n";
		print $fp "</issue>\n";
	}
	print $fp "</details>\n";
}

1;
