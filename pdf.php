<?php
/*
 * Date: 2011-8-3
 *
 */

include('fpdf/chinese-unicode.php');

class PDF extends PDF_Unicode{

  function Header(){ //设置页眉
      //$this->AddGBhwFont('simhei', '黑体'); //载入中文字体
      //$this->SetFont('simhei', '' , 10);
      //$this->SetFont('simsun', '', 12);
      /*
      $this->Write(10, '页眉信息');
      $this->SetFont('Courier' , 'B'); //设置字体样式
      $this->Write(10 , '  http://www.yemei.com');
      */
      //$this->Ln(20);
  }

  function Footer(){ //设置页脚
      $this->SetY(-15);
      $this->SetFont('Courier' , '' , 9);
      $this->Cell(0 , 10 , $this->PageNo(), 0, 0, 'R');
  }
  
  function MyCell($w, $h, $txt, $border=0){
      $le = strlen($txt);
      if($le < 98){
        $this->Cell($w, $h, $txt, $border);
      } else {
        $arr = str_split($txt, 98);
        foreach($arr as $s){
            $this->Cell($w, $h, $s, $border, 2);
        }
      }
  }
}

function isAssic($txt){
    $len = strlen($txt);
    $i = 0;
    while(127>ord($txt[$i]) && $i<$len){
        $i++;
    };
    return $i == $len;
}
$tm1 = time();
ini_set("memory_limit","512M");
ini_set("max_execution_time", 1800);
if($argc < 3){
    exit(1);
}
$xml_file = $argv[1];

$xml = simplexml_load_file($xml_file);

$pdf = new PDF();
$pdf->AddUniGBhwFont('simsun', '宋体'); //载入中文字体

$pdf->Open();
$pdf->AliasNbPages();
$pdf->SetMargins(50, 30, 50);
$pdf->AddPage();

$pdf->SetFont('simsun', 'B', 17); //设置字体样式 B为加粗
$pdf->Cell(500, 50, '网站漏洞扫描报告', 0, 0, 'C');
$pdf->Ln();

//基本信息
$pdf->SetFontSize(15);
$ret = $xml->xpath('/report/information/@title');
$pdf->Write(10, $ret[0]->title);
$pdf->Ln();

//扫描信息
$pdf->SetFontSize(13);
$ret = $xml->xpath('/report/information/scan_info/@title');
$pdf->Write(50, $ret[0]->title);
$pdf->Ln();

$pdf->SetFont('simsun', '', 10);
$ret = $xml->xpath('/report/information/scan_info/item');
for($i=0; $i<count($ret); $i++){
	$pdf->SetFillColor(208, 208, 208);
    $pdf->Cell(120, 18, $ret[$i]['title'], 1, 0, 'R', 1);
    $pdf->Cell(380, 18, '  '.$ret[$i],1);
    $pdf->Ln();
}

//服务器信息
$pdf->SetFont('simsun', 'B', 13);
$ret = $xml->xpath('/report/information/server_info/@title');
$pdf->Write(50, $ret[0]->title);
$pdf->Ln();

$pdf->SetFont('simsun', '', 10);
$ret = $xml->xpath('/report/information/server_info/item');
for($i=0; $i<count($ret); $i++){
    $pdf->Cell(120, 18, $ret[$i]['title'], 1, 0, 'R', 1);
    $pdf->Cell(380, 18, '  '.$ret[$i],1);
    $pdf->Ln();
}

//报告摘要
$pdf->SetFont('simsun', 'B', 15);
$ret = $xml->xpath('/report/summary/@title');
$pdf->Write(40, $ret[0]->title);
$pdf->Ln();

//网站安全风险类型统计
echo "wang zhan feng xian lei xing tong ji\n";
$pdf->SetFontSize(13);
$ret = $xml->xpath('/report/summary/risk_stat/@title');
$pdf->Write(30, $ret[0]->title);
$pdf->Ln();

$pdf->SetFont('simsun', '', 10);
$pdf->Cell(120, 80, '风险等级', 1, 0, 'R', 1);
$x = $pdf->GetX();
$y = $pdf->GetY();
if(file_exists('report/h.jpg'))
    $pdf->Image('report/h.jpg', $x+28, $y+7, 70, 66);
$pdf->Cell(126, 80, '', 1);
$x = $pdf->GetX();
$y = $pdf->GetY();
if(file_exists('report/m.jpg'))
    $pdf->Image('report/m.jpg', $x+28, $y+7, 70, 66);
$pdf->Cell(126, 80, '', 1);
$x = $pdf->GetX();
$y = $pdf->GetY();
if(file_exists('report/l.jpg'))
    $pdf->Image('report/l.jpg', $x+28, $y+7, 70, 66);
$pdf->Cell(126, 80, '', 1);
$pdf->Ln();
$ret = $xml->xpath('/report/summary/risk_stat/item');
$pdf->Cell(120, 18, '漏洞数量', 1, 0, 'R', 1);
$pdf->Cell(126, 18, $ret[0], 1, 0, 'C');
$pdf->Cell(126, 18, $ret[1], 1, 0, 'C');
$pdf->Cell(126, 18, $ret[2], 1, 0, 'C');
$pdf->Ln();

//网站漏洞类型统计
echo "wang zhan lou dong lei xing tong ji\n";
$pdf->SetFont('simsun', 'B', 13);
$ret = $xml->xpath('/report/summary/issue_stat/@title');
$pdf->Write(50, $ret[0]->title);
$pdf->Ln();

$pdf->SetFont('simsun', '', 10);
$ret = $xml->xpath('/report/summary/issue_stat/item');
$leng = array();
for($i=0; $i<count($ret); $i++){
    $leng[] = $ret[$i]+0;    
}

$mx = max($leng);
if($mx < 200){
    for($i=0; $i<count($leng); $i++){
        $leng[$i] = ceil($leng[$i]/$mx*200);
    }
} else if ($mx > 350){
    for($i=0; $i<count($leng); $i++){
        $leng[$i] = ceil($leng[$i]/$mx*350);
    }
}

for($i=0; $i<count($ret); $i++){
    $pdf->Cell(120, 18, $ret[$i]['title'], 1, 0, 'R', 1);
    $x = $pdf->GetX();
    $y = $pdf->GetY();
    $pdf->SetFillColor(254, 240, 82);
    $pdf->Rect($x+25, $y+3, $leng[$i], 12, 'F');
    $pdf->SetFillColor(208, 208, 208);
    $pdf->Cell(380, 18, ' '.$ret[$i], 1);
    $pdf->Ln();
}

//网站漏洞清单
echo "wang zhan lou dong qing dan\n";
$pdf->SetFont('simsun', 'B', 15);
$ret = $xml->xpath('/report/information/@title');
$pdf->Write(50, '网站漏洞清单');
$pdf->Ln();

//安全漏洞
echo "an quan lou dong\n";
$ret = $xml->xpath('/report/details/issue');
for($i=0; $i<count($ret); $i++){
    $pdf->SetFont('simsun', 'B', 13);
    $pdf->Write(40,$ret[$i]['title']);
    $pdf->Ln();
    
    $ret2 = $ret[$i]->xpath('affected_items/@title');
    $pdf->SetFont('simsun', '', 10);
    $pdf->Cell(500, 18,$ret2[0], 1, 0, 'C', 1);
    $pdf->Ln();
    
    /**/
    $ret3 = $ret[$i]->xpath('affected_items/case');
    for($j=0; $j<count($ret3); $j++){
        $u = $pdf->getU();
        $pdf->setU(0);
        $pdf->SetFont('Courier', '', 9);
        $pdf->MultiCell(500, 18, $ret3[$j]->url, 1);
        $pdf->setU($u);
        //$pdf->Cell(500, 18, $ret3[$j]->url, 1);
        //$pdf->Ln();
    }
    $pdf->Ln();
    /**/
    $ret4 = $ret[$i]->xpath('introduction/@title');
    $pdf->SetFont('simsun', '', 10);
    $pdf->Cell(500, 18, $ret4[0], 1, 0, 'C', 1);
    $pdf->Ln();
    
    $ret5 = $ret[$i]->xpath('introduction/item');
    for($j=0; $j<count($ret5); $j++){
        $ox = $pdf->GetX(); //原x
        $oy = $pdf->GetY(); //原y
        
        $pdf->SetX($ox+120); 
        $pdf->MultiCell(380, 18, ' '.$ret5[$j], 1); 
        
        $nx = $pdf->GetX(); //新x
        $ny = $pdf->GetY(); //新y
        
        $height = $ny - $oy;
        if($height < 0){ //换页的情况
            $pdf->SetY(30);
            $height = $ny-30;
        } else {
            $pdf->SetY($oy);
        }
        
        $pdf->Cell(120, $height, $ret5[$j]['title'], 1, 0, 'R', 1);
        
        $pdf->SetXY($nx, $ny);
    }
    $pdf->Ln();
}

//网站风险评估   
echo "wang zhan feng xian ping gu\n";
$pdf->SetFont('simsun', 'B', 13);
$ret = $xml->xpath('/report/summary/evaluation');
$pdf->Write(50, $ret[0]['title']);
$pdf->Ln();

$pdf->SetFont('simsun', '', 10);
$pdf->Cell(120, 18, $ret[0]->risk['title'], 1, 0, 'R', 1);
$pdf->Cell(380, 18, ' '.$ret[0]->risk, 1);
$pdf->Ln();

$ox = $pdf->GetX(); //原x
$oy = $pdf->GetY(); //原y

$pdf->SetX($ox+120); 
$pdf->MultiCell(380, 18, ' '.$ret[0]->description, 1, 1);

$nx = $pdf->GetX(); //新x
$ny = $pdf->GetY(); //新y

$height = $ny - $oy;
if($height < 0){ //换页的情况
    $pdf->SetY(30);
    $height = $ny-30;
} else {
    $pdf->SetY($oy);
}
$pdf->Cell(120, $height, $ret[0]->description['title'], 1, 0, 'R', 1);
$pdf->SetXY($nx, $ny);

$pdf->Ln();
$pdf->Ln();

//附
echo "Fu: \n";
$pdf->SetFont('simsun', 'B', 15);
$ret = $xml->xpath('/report/details/@title');
$pdf->Write(50, "附：".$ret[0]);
$pdf->Ln();

$ret = $xml->xpath('/report/details/issue');
$ct1 = count($ret);
echo "ct1: $ct1\n";
for($i=0; $i<$ct1; $i++){
    $pdf->SetFont('simsun', 'B', 13);
    $pdf->Write(30, $ret[$i]['title']);
    $pdf->Ln();
    
    $pdf->SetFont('simsun', '', 10);
    $ret2 = $ret[$i]->xpath('affected_items/case');
    $ct2 = count($ret2);
    echo "ct2: $ct2\n";
    echo "affected_items/case: ".$ct2."\n";
    
    for($j=0; $j<$ct2; $j++){
        //echo "ct1: ".($i+1)."/$ct1  ct2: ".($j+1)."/$ct2\n";
        
        //---URL---
        // echo "URL...\n";
        $pdf->Cell(500, 18, $ret2[$j]->url['title'], 1, 0, '', 1);
        $pdf->Ln();
        if(isAssic($ret2[$j]->url)){
            $u = $pdf->getU();
            $pdf->setU(0);
            $pdf->SetFont('Courier', '', 9);
            $pdf->MultiCell(500, 18, $ret2[$j]->url, 1);
            $pdf->setU($u);
        } else {
            $pdf->MultiCell(500, 18, $ret2[$j]->url, 1);
        }
        //$pdf->Ln();
                   
        //---Request---
        // echo "---Request---\n";
        $pdf->SetFont('simsun', '', 10);
        $pdf->Cell(500, 18, $ret2[$j]->request['title'], 1, 0, '', 1);
        $pdf->Ln();
        if(isAssic($ret2[$j]->request)){
            $u = $pdf->getU();
            $pdf->setU(0);
            $pdf->SetFont('Courier', '', 9);
            $pdf->MultiCell(500, 18, $ret2[$j]->request, 1);
            $pdf->setU($u);
        } else {
            $pdf->MultiCell(500, 18, $ret2[$j]->request, 1);
        }
        //$pdf->Ln();
        
        //---Response---
        // echo "---Response---\n";
        $pdf->SetFont('simsun', '', 10);
        $pdf->Cell(500, 18, $ret2[$j]->response['title'], 1, 0, '', 1);
        $pdf->Ln();
        if(isAssic($ret2[$j]->response)){
            $u = $pdf->getU();
            $pdf->setU(0);
            $pdf->SetFont('Courier', '', 9);
            $pdf->MultiCell(500, 18, $ret2[$j]->response, 1);
            $pdf->setU($u);
        } else {
            $pdf->MultiCell(500, 18, $ret2[$j]->response, 1);
        }
        //$pdf->Ln();
        
        //---Analyze---
        // echo "---Analyze---\n";
        $pdf->SetFont('simsun', '', 10);  
        $pdf->Cell(500, 18, $ret2[$j]->analyze['title'], 1, 0, '', 1);
        $pdf->Ln();
        if(isAssic($ret2[$j]->analyze)){
            $u = $pdf->getU();
            $pdf->setU(0);
            $pdf->SetFont('Courier', '', 9);
            $pdf->MultiCell(500, 18, $ret2[$j]->analyze, 1);
            $pdf->setU($u);
        } else {
            $pdf->MultiCell(500, 18, $ret2[$j]->analyze, 1);
        }
        //$pdf->Ln();
    }  
    $pdf->Ln();
    
}

$pdf->Output($argv[2], 'F');
$tm2 = time();
echo "\n".($tm2-$tm1)."\n";
?>
