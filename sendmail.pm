package sendmail;
use strict;
use Net::SMTP;
use Authen::SASL;
use MIME::Base64;

require Exporter;
our $VERSION = 1.00;
our @ISA = qw(Exporter);
our @EXPORT = qw(send_mail);

my $smtp_server = 'hikml2.hikvision.com.cn';
my $smtp_accounts = 'wanghongliang@hikvision.com.cn';
my $smtp_password = 'oa850906';

sub send_mail
{
    my ($from, $tolist, $toname, $subject, $body, $attachment, $attachment_name) = @_;
    my $smtp = Net::SMTP->new($smtp_server, Debug=>0) or return 0; # die "set smtp server error.\n";
    $smtp->auth($smtp_accounts, $smtp_password) or return 0; #die "smtp auth error\n"; #Need Authen-SASL
    my $split_bar = "==========w.h.l==========";
    $smtp->mail($smtp_accounts);
    $smtp->to(split(',', $tolist), {SkipBad=>1});
    $smtp->data();
    my $cur_time = `date -R`; chomp($cur_time);
    $smtp->datasend("Date:$cur_time\n");
    $smtp->datasend("From:$from\n");
    $smtp->datasend("To:$toname\n");
    $smtp->datasend("Subject:$subject\n");
    $smtp->datasend("MIME_Version:1.0\n");
    if (defined($attachment))
    {
        $smtp->datasend("Content-Type:multipart/mixed;\n\tboundary=\"$split_bar\"\n\n");
        $smtp->datasend("This is a multi-part message in MIME format.\n\n");
        $smtp->datasend("--$split_bar\n");
    }
    #$smtp->datasend("Content-Type:text/plain;\n");
    $smtp->datasend("Content-Type:text/html;charset=UTF-8\n");
    $smtp->datasend("Content-Transfer-Encoding:base64\n\n");
    $smtp->datasend(encode_base64($body)."\n\n");
    if (defined($attachment))
    {
        $smtp->datasend("--$split_bar\n");
        ($attachment_name) = $attachment=~m/([^\/]*)$/ if(!defined($attachment_name));
        $smtp->datasend("Content-Type:application/octet-stream;\n\tname=\"$attachment_name\"\n");
        $smtp->datasend("Content-Transfer-Encoding:base64\n");
        $smtp->datasend("Content-Disposition:attachment;\n\tfilename=\"$attachment_name\"\n\n");
        $smtp->datasend(encode_base64(readpipe("cat ".quotemeta($attachment)))."\n\n");
        $smtp->datasend("--$split_bar--\n");
    }
    my $succeed = $smtp->dataend() or return 0; # die "Send failed!\n";
    $smtp->quit;
    return $succeed;
}

1;
