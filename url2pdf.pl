#!/usr/bin/perl -w
use strict;

# Created by Zhaoyong, 2010-08-23
# It need use curl

BEGIN
{
    my $path;
    if ($^O eq 'linux')
    {
        if ($0=~m/^(.+)\//) { $path = $1; } else { $path = readpipe('pwd'); chomp($path); }
        unshift(@INC, $path);
        chdir($path);
    }else
    {
        die("Please run this script on linux.\n");   
    }
}

sub usage
{
	print "$0 --url=[url] --pdf=[outfile]\n";
	exit(0);
}

sub main
{
	my ($url, $pdf);
	foreach my $param (@ARGV)
	{
		if ($param=~m/--url=(.+)/)
		{
			$url = $1;
		}elsif ($param=~m/--pdf=(.+)/)
		{
			$pdf = $1;
		}else
		{
			usage("Unknown option: $param\n");
		}
	}
	usage() if (!defined($url) || !defined($pdf));
	my $retry_times = 5;
	while(!url2pdf($url, $pdf))
	{
		$retry_times--;
		die "Convert to pdf failed.\n" if($retry_times == 0);
		sleep(60);
	}
}

sub mycurl
{
	my $param = shift;
	my $curl = "curl --silent --connect-timeout 120 --max-time 1200 --retry 5 --retry-delay 60 --user-agent 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)'";
	my $retry_times = 5;
	while($retry_times)
	{
		my $result = readpipe("$curl $param");
		while($result eq '' && readpipe("$curl http://www.baidu.com/") eq '')
		{
			sleep(10);
			$result = readpipe("$curl $param");
		}
		$retry_times--;
		return $result if($retry_times==0 || $result ne '');
		sleep(60);
	}
}

sub url2pdf
{
	my ($url, $pdf) = @_;
	#login page: http://web2.pdfonline.com/pdfonline/web/signin.asp	username=anchiva, password=rapidrx
	$url=~s/([^\w\-\.\~])/sprintf("%%%02X",ord($1))/ieg;
	my $url2pdf = "http://web2.pdfonline.com/pdfonline/testSettings.asp?cURL=$url&page=0&top=0.5&bottom=0.5&left=0.5&right=0.5";
	my $response = mycurl("--include ".quotemeta($url2pdf));
	if ($response=~m/^(.+?)\r\n\r\n(.*)$/s)
	{
		my ($head, $body) = ($1, $2);
		my $response_code = ($head=~m/^HTTP\/\d+\.\d* (\d+)/)?$1:'0';
		my $content_type = ($head=~m/^Content-Type: ([\w\-\/]+)/mi)?$1:'';
		my $content_length = ($head=~m/^Content-Length: (\d+)/mi)?$1:'0';
		if ($response_code == 200 && $content_type eq 'application/pdf' && $content_length == length($body))
		{
			$body=~s/\r\n\r\n<\/body>\r\n\r\n<\/html>$//i;
			open(FH, ">", $pdf) or die "Can not create outfile: $pdf\n";
			print FH $body;
			close(FH);
			return 1;
		}else
		{
			return 0;
		}
	}else
	{
		die "Can not connect url2pdf url: $url2pdf\n";
	}
}

main();
