package Malsite;
use strict;
use threads;
use threads::shared;
use web;

sub new
{
	my $type = shift;
	my $class = ref($type) || $type;
	my $this = {};
	bless($this, $class);	 
	$this->initialize(@_);
	return $this;
}

sub initialize
{
	my $this = shift;
	$this->{history} = shared_clone({});;
	$this->load_pattern();
}

sub load_pattern
{
	my $this = shift;
	my $pattern = `cat pattern.def`; chomp($pattern);
	$pattern = 'k084uf7d63ew' if ($pattern eq '');  #set pattern for a random string when no pattern
	$this->{pattern} = $pattern;

	my $whitelist = `cat whitelist.def`; chomp($whitelist);
	my @array = split(/\|/,$whitelist);
	my @whitelist_array = ();
	foreach(@array)
	{
		push @whitelist_array,qr/^($_)$/i;
	}
	#$this->{whitelist} = $whitelist;
	$this->{whitelist} = \@whitelist_array;
}

sub malsite_scan
{
	my $this = shift;
	my ($url, $depth) = @_;
	$depth = 0 if (!defined($depth));
	return 0 if ($depth>10);
	{lock $this->{history};
	return ($this->{history}{$url}==1)?1:0 if (defined($this->{history}{$url}));
	$this->{history}{$url} = 0;
	}

	my $domain = get_domain($url);
	return 0 if ($domain~~$this->{whitelist});
	#return 0 if ($domain=~m/^($this->{whitelist})$/i);

	my $head_body = mycurl('--include '.quotemeta($url));
	my ($head, $content) = ($1, $2) if ($head_body=~m/^(?:HTTP\/\d+\.\d+ 3\d\d [^\r]+\r\n(?:[^\r]+\r\n)*\r\n)*(HTTP\/\d+\.\d+ 200 [^\r]+\r\n(?:[^\r]+\r\n)*)\r\n(.*)$/sio);
	return 0 if (!defined($head));

	$content=~s/\\(['"\/\\])/$1/g;
	$content=~s/['"]\s*\+\s*['"]//g;
	$content=~s/(%[a-f0-9]{2})/decode_pct($1)/ieg;

	if ($content=~m/($this->{pattern})/is)
	{lock $this->{history};
		$this->{history}{$url} = 1;
		return 1;
	}

	if (my @script = $content=~m/<script(?=\s)[^>]*?(?<=\s)src\s*=\s*['"]?\s*([^'"\s>]+)[^>]*>/gio)
	{
		foreach my $src (@script)
		{
			$src=~s/\\//g;
			$src = combine_url($url, $src);
			if ($src ne '' && $this->malsite_scan($src, $depth+1))
			{lock $this->{history};
				$this->{history}{$url} = 1;
				return 1;
			}
		}
	}

	if (my @iframe = $content=~m/<i?frame(?=\s)[^>]*?(?<=\s)src\s*=\s*['"]?\s*([^'"\s>]+)[^>]*>/gio)
	{
		foreach my $src (@iframe)
		{
			if (defined($src))
			{
				$src=~s/\\//g;
				$src = combine_url($url, $src);
				if ($src ne '' && $this->malsite_scan($src, $depth+1))
				{lock $this->{history};
					$this->{history}{$url} = 1;
					return 1;
				}
			}
		}
	}
	return 0;
}

sub get_malicious_script
{
	my $this = shift;
	my $content_ref = shift;
	my $script_string = '';
	if (my @script = $$content_ref=~m/<script(?=\s)[^>]*?(?<=\s)src\s*=\s*['"]?\s*(http:\/\/[^'"\s>]+)[^>]*>/gio)
	{
		my %checked = ();
		foreach my $src (@script)
		{
			$src=~s/\\//g;
			my $old_src = $src;
			$src = normalize_url($src);
			if($src ne '' && !defined($checked{$src}))
			{
				$checked{$src} = 1;
				$script_string .= "$old_src\n" if $this->malsite_scan($src);
			}
		}
	}
	return $script_string;
}

sub get_malicious_iframe
{
	my $this = shift;
	my $content_ref = shift;
	my $iframe_string = '';
	if (my @iframe = $$content_ref=~m/<i?frame(?=\s)[^>]*?\s(?:(?:width|height)\s*=\s*['"]?[0-5](?!\d)['"]?\s+[^>]*?(?<=\s)src\s*=\s*['"]?\s*(http:\/\/[^'"\s>]+)|src\s*=\s*['"]?\s*(http:\/\/[^'"\s>]+)[^>]*?\s+(?:width|height)\s*=\s*['"]?[0-5](?!\d)['"]?)[^>]*>|<div(?=\s)[^>]*?(?<=\s)style\s*=\s*['"]?[^>]*?(?:display\s*:\s*none|visibility\s*:\s*hidden|absolute[^>]+?(?:top|left)\s*:\s*(?:expression|\-))[^>]*?['"]?[^>]*>.{0,100}?((?:<i?frame(?=\s)[^>]*?(?<=\s)src\s*=\s*['"]?\s*http:\/\/[^'"\s>]+[^>]*>.{0,100}?)+)<\/div>/gio)
	{
		my %checked = ();
		foreach my $src (@iframe)
		{
			next unless defined $src;
			if ($src=~m/^http:\/\//i)
			{
				$src=~s/\\//g;
				my $old_src = $src;
				$src = normalize_url($src);
				if($src ne '' && !defined($checked{$src}))
				{
					$checked{$src} = 1;
					$iframe_string .= "$old_src\n" if $this->malsite_scan($src);
				}
			}else
			{
				foreach my $div_iframe ($src=~m/(http:\/\/[^'"\s>]+)/gi)
				{
					$div_iframe=~s/\\//g;
					my $old_div_iframe = $div_iframe;
					$div_iframe = normalize_url($div_iframe);
					if ($div_iframe ne '' && !defined($checked{$div_iframe}))
					{
						$checked{$div_iframe} = 1;
						$iframe_string .= "$old_div_iframe\n" if $this->malsite_scan($div_iframe);
					}
				}
			}
		}
	}
	return $iframe_string;
}

sub check_malsite
{
	my $this = shift;
	my ($content_ref) = @_;
	return $this->get_malicious_script($content_ref).$this->get_malicious_iframe($content_ref);
}

sub get_recursion_div
{
	my $content_ref = shift;
	my @div = ();
	my $content = $$content_ref=~/<div(?=\s)[^>]*?(?<=\s)style\s*=\s*['"]?[^>]*?(?:display\s*:\s*none|visibility\s*:\s*hidden|absolute[^>]+?(?:top|left)\s*:\s*(?:expression|\-))[^>]*?['"]?[^>]*>/sio ? $$content_ref : '';
	while ($content=~s/.*?(<div(?=\s)[^>]*?(?<=\s)style\s*=\s*['"]?[^>]*?(?:display\s*:\s*none|visibility\s*:\s*hidden|absolute[^>]+?(?:top|left)\s*:\s*(?:expression|\-))[^>]*?['"]?[^>]*>)//sio)
	{
		my $stack = 1;
		my $content1 = $1.$content;
		while ($content=~s/.*?(<div(?=[\s>])[^>]*>|<\/div>)//is)
		{
			my $div = $1;
			$div=~/^<div/i ? $stack++ : $stack--;
			last if $stack == 0;
		}
		push @div, substr($content1, 0, length($content1)-length($content));
	}
	return @div;
}

sub get_recursion_marquee
{
	my $content_ref = shift;
	my @marquee = ();
	my $content = $$content_ref=~/<marquee(?=\s)[^>]*?(?<=[\s;'"])(?:width|height)\s*[:=]\s*['"]?(?:1(?!\d)|expression\([\d\+\-\*\/]+\))[^>]*>/sio ? $$content_ref : '';
	while ($content=~s/.*?(<marquee(?=\s)[^>]*?(?<=[\s;'"])(?:width|height)\s*[:=]\s*['"]?(?:1(?!\d)|expression\([\d\+\-\*\/]+\))[^>]*>)//sio)
	{
		my $stack = 1;
		my $content1 = $1.$content;
		while ($content=~s/.*?(<marquee(?=[\s>])[^>]*>|<\/marquee>)//is)
		{
			my $marquee = $1;
			$marquee=~/^<marquee/i ? $stack++ : $stack--;
			last if $stack == 0;
		}
		push @marquee, substr($content1, 0, length($content1)-length($content));
	}
	return @marquee;
}

sub get_marquee
{
	my $this = shift;
	my ($content_ref, $domain) = @_;
	my $marquee_string = '';
	for (get_recursion_marquee($content_ref))
	{
		$marquee_string .= "$_\n" if !/^<marquee(?=\s)[^>]*?(?<=\s)(?:id|class)\s*=/i && grep {get_domain($_) ne $domain} /\s(?:href|src)\s*=\s*['"]?\s*(http:\/\/[\w\-.]+)/ig;
	}
	return $marquee_string;
}

sub get_div
{
	my $this = shift;
	my ($content_ref, $domain) = @_;
	my $domain_quote = quotemeta($domain);
	my $div_string = '';
	for (get_recursion_div($content_ref))
	{
		my @adware;
		$div_string .= "$_\n" if !/^<div(?=\s)[^>]*?(?<=\s)(?:id|class)\s*=/i && (@adware = /\s(?:href|src)\s*=\s*['"]?\s*(http:\/\/[\w\-.]+)/ig) && (!grep {get_domain($_)=~/^($domain_quote|alexa\.com|cnzz\.com|linezing\.com|51\.la|51yes\.com|53kf\.com|gostats\.cn|vdoing\.com|google-analytics\.com)$/i} @adware) && (!grep {get_hostname($_)=~/^(exp\.cms\.grandcloud\.cn)$/i} @adware);
	}
	return $div_string;
}

sub get_ahref
{
	my $this = shift;
	my ($content_ref, $domain) = @_;
	my $ahref_string = '';

	# TODO display:none fp prone...
	for ($$content_ref=~m/<a(?=\s)[^>]*?(?<=\s)style\s*=\s*['"]?[^>]*?(?:margin-left\s*:\s*(?:expression|\-)|absolute[^>]+?(?:top|left)\s*:\s*(?:expression|\-))[^>]*>[^<]*<\/a>/gio)
	{
		$ahref_string .= "$_\n" if grep {get_domain($_) ne $domain} /\shref\s*=\s*['"]?\s*(http:\/\/[\w\-.]+)/ig;
	}
	return $ahref_string;
}

sub get_hijack
{
	my $this = shift;
	my ($content_ref, $domain) = @_;
	my $hijack_string = '';

	for ($$content_ref=~m/\.referrer\b.{1,400}\bif\b.{1,50}\.\b(?:indexOf|regexp|test)\b.{1,600}\blocation(?:\.href)?\s*=\s*["']?\s*http:\/\/[^'"\s]+['"\s]/gsio)
	{
		$hijack_string .= "$_\n" if grep {get_domain($_) ne $domain} /\blocation(?:\.href)?\s*=\s*['"]?\s*(http:\/\/[\w\-.]+)/ig;
	}
	return $hijack_string;
}

sub check_seo_adware
{
	my $this = shift;
	my ($content_ref, $domain) = @_;
	return $this->get_div($content_ref, $domain).$this->get_marquee($content_ref, $domain).$this->get_ahref($content_ref, $domain).$this->get_hijack($content_ref, $domain);
}

sub check_hacked_page
{
	my $this = shift;
	my $content_ref = shift;
	my @array = (qr/^(.*(?:hacked).*)$/mio,qr/^(.*(?:fucked).*)$/mio,qr/^(.*(?:vulnerability).*)$/mio,qr/^(.*(?:hmei7).*)$/mio,qr/^(.*(?:s4r4d0).*)$/mio,qr/^(.*(?:HEXB00T3R).*)$/mio,qr/^(.*(?:CRAZY_Q).*)$/mio,qr/^(.*(?:DiJiT?L).*)$/mio,qr/^(.*(?:Sahara).*)$/mio,qr/^(.*(?:ezhoe).*)$/mio,qr/^(.*(?:disini).*)$/mio,qr/^(.*(?:Backlin3).*)$/mio,qr/^(.*(?:BlackHat).*)$/mio,qr/^(.*(?:Cairex).*)$/mio,qr/^(.*(?:amiinn).*)$/mio,qr/^(.*(?:eFeX09).*)$/mio,qr/^(.*(?:BOM2).*)$/mio,qr/^(.*(?:POKENG).*)$/mio,qr/^(.*(?:Parsiland).*)$/mio,qr/^(.*(?:Torres).*)$/mio,);
	#if ($$content_ref=~m/^(.*(?:hacked|fucked|vulnerability|hmei7|s4r4d0|HEXB00T3R|CRAZY_Q|DiJiT?L|Sahara|ezhoe|disini|Backlin3|BlackHat|Cairex|amiinn|eFeX09|BOM2|POKENG|Parsiland|Torres).*)$/mio)
	if($$content_ref~~@array)
	{
		return $1;
	}else
	{
		return '';
	}
}

1;
