#!/usr/bin/perl
#===============================================================================
#
#         FILE:  webscan.pl
#
#  DESCRIPTION:  WEB漏洞扫描器
#
# REQUIREMENTS:  Unix, Perl5.10, Curl
#         BUGS:  No
#
#       AUTHOR:  wanghl
#      COMPANY:  HikVision
#      VERSION:  0.2
#      CREATED:  08/22/2013
#===============================================================================

use 5.010;
use warnings;
use strict;

use Cwd;
BEGIN
{
	if ($^O eq 'linux')
	{
		my $path = $0=~m/^(.+)\// ? $1 : cwd;
		unshift(@INC, $path);
		chdir($path);
	}else
	{
		die("Please run this script on linux.\n");   
	}
}
use Crawler;

sub usage
{
	print $_[0] if (defined($_[0]));
	print "Usage:\n";
	print "\t--url=[website index]\n";
	print "\t--subdomain=[sub domain(default no), yes|no] OPTION\n";
	print "\t--proxy=[ip:port] OPTION\n";
	print "\t--cookie=[cookie file] OPTION\n";
	print "\t--scan_depth=[scan depth(default 4), 0-10(0 mean no limit!)] OPTION\n";
	print "\t--scan_bit=[scan bit(default comm), N bit|hik|min|comm|all] OPTION\n";
	print "\t--scan_thread=[scan thread(default 10), 1-30] OPTION\n";
	print "\t--max_scan_time=[max scan time(hour)(default 10), 1-24] OPTION\n";
	print "\t--report=[xml report] OPTION\n";
	print "\t--reportype=[report type(default expert), common|expert] OPTION\n";

	exit 1;
}

sub check_param
{
	my ($seed, $subdomain, $proxy, $cookie, $scan_depth, $scan_bit, $scan_thread, $max_scan_time, $reportype) = @_;

	usage("Need seed url!\n") if (!defined($seed));
	usage("The seed url format error!\n") if ($seed!~m/^(http|https):\/\/.+$/i);
	usage("The scan bit format error!\n") if (defined($scan_bit) && $scan_bit!~/^([01]+|hik|min|comm|all)$/i);
	usage("The reportype format error!\n") if (defined($reportype) && $reportype!~/^(common|expert)$/i);
	if (defined($subdomain) && $subdomain!~/^(yes|no)$/i)
	{
		usage("subdomain format error\n");
	}
	if (defined($proxy) && $proxy!~m/^(((25[0-4]|2[0-4]\d|1\d\d|[1-9]\d?)\.){3}(25[0-4]|2[0-4]\d|1\d\d|[1-9]\d?):[1-9]\d{0,4})$/)
	{
		usage("proxy format error\n");
	}
	if (defined($cookie))
	{
		usage("cookie file not exist\n") if (!-e $cookie);
		usage("cookie file not directory\n") if (-d $cookie);
	}
	if (defined($scan_depth))
	{
		usage("scan_depth must be numeric\n") if ($scan_depth!~m/^\d+$/);
		usage("scan_depth out of range[0-10]\n") if ($scan_depth>10);
	}
	if (defined($scan_thread))
	{
		usage("scan_thread must be numeric\n") if ($scan_thread!~m/^\d+$/);
		usage("scan_thread out of range[1-30]\n") if ($scan_thread>30 || $scan_thread==0);
	}
	if (defined($max_scan_time))
	{
		usage("max_scan_time must be numeric\n") if ($max_scan_time!~m/^\d+$/);
		usage("max_scan_time out of range[1-24]\n") if ($max_scan_time>24 || $max_scan_time==0);
	}
}

sub main
{
	usage() if (@ARGV==0);

	my ($seed, $subdomain, $proxy, $cookie, $scan_depth, $scan_bit, $scan_thread, $max_scan_time, $white_url, $report, $reportype);
	foreach my $param (@ARGV)
	{
		if ($param=~m/^--url=(.*)$/)
		{
			$seed = $1;
		}elsif ($param=~m/^--subdomain=(.*)$/)
		{
			$subdomain = $1;
		}elsif ($param=~m/^--proxy=(.*)$/)
		{
			$proxy = $1;
		}elsif ($param=~m/^--cookie=(.*)$/)
		{
			$cookie = $1;
		}elsif ($param=~m/^--scan_depth=(.*)$/)
		{
			$scan_depth = $1;
		}elsif ($param=~m/^--scan_bit=(.*)$/)
		{
			$scan_bit = $1
		}elsif ($param=~m/^--scan_thread=(.*)$/)
		{
			$scan_thread = $1;
		}elsif ($param=~m/^--max_scan_time=(.*)$/)
		{
			$max_scan_time = $1;
		}elsif ($param=~m/^--report=(.{0,255})$/)
		{
			$report = $1;
		}elsif ($param=~m/^--reportype=(.*)$/)
		{
			$reportype = $1;
		}else
		{
			usage("Unknown option: $param\n");
		}
	}#foreach
	check_param($seed, $subdomain, $proxy, $cookie, $scan_depth, $scan_bit, $scan_thread, $max_scan_time, $reportype);
	$cookie = `cat $cookie` if (defined($cookie));

	my $spider = new Crawler(Seed => $seed);
	$spider->setProxy($proxy) if (defined($proxy));
	$spider->setCookie($cookie) if (defined($cookie));
	$spider->setSubDomain(lc($subdomain)) if (defined($subdomain));
	$spider->setCrawlDepth($scan_depth) if (defined($scan_depth));
	$spider->setCrawlBit($scan_bit) if (defined($scan_bit));
	$spider->setCrawlThread($scan_thread) if (defined($scan_thread));
	$spider->setMaxCrawlTime($max_scan_time*3600) if (defined($max_scan_time));
	$spider->setWhiteUrl($white_url) if (defined($white_url));
	$spider->start_crawling();
	$spider->{Scanner}->{report}->print_report($report, defined($reportype)?lc($reportype):'expert') if (defined($report));

	exit $spider->{ExitCode};
}

main;
