package Crawler;
use strict;
use threads;
use threads::shared;
use Thread::Queue;
use POSIX qw(strftime);
use web;
use HTTPAgent;
use Scanner;

my $get_page :shared = 0;
my $linked_url :shared = 0;
my $stop = 0;
$SIG{TERM} = $SIG{INT} = \&setStop;

sub setStop
{
	$stop = 1;
}

sub new
{
	my $type = shift;
	my $class = ref($type) || $type;
	my $this = {};
	bless($this, $class);
	$this->initialize(@_);
	return $this;
}

sub initialize
{
	my $this = shift;
	my %param = @_;
	$this->setSeed($param{Seed});
	$this->setCrawlDepth($param{CrawlDepth});
	$this->setCrawlThread($param{CrawlThread});
	$this->setMaxCrawlTime($param{MaxCrawlTime});
	$this->setSubDomain($param{SubDomain});
	$this->setWhiteUrl($param{WhiteUrl});
	$this->{Queue} = new Thread::Queue();
	$this->{Scanner} = new Scanner();
	$this->setProxy($param{Proxy});
	$this->setCookie($param{Cookie});
	$this->setCrawlBit($param{CrawlBit});
	$this->{CatchLinked} = shared_clone({});
	$this->{Crawled} = shared_clone({});
	$this->{Logouted} = shared_clone([]);
	$this->{Running} = 0; share($this->{Running});
	$this->{ExitCode} = 0;
}

sub setSeed
{
	my $this = shift;
	my $seed = defined($_[0])?normalize_url($_[0]):'';
	my $hostname = get_hostname($seed);
	my $domain = get_domain($seed);
	my $ip = host2ip($hostname);
	$this->{Seed} = $seed;
	$this->{Hostname} = $hostname;
	$this->{Domain} = $domain;
	$this->{IP} = $ip;
	$this->{cms} = ''; share($this->{cms});
	$this->{ServerVerion} = ''; share($this->{ServerVerion});
	$this->{ScriptEngine} = ''; share($this->{ScriptEngine});
}

sub setCrawlDepth
{
	my $this = shift;
	my $crawl_depth = defined($_[0])?$_[0]:4;
	$this->{CrawlDepth} = $crawl_depth;
}

sub setCrawlBit
{
        my $this = shift;
        my $crawl_bit = defined($_[0])?$_[0]:'comm';
	$this->{Scanner}->setCrawlBit($crawl_bit);
}

sub setCrawlThread
{
	my $this = shift;
	my $crawl_thread = defined($_[0])?$_[0]:10;
	$this->{CrawlThread} = $crawl_thread;
}

sub setMaxCrawlTime
{
	my $this = shift;
	my $seconds =  defined($_[0])?$_[0]:3600*10;
	$this->{MaxCrawlTime} = $seconds;
}

sub setSubDomain
{
	my $this = shift;
	my $subdomain = defined($_[0])?$_[0]:'no';
	$this->{SubDomain} = $subdomain;
}

sub setWhiteUrl
{
        my $this = shift;
	if (defined($_[0]))
	{
		my @white_url;
		foreach my $url (split /\r?\n/, $_[0])
		{
			push @white_url, $url if ($url = normalize_url($url));
		}
		$this->{WhiteUrl} = \@white_url;
	}else
	{
		$this->{WhiteUrl} = [];
	}
}

sub setProxy
{
	my $this = shift;
	my $proxy = defined($_[0])?$_[0]:'';
	$this->{Proxy} = $proxy;
	$this->{Scanner}->setProxy($proxy);
}

sub setCookie
{
	my $this = shift;
	my $cookie = defined($_[0])?$_[0]:'';
	$this->{Cookie} = $cookie;
	$this->{Scanner}->setCookie($cookie);
}

sub getRunning
{
	my $this = shift;
	my $running;
	lock $this->{Running};
	$running = $this->{Running};
	return $running;
}

sub addRunning
{
	my $this = shift;
	lock $this->{Running};
	$this->{Running}++;
}

sub minusRunning
{
	my $this = shift;
	lock $this->{Running};
	$this->{Running}--;
}

sub sendData
{
	my $this = shift;
	$this->{Scanner}->{report}->{scan_info}{website} = $this->{Seed};
	$this->{Scanner}->{report}->{scan_info}{cms} = $this->{cms};
	$this->{Scanner}->{report}->{scan_info}{start_time} = strftime('%Y-%m-%d %H:%M:%S', localtime($this->{StartTime}));
	$this->{Scanner}->{report}->{scan_info}{end_time} = strftime('%Y-%m-%d %H:%M:%S', localtime($this->{EndTime}));
	my $scan_time = $this->{CrawlTime};
	my $hour = int($scan_time/3600);
	$scan_time = $scan_time - $hour*3600;
	my $min = int($scan_time/60);
	my $sec = $scan_time - $min*60;
	my $time_string = ($hour>0)?"${hour}小时":"";
	$time_string .= ($min>0 || $time_string ne '')?"${min}分":"";
	$time_string .= "${sec}秒";
	$this->{Scanner}->{report}->{scan_info}{scan_time} = $time_string;

	$this->{Scanner}->{report}->{server_info}{hostname} = $this->{Hostname};
	$this->{Scanner}->{report}->{server_info}{ip} = $this->{IP};
	{lock $this->{ServerVerion}; lock $this->{ScriptEngine};
	$this->{Scanner}->{report}->{server_info}{server_verion} = $this->{ServerVerion};
	$this->{Scanner}->{report}->{server_info}{script_engine} = $this->{ScriptEngine};
	}
}

sub start_crawling
{
	my $this = shift;

	$this->{StartTime} = time();
	$this->{Queue}->enqueue([$this->{Seed}, $this->{Seed}, 1, 0]);
	threads->new(\&crawl, $this) for (1 .. $this->{CrawlThread});

OUT:	while (1)
	{
		while ($this->getRunning() || $this->{Queue}->pending())
		{
			for (threads->list(threads::joinable)) #finished running
			{
				$_->detach();
				warn "$_ have finished running\n";
				$this->minusRunning();
				threads->new(\&crawl, $this);
			}
			sleep(5);
			#print $this->getRunning(), '<---->', $this->{Queue}->pending(), "\n";
			$this->{ExitCode} = 2, last OUT if ($stop);
			$this->{ExitCode} = 0, last OUT if (time() - $this->{StartTime} > $this->{MaxCrawlTime});
		}#while: main loop!

		#not need lock, all threads having a rest
		@{$this->{Logouted}} ? $this->{Queue}->enqueue(@{$this->{Logouted}}) : last;
		$this->{Logouted} = shared_clone([]);
	}

	$_->detach() for (threads->list(threads::all));
	my ($all_request_count, $all_receive_bytes) = gethttpinfo();
	print "totally linked $linked_url urls, get $get_page pages, $all_request_count requests, $all_receive_bytes bytes!\n";
	$this->{EndTime} = time();
	$this->{CrawlTime} = $this->{EndTime} - $this->{StartTime};
	$this->sendData();
	print 'Take ', $this->{Scanner}->{report}->{scan_info}{scan_time}, " for all scan!\n";
}

sub crawl
{
	my $this = shift;
	my $i = 0;

BEGIN:
	while (++$i)
	{
		$this->minusRunning() if ($i>1);
		my ($url, $referer, $depth, $guess) = @{$this->{Queue}->dequeue()};
		$this->addRunning();

		next BEGIN if ($this->{SubDomain} eq 'yes' ? $this->{Domain} ne get_domain($url) : $this->{Hostname} ne get_hostname($url));
		next BEGIN if ($url=~/\.(rar|zip|tar|7z|exe|dll|pdf|mp\d|docx?|pptx?|xlsx?)$/i);

		my $url_format;
		{lock $this->{Crawled};
		next BEGIN if (defined($this->{Crawled}{$url}) && ($this->{Crawled}{$url}==0 || $guess!=0));
		$url_format = $url;
		$url_format=~s/([^\&=]+)=[^\&]+/$1=/g; 
		next BEGIN if ($url ne $url_format && defined($this->{Crawled}{$url_format}) && ($this->{Crawled}{$url_format}==0 || $guess!=0));
		next BEGIN if (grep {index($url_format, $_)==0} @{$this->{WhiteUrl}});
		$linked_url++ if ($guess>=0);
		$get_page++;
		#print "$url\n";
		print "$url\n" if ($guess>=0);
		$this->{Crawled}{$url} = $guess;
		}#unlock
		my $agent = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
		$agent->request('GET', $url, $referer);
		$agent->{guess} = $guess;
		while (($agent->{ResponseCode}==301 || $agent->{ResponseCode}==302) && $agent->{ResponseHead}=~m/^Location:\s*(.+)\r$/mi)
		{
			last if ($guess<0);

			$referer = $url;
			$url = combine_url($url, $1);

			next BEGIN if ($this->{SubDomain} eq 'yes' ? $this->{Domain} ne get_domain($url) : $this->{Hostname} ne get_hostname($url));
			next BEGIN if ($url=~/\.(rar|zip|tar|7z|exe|dll|pdf|mp\d|docx?|pptx?|xlsx?)$/i);

			{lock $this->{Crawled};
			next BEGIN if (defined($this->{Crawled}{$url}) && ($this->{Crawled}{$url}==0 || $guess!=0));
			$url_format = $url;
			$url_format=~s/([^\&=]+)=[^\&]+/$1=/g; 
			next BEGIN if ($url ne $url_format && defined($this->{Crawled}{$url_format}) && ($this->{Crawled}{$url_format}==0 || $guess!=0));
			next BEGIN if (grep {index($url_format, $_)==0} @{$this->{WhiteUrl}});
			$linked_url++;
			$get_page++;
			print "Location: $url\n";
			$this->{Crawled}{$url} = $guess;
			}#unlock
			$agent->request('GET', $url, $referer);
		}
		my $head = $agent->{ResponseHead};
		{lock $this->{ServerVerion}; lock $this->{ScriptEngine};
		$this->{ServerVerion} = $1 if ($this->{ServerVerion} eq '' && $this->{Domain} eq get_domain($url) && $head=~m/^Server:\s*(.+)\s*$/mi);
		$this->{ScriptEngine} = $1 if ($this->{ScriptEngine} eq '' && $this->{Domain} eq get_domain($url) && $head=~m/^X-Powered-By:\s*(.+)\s*$/mi);
		}
		if ($agent->{Text}==0 || $agent->{ResponseCode}==404)
		{
			if ($url ne $url_format)
			{lock $this->{Crawled};
				$this->{Crawled}{$url_format} = $guess;
			}
			$this->{Scanner}->scan($url, $referer, $agent);
			next BEGIN;	
		}
		if ($this->{CrawlDepth}==0 || $depth<$this->{CrawlDepth})
		{
			my @links = keys %{$this->catch_links($url, $agent)};
			if ($url ne $url_format && @links==0)
			{lock $this->{Crawled};
				$this->{Crawled}{$url_format} = $guess;
			}
			foreach my $new_url (@links)
			{
				if ($new_url!~/logout[^\/]*$/i)
				{lock $this->{CatchLinked};
					$this->{CatchLinked}{$new_url} = 1;
					$this->{Queue}->enqueue([$new_url, $url, $depth+1, $guess!=0 ? ($guess>0?$guess+1:1) : 0]);
				}else
				{lock $this->{Logouted};
					push @{$this->{Logouted}}, shared_clone([$new_url, $url, $depth+1, $guess!=0 ? ($guess>0?$guess+1:1) : 0]);
				}
			}
		}

		$this->{Scanner}->scan($url, $referer, $agent);

		$this->{cms} = $agent->{cms} if ($this->{Seed} eq $url && $agent->{cms} ne '');
		foreach my $ref (@{$agent->{urls}})
		{
			my ($url_, $guess_) = (normalize_url($ref->[0]), $ref->[1]);
			$this->{Queue}->enqueue([$url_, $url, 1, $guess_]) if ($url_ ne '');
		}
	}#while
	#should not arrive here!
}

sub catch_links
{
	my $this = shift;
	my ($url, $agent) = @_;
	my $links_ref = {};
	my @caught_urls = $agent->{ResponseBody}=~m/(?<!['"])location(?:\.href)?\s*=\s*['"]?((?<=\')[^\'\r\n]+|(?<=\")[^\"\r\n]+|(?<![\'\"])[^\'\">\s][^>\s]*)/gio;
	push @caught_urls, $agent->{ResponseBody}=~m/(?:<i?frame\s+[^<>\r\n]*src|href)\s*=\s*[\'\"]?((?<=\')[^\'\r\n]+|(?<=\")[^\"\r\n]+|(?<![\'\"])[^\'\">\s][^>\s]*)/gio;
	push @caught_urls, $agent->{ResponseBody}=~m/\ssrc\s*=\s*[\'\"]?((?<=\')[^\'\r\n]+\/|(?<=\")[^\"\r\n]+\/|(?<![\'\"])[^\'\">\s][^>\s]*\/)/gio;
	push @caught_urls, $agent->{ResponseBody}=~m/<meta(?=\s)[^>]*?(?<=\s)http-equiv\s*=\s*['"]?refresh['"]?[^>]+?(?<=\b)url\s*=\s*[\'\"]?((?<=\')[^\'\r\n]+|(?<=\")[^\"\r\n]+|(?<![\'\"])[^\'\">\s]+)/gio;
URL:	foreach my $new_url (@caught_urls)
	{
		next unless (defined($new_url));

		$new_url=~s/\\\//\//g;
		$new_url=~s/&nbsp;/ /g;
		$new_url=~s/&lt;/</g;
		$new_url=~s/&gt;/>/g;
		$new_url=~s/&apos;/'/g;
		$new_url=~s/&quot;/"/g;
		$new_url=~s/&amp;/&/g;

		next if ($new_url=~m/^mailto:/i || $new_url=~m/^javascript:/i || $new_url=~m/^\\+['"]/ || $new_url=~m/['"]\s*\+/ || $new_url=~m/\+\s*['"]/);
		next if ($new_url eq '#' || $new_url eq '?');
		$new_url = combine_url($url, $new_url);
		next if ($new_url eq '');
		next if (defined($links_ref->{$new_url}));
		next if ($this->{SubDomain} eq 'yes' ? $this->{Domain} ne get_domain($new_url) : $this->{Hostname} ne get_hostname($new_url));
		next if ($this->{Hostname} eq 'test.shipin7.com' && get_hostname($new_url)=~/^(?:www|mall)\.shipin7\.com$/);
		{lock $this->{CatchLinked};
		next URL if (defined($this->{CatchLinked}{$new_url}));
		}

		my $url_path = $new_url;
		while($url_path=~m/^(https?:\/\/.+\/)./)
		{lock $this->{CatchLinked};
			$url_path = $1;
			$links_ref->{$url_path} = 1 if (!defined($this->{CatchLinked}{$url_path}));
		}

		$links_ref->{$new_url} = 1 if ($new_url!~/\.(rar|zip|tar|7z|exe|dll|pdf|mp\d|docx?|pptx?|xlsx?)$/i);
	}#foreach
	return $links_ref;
}

1; 
