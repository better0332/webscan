package web;
use strict;
use threads;
use threads::shared;
use Time::HiRes qw(time);

require Exporter;
our $VERSION = 1.00;
our @ISA = qw(Exporter);
our @EXPORT = qw(
decode_pct
stand_encode
normalize_url
combine_url
get_hostname
get_domain
host2ip
is_same_host
is_same_domain
mycurl
text2html
sethttpinfo
ctlhttp
gethttpinfo
sethttpspeed
);

my $httpspeed = undef;
my @httpinfo :shared = (0, 0);
my @httpvalve :shared = ($httpspeed, undef);

# URL = scheme ://authority path-abempty [?query] [#fragment]
# scheme = ALPHA *( ALPHA / DIGIT / "+" / "-" / "." )	// scheme = http|https
# authority = [ userinfo "@" ] host [ ":" port ]
#	 userinfo = *( unreserved / pct-encoded / sub-delims / ":" )
#		 unreserved = ALPHA / DIGIT / "-" / "." / "_" / "~"
#		 sub-delims = "!" / "$" / "&" / "'" / "(" / ")" / "*" / "+" / "," / ";" / "="
#		 pct-encoded = "%" HEXDIG HEXDIG
#	 host = IPv4address / reg-name
#		 IPv4address = dec-octet "." dec-octet "." dec-octet "." dec-octet
#		 reg-name = *( unreserved / pct-encoded / sub-delims )
#	 port = *DIGIT
# path-abempty = *( "/" segment )
#	 segment = *pchar
#		 pchar = unreserved / pct-encoded / sub-delims / ":" / "@"
# query = *( pchar / "/" / "?" )
# fragment = *( pchar / "/" / "?" )

sub decode_pct
{
	return $_[0] unless (uc($_[0])=~m/%([\dA-F])([\dA-F])/);
	my ($high, $low) = ($1, $2);
	my $high_num = ($high ge 'A' && $high le 'F')?(ord($high)-ord('A')+10):(ord($high)-ord('0'));
	my $low_num = ($low ge 'A' && $low le 'F')?(ord($low)-ord('A')+10):(ord($low)-ord('0'));
	my $asc = $high_num * 16 + $low_num;
	return chr($asc);
}

sub stand_encode
{
	my ($string, $is_host) = @_;
	$string=~s/(%[\da-fA-F]{2})/decode_pct($1)/eg;
	if ($is_host)
	{
		$string=~s/(%[\da-fA-F]{2})/decode_pct($1)/eg; #decode 2 times for trojan link
		$string = lc($string);
	}
	$string=~s/([^\w\-\.\~\;\+])/sprintf("%%%02X",ord($1))/eg;
	return $string;
}

sub stand_query
{
	my $query_string= shift;
	my @params = split(/&/, $query_string);
	my %param_list = ();
	foreach (@params)
	{
		if (/([^=]+)=(.*)/s)
		{
			my ($name, $value) = (stand_encode($1, 0), stand_encode($2, 0));
			$param_list{$name}=$value;
		}
	}
	my $query = '';
	foreach my $name (sort keys(%param_list))
	{
		$query .= $name.'='.$param_list{$name}.'&';
	}
	substr($query, -1) = '' if ($query ne '');
	return $query;
}

sub normalize_url
{
	my $original_url = shift;
	my $new_url = '';
	return $new_url if ($original_url!~m/^(https?):\/\/(?:([^@\/]*)@)?([^:\/\?\#]+)(?::(\d*))?(\/[^\?\#]*)?(?:\?([^\#]*))?/io);
	my $scheme = lc($1);
	my $userinfo = defined($2)?$2:'';
	my $host = lc($3);
	my $port = defined($4)?$4:'';
	$port = '' if ($port eq '80' && $scheme eq 'http' || $port eq '443' && $scheme eq 'https');
	my $path = defined($5)?$5:'/';
	my $query = defined($6)?$6:'';
	
	$userinfo = stand_encode($userinfo, 0);
	$host = stand_encode($host, 1);
	$host=~s/^(.*?)\.+$/$1/s;
	$path=~s/([^\/]+)/stand_encode($1, 0)/eg;
	$path=~s/\/{2,}/\//g;
	$path=~s/(?<=\/)\.(\/|$)//g;
	while ($path=~s/((?<=^\/)|[^\/]+\/)\.\.(\/|$)//o) {};
	$query = stand_query($query);
	$new_url = "$scheme://".(($userinfo ne '')?"$userinfo@":'').$host.(($port ne '')?":$port":'').$path.(($query ne '')?"?$query":'');
	return $new_url;
}

sub combine_url
{
	my ($current_url, $relative_url) = @_;
	my $new_url = '';
	$current_url = normalize_url($current_url);
	return $new_url  if ($current_url!~m/^(https?):\/\/([^\/]+)(.*\/)([^\?\#]*)/so);
	my ($scheme, $authority, $path, $filename) = ($1, $2, $3, $4);
	if ($relative_url=~m/^(https?):\/\//i)
	{
		$new_url = $relative_url;
	}elsif ($relative_url=~m/^\/\//i)
	{
		$new_url = "$scheme:$relative_url";
	}elsif ($relative_url=~m/^\//i)
	{
		$new_url = "$scheme://$authority$relative_url";
	}elsif ($relative_url=~m/^\?/i)
	{
		$new_url = "$scheme://$authority$path$filename$relative_url";
	}elsif ($relative_url=~m/^\#/i || $relative_url eq '')
	{
		$new_url = $current_url;
	}else
	{
		$new_url = "$scheme://$authority$path$relative_url";
	}
	$new_url = normalize_url($new_url);
	return $new_url;
}

sub get_hostname
{
	my $url = shift;
	my $hostname = ($url=~m/^https?:\/\/(?:[^@]*@)?([^:\/\?\#]+)(?:\.+)?/io)?lc($1):'';
	$hostname=~s/^(.*?)\.+$/$1/s;
	return $hostname;
}

sub get_domain
{
	my $url = shift;
	my $hostname = get_hostname($url);
	my $domain = '';
	if ($hostname=~m/^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})$/)
	{
		$domain = $hostname;	
	}elsif ($hostname=~m/([^\.]+\.(?:com|net|org|gov|edu)\.[a-z]{2})$/o)
	{
		$domain = $1;
	}elsif ($hostname=~m/([^\.]+\.(?:ac|bj|sh|tj|cq|he|sx|nm|ln|jl|hl|js|zj|ah|fj|jx|sd|ha|hb|hn|gd|gx|hi|sc|gz|yn|xz|sn|gs|qh|nx|xj|tw|hk|mo)\.cn)$/o)
	{
		$domain = $1;
	}elsif ($hostname=~m/([^\.]+\.[^\.]+)$/)
	{
		$domain = $1;
	}
	return $domain;
}

sub mycurl
{
	my $param_string = shift;
	my $proxy = ($param_string=~m/(https?)\\?:\\?\/\\?\/[^:\/]+\.(tw|hk|mo)(\\?:\d*)?\\?\//i)?'--proxy 172.16.11.60:8081':'';
	my $curl = "curl $proxy --silent --location --max-redirs 5 --insecure --connect-timeout 30 --max-time 90 --max-filesize 2097152 --retry 2 --retry-delay 1 --user-agent 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)'";
	my $curl_cmd = "$curl $param_string";
	my $result = `$curl_cmd`;
	while($result eq '' && `$curl http://www.baidu.com/` eq '')
	{
		sleep(3);
		$result = `$curl_cmd`;
	}
	return $result;
}

sub host2ip
{
	my $hostname = shift;
	my $ip = '';
	if ($hostname=~m/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/)
	{
		$ip = $hostname;
	}else   #get ip from hostname
	{
		my $raw_ip;
		while(!($raw_ip = gethostbyname($hostname)))
		{
			if (gethostbyname('www.baidu.com'))
			{
				last;
			}else
			{
				sleep(1);
			}
		}
		$ip = defined($raw_ip)?join('.', unpack('C4', $raw_ip)):'';
	}
	return $ip;
}

sub is_same_host
{
	my ($url1, $url2) = @_;
	my $host1 = get_hostname($url1);
	my $host2 = get_hostname($url2);
	return 1 if ($host1 eq $host2);
	my $domain1 = get_domain($url1);
	my $domain2 = get_domain($url2);
	return 0 if ($domain1 ne $domain2);
	my $ip1 =  host2ip($host1);
	my $ip2 =  host2ip($host2);
	return ($ip1 eq $ip2)?1:0;
}

sub is_same_domain
{
	my ($url1, $url2) = @_;
	my $domain1 = get_domain($url1);
	my $domain2 = get_domain($url2);
	return ($domain1 eq $domain2)?1:0;
}

sub text2html
{
	my $string = shift;
	#$string=~s/([\w\%]{60})/$1\n/g;
	$string=~s/[\t ]{2,}/ /g;
	$string=~s/\r//g;
	$string=~s/&/&amp;/g;
	$string=~s/</&lt;/g;
	$string=~s/>/&gt;/g;
	$string=~s/'/&apos;/g;
	$string=~s/"/&quot;/g;
	return $string;
}

sub sethttpinfo
{
	lock @httpinfo;
	$httpinfo[0]++;
	$httpinfo[1] += $_[0];
}

sub ctlhttp
{
	lock @httpvalve;
	if (defined($httpvalve[0]) && --$httpvalve[0]<=0)
	{
		my $t = time();
		my $interval = $t - $httpvalve[1];
		@httpvalve = ($httpspeed, $t);
		select(undef, undef, undef, 1-$interval) if ($interval<1); 
	}
}

sub gethttpinfo
{
	lock @httpinfo;

	my @info = @httpinfo;
	return @info;
}

sub sethttpspeed
{
	$httpspeed = defined($_[0])?$_[0]:undef;
}

1;
