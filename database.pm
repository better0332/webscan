package database;
use strict;
use DBI;

require Exporter;
our $VERSION = 1.00;
our @ISA = qw(Exporter);
our @EXPORT = qw(connect_db my_query my_update);

my $mysql_server = '172.16.11.60';
my $mysql_user = 'root';
my $mysql_password = 'av1amgine0';
my $database = 'webscan';

sub connect_db
{
	my $dbh = DBI->connect("DBI:mysql:$database:$mysql_server", $mysql_user, $mysql_password, {PrintError=>0, RaiseError=>0, AutoCommit=>1});
    while(!$dbh)
	{
		sleep(5);
		$dbh = DBI->connect("DBI:mysql:$database:$mysql_server", $mysql_user, $mysql_password, {PrintError=>0, RaiseError=>0, AutoCommit=>1});
	}
	$dbh->do("set names 'utf8'");
	return $dbh;
}

sub my_query
{
    my ($dbh, $sql) = @_;
    my $sth = $dbh->prepare($sql) or die($dbh->err.':'.$dbh->errstr);
    $sth->execute();
    while(defined($sth->err) && $sth->err == 2006) #error 2006: can not connect mysql server
    {
        $dbh = $_[0] = connect_db();
        $sth = $dbh->prepare($sql) or die($dbh->err.':'.$dbh->errstr);
        $sth->execute();  
    }    
    return $sth;
}

sub my_update
{
    my ($dbh, $sql) = @_;
    my $rc;
    $rc = $dbh->do($sql);
    while(defined($dbh->err) && $dbh->err == 2006) #error 2006: can not connect mysql server
    {
        $dbh = $_[0] = connect_db();
        $rc = $dbh->do($sql);
    }    
    if (defined($dbh->err) or $rc eq '0E0'){return 0;} else {return $rc;}    
}

1;
