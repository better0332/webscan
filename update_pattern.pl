#!/usr/bin/perl -w
use strict;

BEGIN
{
	my $path;
	if ($^O eq 'linux')
	{
		if ($0=~m/^(.+)\//) { $path = $1; } else { $path = readpipe('pwd'); chomp($path); }
		unshift(@INC, $path);
		chdir($path);
	}else
	{
		die("Please run this script on linux.\n");   
	}
}

use database;

sub update_pattern
{
	my $pattern = '';
	my $dbh = connect_db();
	my $sql = "select rule from webmon.malicious_rules";
	my $sth = my_query($dbh, $sql);
	while (my $record = $sth->fetchrow_hashref())
	{
		$pattern .= $record->{rule}.'|' if($record->{rule}!~/^\s*$/s);
	}
	$sth->finish;
	$dbh->disconnect;
	substr($pattern, -1) = '' if ($pattern ne '');
	open(FH, '>', 'pattern.def') or die "Can not write file: pattern.def";
	print FH $pattern;
	close(FH);
}

sub update_whitelist
{
	my $whitelist = '';
	my $dbh = connect_db();
	my $sql = "select domain from webmon.safe_site";
	my $sth = my_query($dbh, $sql);
	while (my $record = $sth->fetchrow_hashref())
	{
		$whitelist .= $record->{domain}.'|';
	}
	$sth->finish;
	$dbh->disconnect;
	substr($whitelist, -1) = '' if ($whitelist ne '');
	open(FH, '>', 'whitelist.def') or die "Can not write file: whitelist.def";
	print FH $whitelist;
	close(FH);
}

while(1)
{
	update_pattern();
	update_whitelist();
	sleep(600);
}
