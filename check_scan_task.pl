#!/usr/bin/perl -w
use strict;
BEGIN
{
    my $path;
    if ($^O eq 'linux')
    {
        if ($0=~m/^(.+)\//) { $path = $1; } else { $path = readpipe('pwd'); chomp($path); }
        unshift(@INC, $path);
        chdir($path);
    }else
    {
        die("Please run this script on linux.\n");   
    }
}

use database;

sub check_scan_task
{
	my $max_scan_process_count = 6;
	my $dbh = connect_db();
	my $sql = "select count(*) as scanning from scan_task where status='scanning'";
	my $sth = my_query($dbh, $sql);
	my @record = $sth->fetchrow_array();
	my $scaning_count = $record[0]; 
	$sth->finish();
	
	my $usable_process_count = $max_scan_process_count - $scaning_count;
	if ($usable_process_count <= 0)
	{
	    $dbh->disconnect();
	    return;   
	}

	$sql = "select submit_id from scan_task where status='waiting' order by priority desc, submit_id limit $usable_process_count";
	$sth = my_query($dbh, $sql);
	while (my @record = $sth->fetchrow_array())
	{
		my $submit_id = $record[0];
		$sql = "update scan_task set status='scanning' where submit_id=$submit_id and status='waiting'";
		if (my_update($dbh, $sql))
		{
			system("./scan_task.pl --id=$submit_id >/dev/null 2>/dev/null &");
		}
	}
	$sth->finish(); 
	$dbh->disconnect();
}

sub reset_status
{
	my $dbh = connect_db();
	my $sql = "update scan_task set status='waiting' where status='scanning'";
	my_update($dbh, $sql);
	$dbh->disconnect();
}

reset_status();
while(1)
{
	check_scan_task();
	sleep(10);
}
