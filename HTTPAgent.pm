package HTTPAgent;
use strict;
use Encode::HanExtra;
use Encode;
use web;
use Report;

sub new
{
	my $type = shift;
	my $class = ref($type) || $type;
	my $this = {};
	bless($this, $class);	 
	$this->initialize(@_);
	return $this;
}

sub initialize
{
	my $this = shift;
	my %param = @_;
	$this->{urls} = [];
	$this->{cms} = '';
	$this->setProxy($param{Proxy});
	$this->setCookie($param{Cookie});
	$this->setContentType($param{ContentType});
	$this->setAuth($param{Auth});
}

sub setProxy
{
	my $this = shift;
	$this->{Proxy} = defined($_[0])?$_[0]:'';
}

sub setCookie
{
	my $this = shift;
	$this->{Cookie} = defined($_[0])?$_[0]:'';
}

sub setContentType
{
	my $this = shift;
	$this->{ContentType} = $_[0];
}

sub setAuth
{
	my $this = shift;
	$this->{Auth} = defined($_[0])?$_[0]:'';
}

sub addCookie
{
	my $this = shift;
	$this->{Cookie} = $this->{Cookie}.' ;'.$_[0] if (defined($_[0]) && $_[0] ne '');
}

sub get_utf8_var
{
	my $this = shift;
	my ($var) = @_;
	return lc($this->{Charset}) eq 'utf-8' ? $var : encode('utf-8', decode($this->{Charset}, $var));
}

sub get_utf8_request
{
	my $this = shift;
	return lc($this->{Charset}) eq 'utf-8' ? $this->{RequestHead} : encode('utf-8', decode($this->{Charset}, $this->{RequestHead}));
}

sub get_utf8_response
{
	my $this = shift;
	#return lc($this->{Charset}) eq 'utf-8' ? $this->{ResponseHead} : encode('utf-8', decode('utf-8', $this->{ResponseHead}));
	return lc($this->{Charset}) eq 'utf-8' ? $this->{ResponseHead} : encode('utf-8', $this->{ResponseHead});
}

sub get_utf8_body
{
	my $this = shift;
	my ($length) = @_;
	if (!defined($length))
	{
		return lc($this->{Charset}) eq 'utf-8' ? \$this->{ResponseBody} : \encode('utf-8', decode($this->{Charset}, $this->{ResponseBody}));
	}else
	{
		if ($length>=0)
		{
			if (lc($this->{Charset}) eq 'utf-8')
			{
				Encode::_utf8_on($this->{ResponseBody});
				my $ResponseBody = substr($this->{ResponseBody}, 0, $length);
				Encode::_utf8_off($this->{ResponseBody});
				Encode::_utf8_off($ResponseBody);
				return \$ResponseBody;
			}else
			{
				return \encode('utf-8', substr(decode($this->{Charset}, $this->{ResponseBody}), 0, $length));
			}
		}else
		{
			if (lc($this->{Charset}) eq 'utf-8')
			{
				Encode::_utf8_on($this->{ResponseBody});
				my $ResponseBody = substr($this->{ResponseBody}, $length);
				Encode::_utf8_off($this->{ResponseBody});
				Encode::_utf8_off($ResponseBody);
				return \$ResponseBody;
			}else
			{
				return \encode('utf-8', substr(decode($this->{Charset}, $this->{ResponseBody}), $length));
			}
		}
	}
}

sub request
{
	my $this = shift;
	my ($method, $url, $referer, $data) = @_;
	($this->{RequestHead}, $this->{ResponseHead}, $this->{ResponseBody}, $this->{Charset}) = ('', '', '', '');
	($this->{ResponseCode}, $this->{Text}, $this->{Static}, $this->{CurlCode}) = (0, 0, 0, 255);
	return if (!defined($method) || !defined($url) || $url eq '' || $method!~m/^(HEAD|GET|POST)$/);
	#return if (($method eq 'POST') && (!defined($data) || $data eq ''));

	$this->{url} = $url;
	my $hostname = get_hostname($url);
	my $path = ($url=~m/^(?:http|https):\/\/[^\/]+(\/.*)$/i)?$1:'/';
	$referer = ($url=~m/^((?:http|https):\/\/[^\/]+\/?)/i)?$1:'' if (!defined($referer) || $referer eq '');
	$data = '' if (!defined($data));
	my $user_agent = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)";
	my $request_head = "$method $path HTTP/1.1\r\n";
	$request_head .= "Host: $hostname\r\n";
	$request_head .= "Authorization: *****\r\n" if ($this->{Auth} ne '');
	$request_head .= "User-Agent: $user_agent\r\n";
	$request_head .= "Accept: */*\r\n";
	$request_head .= "Accept-Language: zh-cn\r\n";
	$request_head .= "Referer: $referer\r\n";
	if ($method eq 'POST')
	{
		$request_head .= defined($this->{ContentType}) ? "Content-Type: ".$this->{ContentType}."\r\n" : "Content-Type: application/x-www-form-urlencoded\r\n";
		$request_head .= "Content-Length: ".length($data)."\r\n";
	}
	$request_head .= "Cookie: ".$this->{Cookie}."\r\n" if ($this->{Cookie} ne '');
	$request_head .= "\r\n$data" if ($method eq 'POST');
	$this->{RequestHead} = $request_head;
	
	my $curl .= "curl --silent --connect-timeout 15 --max-time 90 --retry 2 --retry-delay 1 --max-filesize 2097152";
	my $options = "--user-agent '$user_agent' --header 'Accept-Language: zh-cn' --referer ".quotemeta($referer);
	$options .= ($url=~m/^https:\/\//i)?" --insecure":'';
	$options .= ($this->{Proxy} ne '')?(" --proxy ".quotemeta($this->{Proxy})):'';
	$options .= ($this->{Auth} ne '')?(" --user ".quotemeta($this->{Auth})):'';
	$options .= " --header ".quotemeta('Content-Type: '.$this->{ContentType}) if ($method eq 'POST' && defined($this->{ContentType}));
	$options .= " --data ".join("'\n'", map(quotemeta, split(/\n/, $data))) if ($method eq 'POST' && defined($data));
	$options .= " --cookie ".quotemeta($this->{Cookie}) if ($this->{Cookie} ne '');
	$options .= ($method eq 'HEAD')?" --head":" --include";
	my $curl_cmd = "$curl $options ".quotemeta($url);
	#print "$curl_cmd\n";
	my $result = `$curl_cmd`;
	$this->{CurlCode} = $?;
	sethttpinfo(length($result));
	ctlhttp();
	($this->{ResponseHead}, $this->{ResponseBody}) = ($1, $2) if ($result=~m/^(.+?\r\n)\r\n(.*)$/s);
        $this->{ResponseHead} =~ s/[\x80-\xff]//g;
	$this->{ResponseCode} = ($this->{ResponseHead}=~m/^HTTP\/\d+\.\d+\s+(\d+)/i)?$1:0;
	$this->{Text} = ($this->{ResponseHead}=~m/^Content-Type:\s*.*text\//mi)?1:0;
	$this->{Static} = ($this->{ResponseHead}=~m/^ETag:/mi || $this->{ResponseHead}=~m/^Last-Modified:/mi)?1:0;
	
	my $charset = ($this->{ResponseHead}=~m/^Content-Type: .*charset\s*=\s*([\w\-]+)/mi)?lc($1):'';
	$charset = '' if ($charset eq 'null');
	$charset = lc($1) if($charset eq '' && $this->{ResponseBody}=~m/<meta(?=\s)[^>]*?(?<=\s)content\s*=\s*[^>]*?(?<=\s)charset\s*=\s*([\w\-]+)/mi);
	$charset = 'gbk' if ($charset eq 'x-gbk');
	$charset = 'gb2312' if ($charset!~m/^(gb2312|gbk|gb18030|big5|utf-8|hz|iso-8859-1|big5-hkscs|euc-tw)$/i);
	$this->{Charset} = $charset;
}

1;
