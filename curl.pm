package curl;

BEGIN{unshift(@INC, $1) if ($0=~m/(.+)\//);}
use warnings;
use strict;
use threads;
use threads::shared;
use LWP::UserAgent;
use HTTP::Request;
use HTTP::Cookies;
use LWP::Protocol::http;
use Net::SSL (); # used for https proxy

use Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw(new_agent curl);

sub new_agent
{
    my (%param) = @_;
    my $ua = LWP::UserAgent->new(keep_alive=>1);
    
    $ua->agent($param{'agent'}) if $param{'agent'};
    $ua->timeout($param{'timeout'}) if $param{'timeout'};
    $ua->ssl_opts(verify_hostname => 0); # insecure
    $ua->proxy(['http'], "$param{'proxy'}") if $param{'proxy'}; # proxy; but https cannot make sense
    $ENV{HTTPS_PROXY} = $param{'proxy'} if $param{'proxy'}; # https use proxy
    $ua->local_address($param{'local_address'}) if $param{'local_address'}; # use a specified local address

    return $ua;
}

sub curl
{
    my ($ua, $url, %param) = @_;
    die("No user agent.\n") unless $ua;
    die("You must specify url.\n") unless $url;
    my $retry_times = 3;
    my $retry_delay = 5;
    my $req_method = 'GET';

    #my $ua = LWP::UserAgent->new(keep_alive=>1);

    # setup http request headers
    $ua->agent($param{'agent'}) if $param{'agent'};
    $ua->timeout($param{'timeout'}) if $param{'timeout'};
    $ua->max_size($param{'max_size'}) if $param{'max_size'};
    $ua->cookie_jar($param{'cookie_jar'}) if $param{'cookie_jar'}; # cookie jar
    $ua->default_header('Cookie' => $param{'cookie'}) if $param{'cookie'}; # key=value string cookie

    $ua->default_header('Referer' => $param{'referer'}) if $param{'referer'}; # referer
    $ua->ssl_opts(verify_hostname => 0); # insecure
    $ua->proxy(['http'], "$param{'proxy'}") if $param{'proxy'}; # proxy; but https cannot make sense
    $ENV{HTTPS_PROXY} = $param{'proxy'} if $param{'proxy'}; # https use proxy
    $ua->local_address($param{'local_address'}) if $param{'local_address'}; # use a specified local address
    push(@LWP::Protocol::http::EXTRA_SOCK_OPTS, SendTE => 0); # get rid of TE header

    $param{'accept_language'} ? 
        $ua->default_header('Accept-Language' => $param{'accept_language'})
        : $ua->default_header('Accept-Language' => 'zh-cn');

    $retry_times = $param{'retry_times'} if $param{'retry_times'};
    $retry_delay = $param{'retry_delay'} if $param{'retry_delay'};
    $req_method = uc $param{'req_method'} if $param{'req_method'};
    @{$ua->requests_redirectable} = ();
    @{$ua->requests_redirectable} = $param{'redirect_method'} if $param{'redirect_method'};

    # dispatch a request on the given url
    my $i = 0;
    my $response;
    my ($reqh, $code, $head, $content) = ('', '', '', '');
    my $req = HTTP::Request->new($req_method => $url);
    if ($req_method eq 'POST')
    {
	die("Please specify post data.\n") unless $param{'data'};
	$req->content_type('application/x-www-form-urlencoded');
	$req->content($param{'data'});
    }
    $ua->add_handler(request_prepare => sub {$reqh = $req->headers_as_string;});

    while ($i < $retry_times)
    {
        $response = $ua->request($req);
        if ($response)
        {
            $code = $response->code;
            #$head = $response->header('Content-Type'); # Get specified header
            my $status = $response->status_line;
            $head = $response->headers()->as_string;
            $head = "HTTP/1.1 ".$status."\n".$head;
            $content = $response->content;
            my $rq_path = $req->uri->path_query;
            $rq_path = "/" unless $rq_path;
            my $rq_line = $req->method." ".$rq_path." "."HTTP/1.1\n";
            $reqh = $rq_line."Host: ".$req->uri->host."\nConnection: Keep-Alive\nKeep-Alive: 300\n".$reqh;
            $reqh.= "Content-Length: ".length($param{'data'})."\n\n$param{'data'}" if ($req_method eq 'POST');
            #print "Cookie2: ".$param{'cookie_jar'}->as_string."\n";
            last;
        } else
        {
            sleep $retry_delay;
            $i++;
        }
    }
    return ($reqh, $code, $head, $content);
}

1;
