package Scanner;
use strict;
use threads;
use threads::shared;
use bytes;#can't remove!!
use Digest::MD5;
use web;
use cms;
use HTTPAgent;
use Report;
use Malsite;
use cms_vul;

#my @list_dir = ('admin', 'manage', 'upload', 'uploadfile', 'uploadfiles', 'down', 'download', 'include', 'data', 'bak', 'back', 'backup', 'test', 'inc', 'temp', 'tmp', 'database', 'db', 'editor', 'eWebEditor', 'FCKeditor', 'config', 'user', 'users', '.svn', '.git', 'system', 'product', 'info', 'company', 'home', 'demo', 'web', 'website', 'article', 'old', 'new', 'news', 'conn', 'template', 'bin', 'cms', 'common', 'cn', 'en', 'tw', 'code', 'default', 'domain', 'forum', 'html', 'images', 'img', 'install', 'setup', 'help', 'link', 'log', 'login', 'local', 'main', 'other', 'photo', 'photos', 'php', 'plugins', 'blog', 'public', 'pub', 'js', 'javascript', 'service', 'services', 'shell', 'shop', 'support', 'sys', 'tool', 'tools', 'topics', 'oa', 'access', 'account', 'ad', 'bbs', 'webadmin');
my @list_dir = ('admin', 'manage', 'upload', 'uploadfile', 'uploadfiles', 'down', 'download', 'include', 'data', 'bak', 'back', 'backup', 'test', 'inc', 'temp', 'tmp', 'database', 'db', 'editor', 'eWebEditor', 'FCKeditor', 'config', 'user', 'users', '.svn', '.git', 'system', 'info', 'old', 'conn', 'template', 'bin', 'cms', 'install', 'setup', 'help', 'log', 'login', 'local', 'other', 'plugins', 'shell', 'sys', 'tool', 'tools', 'access', 'account', 'ad');

my @list_cgi = ('login', 'admin', 'admin_login', 'config', 'upload', 'upload_pic', 'admin_upload', 'upload_article', 'upload_news', 'user_upload', 'upfile', 'upfile_pic', 'user_upfile', 'admin_upfile', 'shell', 'webshell', 'editor', 'ad_edit', 'ad_manage', 'ad_login', 'sa', 'check_server', 'check', 'admin_login', 'upic', 'sql', '1', '0', '01', '11', '123', 'aa', 'a', 'access', 'account', 'add_user', 'add_admin', 'add_news', 'add_article', 'info', 'fso', 'setup', 'admin_uppic', 'asp', 'php', 'backdata', 'backup', 'code', 'user', 'sys', 'system', 'diy', 'test', 'base', 'cmd', 'db', 'my', 'tmp', 'temp', 'file_upload', 'conn');
my @list_txt = ('readme.txt', 'README', 'readme.html', 'CHANGES.txt', 'CHANGES', 'CHANGES.html', 'phpinfo.php', 'version.txt', 'db.inc', 'db.rar', 'db.zip', 'config', 'config.txt', 'config.inc', 'a.txt', 'a.rar', 'a.zip', '1.txt', '1.rar', '1.zip', 'data.zip', 'data.rar', 'data.mdb', '安装.txt', '安装说明.txt', '安装须知.txt', '版权说明.txt', '版本说明.txt', '版权声明.txt', '配置说明.txt', '升级.txt', '说明.txt', '用户手册.txt', '修改说明.txt', '密码.txt', '新建 文本文档.txt', 'New Text Document.txt', 'robots.txt', 'crossdomain.xml');

my $regex_err = 'You have an error in your SQL syntax|Database error:.{1,8}Invalid SQL:|Microsoft OLE DB Provider for|Microsoft JET Database Engine|ORA-01756|Incorrect syntax near';

my %inject_sig = (
		#	1 => [q{%0DaNd%0D1}               , q{%0DaNd%0D0}], 
			2 => [q{'%0DaNd's'''%0DlIkE's''}  , q{'%0DaNd's'''%0DlIkE'w''}], #支持字符型和搜索型
			3 => [q{%0D-%0D000000}            , q{%0D-%0D999999}],
		#	4 => [q{'%0DaNd's''%'%0DlIkE's''} , q{'%0DaNd's''%'%0DlIkE'w''}] #access not support %
		 ); #sort keys no more than 10

my %inject_time_sig = (
			1 => q{%0DaNd%0Dsleep(5)}, 
			2 => q{'aNd%0Dsleep(5)='0}, 
			3 => q{;waitfor%0Ddelay%0D'0:0:5'--}, 
			4 => q{';waitfor%0Ddelay%0D'0:0:5'--}
		      ); #sort keys no more than 10

my %xss_sig = (
			1 => q{<hikvision0317>}, 
			2 => q{'"hikvision0906'"}
	      ); #sort keys no more than 10

my %err_sig = (
			1 => q{'"}, 
			2 => q{%B2'%B2"}, 
			3 => q{%2527%2522}
	      ); #sort keys no more than 10

sub new
{
	my $type = shift;
	my $class = ref($type) || $type;
	my $this = {};
	bless($this, $class);
	$this->initialize(@_);
	return $this;
}

sub initialize
{
	my $this = shift;
	my %param = @_;
	$this->{script} = shared_clone({});
	$this->{page404} = shared_clone({});
	$this->setProxy($param{Proxy});
	$this->setCookie($param{Cookie});
	$this->setCrawlBit($param{CrawlBit});
	$this->{report} = new Report();
	$this->{malsite} = new Malsite();
	$this->{keyword} = '';
	$this->{url_filter} = '';
}

sub setProxy
{
	my $this = shift;
	my $proxy = defined($_[0])?$_[0]:'';
	$this->{Proxy} = $proxy;
}

sub setCookie
{
	my $this = shift;
	my $cookie = defined($_[0])?$_[0]:'';
	$this->{Cookie} = $cookie;
}

sub setCrawlBit
{
	my $this = shift;
	my $crawl_bit = defined($_[0])?$_[0]:'comm';
	$this->{CrawlBit} = $crawl_bit;
}

sub scan
{
	my $this = shift;
	my ($url, $referer, $data) = @_;
	my $scan_bit = $this->{CrawlBit};

	$this->check_cms($url, $referer, $data)                 if ($scan_bit=~/^(min|comm|all)$/i     || $scan_bit=~/^[01]+$/ && substr($scan_bit, 0, 1));
        $this->check_cms_vul($url, $referer, $data)             if ($scan_bit=~/^(min|comm|all)$/i     || $scan_bit=~/^[01]+$/ && substr($scan_bit, 1, 1));
	$this->check_hacked_page($url, $data)                   if ($scan_bit=~/^(min|comm|all)$/i     || $scan_bit=~/^[01]+$/ && substr($scan_bit, 2, 1));
	$this->check_malicious_site($url, $data)                if ($scan_bit=~/^(min|comm|all)$/i     || $scan_bit=~/^[01]+$/ && substr($scan_bit, 3, 1));
	$this->check_seo_adware($url, $data)                    if ($scan_bit=~/^(min|comm|all)$/i     || $scan_bit=~/^[01]+$/ && substr($scan_bit, 4, 1));
	$this->check_sql_injection($url, $referer, $data)       if ($scan_bit=~/^(hik|min|comm|all)$/i || $scan_bit=~/^[01]+$/ && substr($scan_bit, 5, 1));#see check_form!!
	$this->check_xss_attack($url, $referer, $data)          if ($scan_bit=~/^(hik|min|comm|all)$/i || $scan_bit=~/^[01]+$/ && substr($scan_bit, 6, 1));#see check_form!!
	$this->check_app_error($url, $referer, $data)           if ($scan_bit=~/^(hik|min|comm|all)$/i || $scan_bit=~/^[01]+$/ && substr($scan_bit, 7, 1));#see check_form!!
	$this->check_form($url, $data);                         #scan_bit in function!!
	$this->check_code_leak($url, $data)                     if ($scan_bit=~/^(hik|comm|all)$/i     || $scan_bit=~/^[01]+$/ && substr($scan_bit, 8, 1));
	$this->check_directory_traversal($url, $referer, $data) if ($scan_bit=~/^(hik|comm|all)$/i     || $scan_bit=~/^[01]+$/ && substr($scan_bit, 9, 1));
	$this->check_directory_explore($url, $data)             if ($scan_bit=~/^(hik|comm|all)$/i     || $scan_bit=~/^[01]+$/ && substr($scan_bit, 10, 1));
	$this->check_internal_directory($url, $referer, $data)  if ($scan_bit=~/^(hik|comm|all)$/i     || $scan_bit=~/^[01]+$/ && substr($scan_bit, 11, 1));
	$this->check_internal_file($url, $referer, $data)       if ($scan_bit=~/^(hik|comm|all)$/i     || $scan_bit=~/^[01]+$/ && substr($scan_bit, 12, 1));
	$this->check_webdav($url, $data)                        if ($scan_bit=~/^(hik|comm|all)$/i     || $scan_bit=~/^[01]+$/ && substr($scan_bit, 13, 1));
	$this->check_login_page($url, $data)                    if ($scan_bit=~/^(hik|comm|all)$/i     || $scan_bit=~/^[01]+$/ && substr($scan_bit, 14, 1));
	$this->check_xss_bom_attack($url, $referer, $data)      if ($scan_bit=~/^(comm|all)$/i         || $scan_bit=~/^[01]+$/ && substr($scan_bit, 15, 1));
	$this->check_invalid_link($url, $referer, $data)        if ($scan_bit=~/^(all)$/i              || $scan_bit=~/^[01]+$/ && substr($scan_bit, 16, 1));
	$this->check_internal_ip($url, $data)                   if ($scan_bit=~/^(all)$/i              || $scan_bit=~/^[01]+$/ && substr($scan_bit, 17, 1));
	$this->check_email_leak($url, $data)                    if ($scan_bit=~/^(all)$/i              || $scan_bit=~/^[01]+$/ && substr($scan_bit, 18, 1));
	$this->check_keyword($url, $data)                       if ($scan_bit=~/^(all)$/i              || $scan_bit=~/^[01]+$/ && substr($scan_bit, 19, 1));
	$this->check_url_filter($url, $data)                    if ($scan_bit=~/^(hik|all)$/i          || $scan_bit=~/^[01]+$/ && substr($scan_bit, 20, 1));
}

sub is_page404
{
	my $this = shift;
	my ($url, $data) = @_;
	#如果404页面返回类似200响应，并且没有title，内容是动态的（排除文件名），可能误判成正常页面
	#如果404页面返回类似200响应，并且是检测页面和错误页面都是空的，可能误判成错误页面
	#是404页面返回1
	#不是404页面返回0
	#目录文件返回-2
	#不能判断的页面返回-1

	return 1  if ($data->{ResponseCode}==404);
	return -1 if ($data->{ResponseCode}<200 || $data->{ResponseCode}>500);

	my ($path) = $url=~m/(.+\/)/ or return -1;
	my ($hostname, $dir, $file) = $url=~m/^(https?:\/\/[^\/]+\/)(.*\/)?([^\?]*)(\?|$)/o?($1, $2, $3):('', '', '', '');
	my $script = $file=~m/\.([^\.\/]*)$/?$1:'';

	my $notexist;
	{lock $this->{page404};
		$notexist = !defined($this->{page404}{$path}) || !defined($this->{page404}{$path}{$script});
	}
	my ($agent, $file_, $url_, $location, $bodylength, $title);
	if ($notexist)
	{
		$file_ = 'asdf342sf7872';
		$file_ = "$file_.$script" if ($script ne '');
		$url_ = $hostname.$dir.$file_;
		$agent = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
		$agent->request('GET', $url_, $url); 
		return -1 if ($agent->{CurlCode}!=0 || $agent->{ResponseHead} eq '');
		if (($agent->{ResponseCode}==301 || $agent->{ResponseCode}==302) && $agent->{ResponseHead}=~m/^Location:\s*([^\?;\r]+).*\r$/mi)
		{
			$location = $1;
			$location = 'maywaf' if (!is_same_domain($url_, combine_url($url_, $location)));
			#排除运营商或WAF的干扰！！
		}else
		{
			$location = '';
		}
		$bodylength = length($agent->{ResponseBody});
		$title = $agent->{ResponseBody}=~m/<title(?:\s[^>]*)?>([^<]+)<\/title>/i?$1:'';
		$title=~s/$file_//g;
	}
	lock $this->{page404};
	if (!defined($this->{page404}{$path}) || !defined($this->{page404}{$path}{$script}))
	{
		$this->{page404}{$path} = shared_clone({}) unless (defined($this->{page404}{$path}));
		$this->{page404}{$path}{$script} = shared_clone({ResponseCode => $agent->{ResponseCode}, 
								 #ResponseHead => $agent->{ResponseHead}, 
								 #ResponseBody => $agent->{ResponseBody}, 
								 ResponseBodyMD5 => Digest::MD5::md5_hex($agent->{ResponseBody}), 
								 BodyLength => $bodylength, 
								 Location => $location, 
								 Title => $title});
	}

	return -1 if ($this->{page404}{$path}{$script}{BodyLength}>50*1024);
	if ($data->{ResponseCode}==$this->{page404}{$path}{$script}{ResponseCode})
	{
		if (($data->{ResponseCode}==301 || $data->{ResponseCode}==302) && $data->{ResponseHead}=~m/^Location:\s*([^\?;\r]+).*\r$/mi)
		{
			return -1 if ($this->{page404}{$path}{$script}{Location} eq 'maywaf');
			return 1  if ($1 eq $this->{page404}{$path}{$script}{Location});
		}else
		{
			return -1 if ($data->{CurlCode}!=0);
			return 1 if (Digest::MD5::md5_hex($data->{ResponseBody}) eq $this->{page404}{$path}{$script}{ResponseBodyMD5});
			if (abs(length($data->{ResponseBody})-$this->{page404}{$path}{$script}{BodyLength})<256 && $data->{ResponseBody}=~m/<title(?:\s[^>]*)?>([^<]+)<\/title>/i)
			{
				my ($title, $title404) = ($1, $this->{page404}{$path}{$script}{Title});
				$title=~s/[ +]|%20|%2B//ig;
				if ($url=~/([^\/]+)$/)
				{
					my $org_file = $1;
					$org_file=~s/[ +]|%20|%2B//ig;
					my $quote_org_file = quotemeta($org_file);
					$title=~s/$quote_org_file//g;
				}
				$title404=~s/[ +]|%20|%2B//ig;
				return 1 if ($title eq $title404);
			}
		}
	}
	return 0;
}

sub check_hacked_page
{
	my $this = shift;
	my ($url, $data) = @_;
	my $issue_id = 30008;
	if ($data->{ResponseCode}==200 && $data->{Text}==1 && $url!~/^https?:\/\/([^\/]+\/){3}/ && $url=~m/[^\?]+(\.aspx?|\.php|\.jsp|\.s?html?|\.stm|\/)$/i && length($data->{ResponseBody})<10240)
	{
		{lock $this->{report}->{tested};
		return if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$url}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$url} = 1;
		}

		my $hacked = $this->{malsite}->check_hacked_page($data->get_utf8_body(1024));
		if ($hacked ne '')
		{lock $this->{report}->{issue};
			$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
			$this->{report}->{issue}{$issue_id}{$url} = shared_clone({request => $data->get_utf8_request()});
			$this->{report}->{issue}{$issue_id}{$url}{response} = $data->get_utf8_response();
			$this->{report}->{issue}{$issue_id}{$url}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, "\n".$hacked);
			print "<!-- $url found hacked page --!>\n";
		}
	}
}

sub check_malicious_site
{
	my $this = shift;
	my ($url, $data) = @_;
	my $issue_id = 30006;
	if ($data->{ResponseCode}==200 && $data->{Text}==1)
	{
		my ($base_url) = $url=~m/^([^\?]+)/;
		{lock $this->{report}->{tested};
		return if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$base_url}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$base_url} = 1;
		}

		my $malsite = $this->{malsite}->check_malsite($data->get_utf8_body());
		if ($malsite ne '')
		{lock $this->{report}->{issue};
			$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
			$this->{report}->{issue}{$issue_id}{$base_url} = shared_clone({request => $data->get_utf8_request()});
			$this->{report}->{issue}{$issue_id}{$base_url}{response} = $data->get_utf8_response();
			$this->{report}->{issue}{$issue_id}{$base_url}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, "\n".$malsite);
			print "<!-- $base_url found malicious --!>\n";
		}
	}
}

sub check_seo_adware
{
	my $this = shift;
	my ($url, $data) = @_;
	my $issue_id = 30005;
	if ($data->{ResponseCode}==200 && $data->{Text}==1)
	{
		my ($base_url) = $url=~m/^([^\?]+)/;
		{lock $this->{report}->{tested};
		return if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$base_url}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$base_url} = 1;
		}

		my $seo = $this->{malsite}->check_seo_adware($data->get_utf8_body(), get_domain($url));
		if ($seo ne '')
		{lock $this->{report}->{issue};
			$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
			$this->{report}->{issue}{$issue_id}{$base_url} = shared_clone({request => $data->get_utf8_request()});
			$this->{report}->{issue}{$issue_id}{$base_url}{response} = $data->get_utf8_response();
			$this->{report}->{issue}{$issue_id}{$base_url}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, "\n".$seo);
			print "<!-- $base_url found adware --!>\n";
		}
	}
}

sub check_sql_injection
{
	my $this = shift;
	my ($url, $referer, $data) = @_;
	return if ($data->{ResponseCode}!=200 || $data->{Static}==1 || index($url, '?')<0);

	my $issue_id = 30001;
	my ($base_url, $query_string) = $url=~m/^([^\?]+)\?(.+)/;
	my @params = split(/&/, $query_string);
PARAM:	foreach my $param (@params)
	{
		my ($name, $value) = $param=~m/(.+)=(.*)/;
		my $target = $base_url." => ".$name;
		{lock $this->{report}->{tested};
		next PARAM if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$target}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$target} = 1;
		}

		my $quote_param = quotemeta($param);
		foreach my $sigid (sort keys %inject_sig)
		{
			next if (($value!~/^\d+$/ || $value eq '') && ($sigid==1 || $sigid==3));
			my ($test_code1, $test_code2) = map $value.$_, @{$inject_sig{$sigid}};
			my $length0 = $data->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:
								$data->{CurlCode}==0 && $data->{ResponseBody}!~/HTTP\/\d\.\d 200 OK\r\n/?
									length($data->{ResponseBody}):goto COOKIE;

			my $agent1 = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
			my $test_query1 = $query_string;
			my $test_param1 = "$name=".stand_encode($test_code1, 0);
			$test_query1=~s/$quote_param/$test_param1/;
			my $test_url1 = "$base_url?$test_query1";
			$agent1->request('GET', $test_url1, $referer);
			goto COOKIE if ($agent1->{ResponseCode}!=200);
			my $length1 = $agent1->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:
								$agent1->{CurlCode}==0 && $agent1->{ResponseBody}!~/HTTP\/\d\.\d 200 OK\r\n/?
									length($agent1->{ResponseBody}):goto COOKIE;
			my $response_body1 = $agent1->{ResponseBody};

			my $agent2 = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
			my $test_query2 = $query_string;
			my $test_param2 = "$name=".stand_encode($test_code2, 0);
			$test_query2=~s/$quote_param/$test_param2/;
			my $test_url2 = "$base_url?$test_query2";
			$agent2->request('GET', $test_url2, $referer);
			goto COOKIE if ($agent2->{ResponseCode}!=200);
			my $length2 = $agent2->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:
								$agent2->{CurlCode}==0 && $agent2->{ResponseBody}!~/HTTP\/\d\.\d 200 OK\r\n/?
									length($agent2->{ResponseBody}):goto COOKIE;
			my $response_body2 = $agent2->{ResponseBody};

			if (abs($length1 - $length2) - abs($length0 - $length1)>20)
			{
				goto COOKIE if (($base_url=~m/search.{0,8}$/i || $name=~m/search|^q$/i) && abs($length1 - $length2) - abs($length0 - $length1)<200);
				lock $this->{report}->{issue};
				$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
				my $request0 = $data->get_utf8_request();
				my $request1 = $agent1->get_utf8_request();
				my $request2 = $agent2->get_utf8_request();
				my $response_head0 = $data->get_utf8_response();
				my $response_head1 = $agent1->get_utf8_response();
				my $response_head2 = $agent2->get_utf8_response();
				$this->{report}->{issue}{$issue_id}{$target} = shared_clone({request => "原始请求:\n$request0\n注入真条件请求:\n$request1\n注入假条件请求:\n$request2"});
				$this->{report}->{issue}{$issue_id}{$target}{response} = "原始响应:\n$response_head0\n注入真条件响应:\n$response_head1\n注入假条件响应:\n$response_head2";
				$this->{report}->{issue}{$issue_id}{$target}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $name);
				print "<!-- $target found sql injection(sigid=$sigid) --!>\n";

				$target=~s/\//\\/g;
				open HFILE, "> $target-sql-$length0-(0).html" or die $!;
				print HFILE $data->{ResponseBody};
				close HFILE;
				open HFILE, "> $target-sql-$length1-(1).html" or die $!;
				print HFILE $response_body1;
				close HFILE;
				open HFILE, "> $target-sql-$length2-(2).html" or die $!;
				print HFILE $response_body2;
				close HFILE;

				last;
			}#if

COOKIE:			my $test_query = $query_string;
			$test_query=~s/$quote_param//;
			my $test_url = "$base_url?$test_query";

			$agent1 = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
			$test_param1 = "$name=".stand_encode($test_code1, 0);
			$test_param1=~s/;/%3B/g;
			$agent1->addCookie($test_param1);
			$agent1->request('GET', $test_url, $referer);
			next if ($agent1->{ResponseCode}!=200);
			my $length1 = $agent1->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:
								$agent1->{CurlCode}==0 && $agent1->{ResponseBody}!~/HTTP\/\d\.\d 200 OK\r\n/?
									length($agent1->{ResponseBody}):next;
			$response_body1 = $agent1->{ResponseBody};

			$agent2 = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
			$test_param2 = "$name=".stand_encode($test_code2, 0);
			$test_param2=~s/;/%3B/g;
			$agent2->addCookie($test_param2);
			$agent2->request('GET', $test_url, $referer);
			next if ($agent2->{ResponseCode}!=200);
			my $length2 = $agent2->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:
								$agent2->{CurlCode}==0 && $agent2->{ResponseBody}!~/HTTP\/\d\.\d 200 OK\r\n/?
									length($agent2->{ResponseBody}):next;
			$response_body2 = $agent2->{ResponseBody};

			if (abs($length1 - $length2) - abs($length0 - $length1)>20)
			{
				next if (($base_url=~m/search.{0,8}$/i || $name=~m/search|^q$/i) && abs($length1 - $length2) - abs($length0 - $length1)<200);
				lock $this->{report}->{issue};
				$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
				my $request0 = $data->get_utf8_request();
				my $request1 = $agent1->get_utf8_request();
				my $request2 = $agent2->get_utf8_request();
				my $response_head0 = $data->get_utf8_response();
				my $response_head1 = $agent1->get_utf8_response();
				my $response_head2 = $agent2->get_utf8_response();
				$this->{report}->{issue}{$issue_id}{$target} = shared_clone({request => "原始请求:\n$request0\n注入真条件请求:\n$request1\n注入假条件请求:\n$request2"});
				$this->{report}->{issue}{$issue_id}{$target}{response} = "原始响应:\n$response_head0\n注入真条件响应:\n$response_head1\n注入假条件响应:\n$response_head2";
				$this->{report}->{issue}{$issue_id}{$target}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $name);
				print "<!-- $target found sql cookie injection(sigid=$sigid) --!>\n";

				$target=~s/\//\\/g;
				open HFILE, "> $target-sql-$length0-(0)" or die $!;
				print HFILE $data->{ResponseBody};
				close HFILE;
				open HFILE, "> $target-sql-$length1-(1)" or die $!;
				print HFILE $response_body1;
				close HFILE;
				open HFILE, "> $target-sql-$length2-(2)" or die $!;
				print HFILE $response_body2;
				close HFILE;

				last;
			}#if
		}#foreach
	}#foreach
}

sub check_xss_attack
{
	my $this = shift;
	my ($url, $referer, $data) = @_;
	return if ($data->{ResponseCode}!=200 || $data->{Static}==1 || index($url, '?')<0);

	my $issue_id = 30002;
	my ($base_url, $query_string) = $url=~m/^([^\?]+)\?(.+)/;
	my @params = split(/&/, $query_string);
PARAM:	foreach my $param (@params)
	{
		my ($name, $value) = $param=~m/(.+)=(.*)/;
		my $target = $base_url." => ".$name;
		{lock $this->{report}->{tested};
		next PARAM if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$target}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$target} = 1;
		}

		my $quote_param = quotemeta($param);
		foreach my $sigid (sort keys %xss_sig)
		{
			my $test_code = $xss_sig{$sigid};
			my $test_query = $query_string;
			my $test_param = "$name=".stand_encode($test_code, 0);
			$test_query=~s/$quote_param/$test_param/;
			my $test_url = "$base_url?$test_query";

			my $agent = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
			$agent->request('GET', $test_url, $referer);
			my $quote_code = quotemeta($test_code);
			$quote_code = $quote_code.'(?![^<>]{0,16}[\'"]\s*[,);+\]}|])' if $sigid==1;
			$quote_code = $quote_code.'[^<>]{0,16}[\'"]' if $sigid==2;
			if (${$agent->get_utf8_body()}=~m/^(.*$quote_code.*)$/mi)
			{lock $this->{report}->{issue};
				my $xss_line = "\n$1";
				$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
				$this->{report}->{issue}{$issue_id}{$target} = shared_clone({request => $agent->get_utf8_request()});
				$this->{report}->{issue}{$issue_id}{$target}{response} = $agent->get_utf8_response();
				$this->{report}->{issue}{$issue_id}{$target}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $name, $xss_line);
				print "<!-- $target found xss(sigid=$sigid) --!>\n";
				last;
			}

			$test_query = $query_string;
			$test_query=~s/$quote_param//;
			$test_url = "$base_url?$test_query";
			$test_param=~s/;/%3B/g;
			$agent->addCookie($test_param);
			$agent->request('GET', $test_url, $referer);
			if (${$agent->get_utf8_body()}=~m/^(.*$quote_code.*)$/mi)
			{lock $this->{report}->{issue};
				my $xss_line = "\n$1";
				$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
				$this->{report}->{issue}{$issue_id}{$target} = shared_clone({request => $agent->get_utf8_request()});
				$this->{report}->{issue}{$issue_id}{$target}{response} = $agent->get_utf8_response();
				$this->{report}->{issue}{$issue_id}{$target}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $name, $xss_line);
				print "<!-- $target found cookie xss(sigid=$sigid) --!>\n";
				last;
			}#if
		}#foreach
	}#foreach
}

sub check_code_leak
{
	my $this = shift;
	my ($url, $data) = @_;
	my $issue_id = 30003;
	if ($data->{ResponseCode}==200 && $data->{Static}==1 && $url!~m/(\.s?html?|\.stm|\.js|\/)$/)
	{
		my ($base_url) = $url=~m/^([^\?]+)/;
		{lock $this->{report}->{tested};
		return if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$base_url}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$base_url} = 1;
		}
		if (substr($data->{ResponseBody}, 0, 512)=~m/(<[\%\?]\s|<\?php\s|<jsp:|runat\s*=\s*[\'\"]?server[\'\"]?)/mio)
		{lock $this->{report}->{issue};
			$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
			$this->{report}->{issue}{$issue_id}{$base_url} = shared_clone({request => $data->get_utf8_request()});
			$this->{report}->{issue}{$issue_id}{$base_url}{response} = $data->get_utf8_response();
			$this->{report}->{issue}{$issue_id}{$base_url}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, "\n".${$data->get_utf8_body(512)});
			print "<!-- $base_url found code leak --!>\n";
		}
	}
}

sub check_directory_traversal
{
	my $this = shift;
	my ($url, $referer, $data) = @_;
	return if ($data->{ResponseCode}!=200 || $data->{Static}==1 || index($url, '?')<0);

	my $issue_id = 30004;
	my ($base_url, $query_string) = $url=~m/^([^\?]+)\?(.+)/;
	my @params = split(/&/, $query_string);
PARAM:	foreach my $param (@params)
	{
		my ($name, $value) = $param=~m/(.+)=(.*)/;
		my $target = $base_url." => ".$name;
		my ($org_value) = $value=~s/(%[a-f0-9]{2})/decode_pct($1)/ieg;
		next PARAM if (($name!~m/file/i || lc($name) ne 'q') && $org_value!~m/(^[a-zA-Z]:\/|^\.\.[\/\\]|^\.[\/\\]|^\/|\.\w{1,4}$)/o);
		{lock $this->{report}->{tested};
		next PARAM if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$target}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$target} = 1;
		}
		my $selfname = $base_url=~m/([^\/]+)$/;
		my %test_list = ("c:/windows/system32/drivers/etc/hosts" => "127\\.0\\.0\\.1\\s+localhost",
				"/../../../../../../../../../../../../windows/system32/drivers/etc/hosts" => "127\\.0\\.0\\.1\\s+localhost",
				"/etc/passwd" => "^root:x:0:0:root",
				"/../../../../../../../../../../../../etc/passwd" => "^root:x:0:0:root",
				"$selfname" => "^.{0, 512}(<[\\%\\?]\\s|<\\?php\\s|<jsp:|runat\\s*=\\s*['\"]?server['\"]?)");
		foreach my $test_code (keys(%test_list))
		{
			my $test_query = $query_string;
			my $quote_param = quotemeta($param);
			my $test_param = "$name=".stand_encode($test_code, 0);
			$test_query=~s/$quote_param/$test_param/;
			my $test_url = "$base_url?$test_query";
			my $agent = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
			$agent->request('GET', $test_url, $referer);
			if (${$agent->get_utf8_body()}=~m/$test_list{$test_code}/si)
			{lock $this->{report}->{issue};
				$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
				$this->{report}->{issue}{$issue_id}{$target} = shared_clone({request => $agent->get_utf8_request()});
				$this->{report}->{issue}{$issue_id}{$target}{response} = $agent->get_utf8_response();
				$this->{report}->{issue}{$issue_id}{$target}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $name, "\n".${$agent->get_utf8_body()});
				print "<!-- $target found directory traversal --!>\n";
				last;
			}
		}
	}
}

sub check_app_error
{
	my $this = shift;
	my ($url, $referer, $data) = @_;
	my $issue_id = 20001;

	return if ($data->{guess}<0);

	if ($data->{ResponseCode}==500)
	{lock $this->{report}->{issue};
		$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
		$this->{report}->{issue}{$issue_id}{$url} = shared_clone({request => $data->get_utf8_request()});
		$this->{report}->{issue}{$issue_id}{$url}{response} = $data->get_utf8_response();
		$this->{report}->{issue}{$issue_id}{$url}{analyze} = $this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze};

		print "<!-- $url found application error(original) --!>\n";
	}else
	{
		return if ($data->{ResponseCode}!=200 || $data->{Static}==1 || index($url, '?')<0);
		my ($base_url, $query_string) = $url=~m/^([^\?]+)\?(.+)/;
		my @params = split(/&/, $query_string);
PARAM:		foreach my $param (@params)
		{
			my ($name, $value) = $param=~m/(.+)=(.*)/;
			my $target = $base_url." => ".$name;
			{lock $this->{report}->{tested};
			next PARAM if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$target}));
			$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
			$this->{report}->{tested}{$issue_id}{$target} = 1;
			}

			my $quote_param = quotemeta($param);
			foreach my $sigid (sort keys %err_sig)
			{
				next if ($value!~/^\d+$/ && $sigid==3);
				my $test_code = $value.$err_sig{$sigid};
				my $test_query = $query_string;
				my $test_param = "$name=".stand_encode($test_code, 0);
				$test_query=~s/$quote_param/$test_param/;
				my $test_url = "$base_url?$test_query";
				my $agent = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
				$agent->request('GET', $test_url, $referer);
				if ($agent->{ResponseCode}==500 || ($agent->{ResponseCode}==200 && ${$agent->get_utf8_body(-1024)}=~/$regex_err/))
				{lock $this->{report}->{issue};
					$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
					$this->{report}->{issue}{$issue_id}{$target} = shared_clone({request => $agent->get_utf8_request()});
					$this->{report}->{issue}{$issue_id}{$target}{response} = $agent->get_utf8_response();
					$this->{report}->{issue}{$issue_id}{$target}{analyze} = $this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze};
					print "<!-- $target found application error(sigid=$sigid) --!>\n";
					last;
				}

				$test_query=~s/$quote_param//;
				$test_url = "$base_url?$test_query";
				$test_param=~s/;/%3B/g;
				$agent->addCookie($test_param);
				$agent->request('GET', $test_url, $referer);
				if ($agent->{ResponseCode}==500 || ($agent->{ResponseCode}==200 && ${$agent->get_utf8_body(-1024)}=~/$regex_err/))
				{lock $this->{report}->{issue};
					$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
					$this->{report}->{issue}{$issue_id}{$target} = shared_clone({request => $agent->get_utf8_request()});
					$this->{report}->{issue}{$issue_id}{$target}{response} = $agent->get_utf8_response();
					$this->{report}->{issue}{$issue_id}{$target}{analyze} = $this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze};
					print "<!-- $target found application error by cookie(sigid=$sigid) --!>\n";
					last;
				}
			}#foreach
		}#foreach
	}#if else
}

sub check_directory_explore
{
	my $this = shift;
	my ($url, $data) = @_;
	my $issue_id = 20002;
	if (substr($url, -1) eq '/' && $data->{ResponseCode}==200 && $data->{Static}==0)
	{
		{lock $this->{report}->{tested};
		return if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$url}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$url} = 1;
		}
		my $host = get_hostname($url);
		my $quote_host = quotemeta($host);
		if ($data->{ResponseBody}=~m/<title>(Index of|$quote_host -) \/[^<]*<\/title>/i)
		{lock $this->{report}->{issue};
			$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
			$this->{report}->{issue}{$issue_id}{$url} = shared_clone({request => $data->get_utf8_request()});
			$this->{report}->{issue}{$issue_id}{$url}{response} = $data->get_utf8_response();
			$this->{report}->{issue}{$issue_id}{$url}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, "\n".${$data->get_utf8_body(1024)});
			print "<!-- $url found directory explore --!>\n";
		}
	}
}

sub check_internal_file
{
	my $this = shift;
	my ($url, $referer, $data) = @_;
	my $issue_id = 20003;

	if ($data->{guess}==-1 && substr($url, -1) ne '/')
	{
		return if ($this->is_page404($url, $data)!=0);

		#recheck again!!
		my $url_fp = $url;
		$url_fp=~s/^(.+\/)(.*?)(\.[^\.]*)?$/$1$2Za59caqkd6r$3/s;
		$url_fp=~s/\.$//;
		my $agent = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
		$agent->request('GET', $url_fp, $url); 
		return if ($this->is_page404($url_fp, $agent)!=1);

		{lock $this->{report}->{tested}; lock $this->{report}->{issue};
			unless (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$url}))
			{
				$this->{report}->{tested}{$issue_id}{lc($url)} = 1;
				$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
				$this->{report}->{issue}{$issue_id}{$url} = shared_clone({request => $data->get_utf8_request()});
				$this->{report}->{issue}{$issue_id}{$url}{response} = $data->get_utf8_response();
				$this->{report}->{issue}{$issue_id}{$url}{analyze} = $this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze};
				print "<!-- $url found internal file leak --!>\n";
			}
		}#lock end
	}

	return if ($data->{ResponseCode}!=200 && $data->{ResponseCode}!=403 && $data->{ResponseCode}!=500);
	return if ($url=~m/^https?:\/\/([^\/]+\/){3}/);

	my @list_file = ();
	my ($dir, $file, $script, $path) = ('', '', '', '');
	my ($host) = $url=~m/^(https?:\/\/[^\/]+)/;
	{lock $this->{script};
		if (!defined($this->{script}{$host}))
		{
			$this->{script}{$host} = shared_clone({});
			$this->{script}{$host}{type} = '';
			$this->{script}{$host}{buffered} = shared_clone({});
		}
		if ($data->{Static}==0 && $url=~m/^(https?:\/\/.+\/)([^\?]+)\.(aspx?|php|jsp)(\?|$)/io)
		{
			($dir, $file, $script) = ($1, $2, $3);
			$path = "$dir$file.$script";
			$this->{script}{$host}{type} = lc($script) if ($this->{script}{$host}{type} eq '');
		}elsif ($url=~m/^(https?:\/\/.+\/)/)
		{
			$dir = $1;
		}else
		{
			return;
		}
	}#lock end

	{lock $this->{report}->{tested}; lock $this->{report}->{issue}; lock $this->{script};
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		if ($path ne '')
		{
			if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{lc($path)}))
			{
				if ($this->{report}->{tested}{$issue_id}{lc($path)}==1 && $data->{guess}==0)
				{
					delete($this->{report}->{issue}{$issue_id}{$path});
					delete($this->{report}->{issue}{$issue_id}) if (keys(%{$this->{report}->{issue}{$issue_id}})==0);
					print "<!!-- $path remove from internal file leak list --!!>\n";
				}
			}else
			{
				$this->{report}->{tested}{$issue_id}{lc($path)} = 0;
				push @list_file, "$path.bak";
				push @list_file, "$path~";
				push @list_file, $dir."复件 $file.".$script;
				push @list_file, $dir."Copy of $file.".$script;
				push @list_file, "$dir$file".'1.'.$script if ($file!~/\d$/);
			}
		}
		if (!defined($this->{report}->{tested}{$issue_id}) || !defined($this->{report}->{tested}{$issue_id}{lc($dir)}))
		{
			if ($this->{script}{$host}{type} eq '')
			{
				if (!defined($this->{script}{$host}{buffered}{$dir}))
				{
					$this->{script}{$host}{buffered}{lc($dir)} = 1;
					push @list_file, $dir.$_ for @list_txt;
				}
			}else
			{
				if (!defined($this->{script}{$host}{buffered}{$dir}))
				{
					$this->{report}->{tested}{$issue_id}{lc($dir)} = 0;
					push @list_file, $dir.$_ for @list_txt;
					push @list_file, "$dir$_.".$this->{script}{$host}{type} for @list_cgi;
				}
				foreach my $dir (keys(%{$this->{script}{$host}{buffered}}))
				{
					$this->{report}->{tested}{$issue_id}{lc($dir)} = 0;
					push @list_file, "$dir$_.".$this->{script}{$host}{type} for @list_cgi;
				}
				%{$this->{script}{$host}{buffered}} = ();
			}
		}
	}#lock end

	push @{$data->{urls}}, [$_, -1] for (@list_file);
}

sub check_webdav
{
	my $this = shift;
	my ($url, $data) = @_;
	my $issue_id =20004;
	if ($url=~m/^https?:\/\/[^\/]+\/$/ && $data->{RequestHead}=~m/^DAV:/mi)
	{lock $this->{report}->{issue};
		$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
		$this->{report}->{issue}{$issue_id}{$url} = shared_clone({request => $data->get_utf8_request()});
		$this->{report}->{issue}{$issue_id}{$url}{response} = $data->get_utf8_response();
		$this->{report}->{issue}{$issue_id}{$url}{analyze} = $this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze};
		print "<!-- $url found webdav --!>\n";
	}
}

sub check_invalid_link
{
	my $this = shift;
	my ($url, $referer, $data) = @_;
	my $issue_id = 10001;

	return if ($data->{guess}<0);

	if ($data->{ResponseCode}==404)
	{
		my ($base_url) = $url=~m/^([^\?]+)/;
		#return if ($base_url=~m/%2b|\+/i);
		lock $this->{report}->{tested};
		return if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$base_url}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$base_url} = 1;

		lock $this->{report}->{issue};
		$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
		$this->{report}->{issue}{$issue_id}{$base_url} = shared_clone({request => $data->get_utf8_request()});
		$this->{report}->{issue}{$issue_id}{$base_url}{response} = $data->get_utf8_response();
		$this->{report}->{issue}{$issue_id}{$base_url}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $referer);
		print "<!-- $base_url found invalid link --!>\n";
	}
}

sub check_login_page
{
	my $this = shift;
	my ($url, $data) = @_;
	my $issue_id = 10002;
	if ($data->{ResponseCode}==200 && $data->{Text}==1 && $url!~m/\.(css|js)$/i && $data->{ResponseBody} ne '')
	{
		{lock $this->{report}->{tested};
		return if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$url}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$url} = 1;
		}
		if ($url=~m/login/i || ${$data->get_utf8_body(512)}=~m/<title>(login|登录)<\/title>/i)
		{
			return if ($data->{guess}<0 && is_page404($url, $data)!=0);

			lock $this->{report}->{issue};
			$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
			$this->{report}->{issue}{$issue_id}{$url} = shared_clone({request => $data->get_utf8_request()});
			$this->{report}->{issue}{$issue_id}{$url}{response} = $data->get_utf8_response();
			$this->{report}->{issue}{$issue_id}{$url}{analyze} = $this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze};
			print "<!-- $url found login page--!>\n";
		}
	}
}

sub check_internal_ip
{
	my $this = shift;
	my ($url, $data) = @_;
	my $issue_id = 10003;
	if ($data->{ResponseCode}==200 && $data->{Text}==1 && $data->{ResponseBody} ne '')
	{
		{lock $this->{report}->{tested};
		return if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$url}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$url} = 1;
		}
		my @ip_list = $data->{ResponseBody}=~m/(?:src|href)\s*=\s*['"](https?:\/\/(?:10\.\d{1,3}\.\d{1,3}\.\d{1,3}|172\.(?:1[6-9]|2\d|3[01])\.\d{1,3}\.\d{1.3}|192\.168\.\d{1,3}\.\d{1,3})(?:\:\d+)?\/[^\s'">]+)/gio;
		if (@ip_list>0)
		{
			my $ips = "\n";
			foreach my $ip (@ip_list)
			{
				$ips .= "$ip, " if (index($ips, $ip)<0);
			}
			substr($ips, -2) = '';
			lock $this->{report}->{issue};
			$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
			$this->{report}->{issue}{$issue_id}{$url} = shared_clone({request => $data->get_utf8_request()});
			$this->{report}->{issue}{$issue_id}{$url}{response} = $data->get_utf8_response();
			$this->{report}->{issue}{$issue_id}{$url}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $ips);
			print "<!-- $url found internal ip --!>\n";
		}
	}
}

sub check_email_leak
{
	my $this = shift;
	my ($url, $data) = @_;
	my $issue_id = 10004;
	if ($data->{ResponseCode}==200 && $data->{Text}==1 && $data->{ResponseBody} ne '')
	{
		{lock $this->{report}->{tested};
		return if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$url}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$url} = 1;
		}
		my @email_list = $data->{ResponseBody}=~m/([\w\-\.]+@(?:[\w\-]+\.)+[a-z]{2,4})[^\w\.]/gio;
		if (@email_list>0)
		{
			my $emails = "\n";
			foreach my $email (@email_list)
			{
				$emails .= lc("$email, ") if (index($emails, lc($email))<0);
			}
			substr($emails, -2) = '';
			lock $this->{report}->{issue};
			$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
			$this->{report}->{issue}{$issue_id}{$url} = shared_clone({request => $data->get_utf8_request()});
			$this->{report}->{issue}{$issue_id}{$url}{response} = $data->get_utf8_response();
			$this->{report}->{issue}{$issue_id}{$url}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $emails);
			print "<!-- $url found email leak --!>\n";
		}
	}
}

sub check_keyword
{
	my $this = shift;
	my ($url, $data) = @_;
	my $issue_id = 10007;

	if ($data->{ResponseCode}==200 && $data->{Text}==1 && $data->{ResponseBody} ne '' && $this->{keyword} ne '')
	{
		{lock $this->{report}->{tested};
		return if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$url}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$url} = 1;
		}
		my %keyword_list;
		$keyword_list{"$_,"} = 1 for (${$data->get_utf8_body()}=~m/($this->{keyword})/ig);
		my @keyword_list = keys(%keyword_list);
		my $keywords = "@keyword_list";
		if ($keywords ne '')
		{
			lock $this->{report}->{issue};
			$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
			$this->{report}->{issue}{$issue_id}{$url} = shared_clone({request => $data->get_utf8_request()});
			$this->{report}->{issue}{$issue_id}{$url}{response} = $data->get_utf8_response();
			$this->{report}->{issue}{$issue_id}{$url}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $keywords);
			print "<!-- $url found keyword '$keywords' --!>\n";
		}
	}
}

sub check_url_filter
{
	my $this = shift;
	my ($url, $data) = @_;
	my $issue_id = 10008;

	$this->{url_filter} = '.*' if ($this->{CrawlBit} eq 'hik');
	return if ($this->{url_filter} eq '');

	{lock $this->{report}->{tested};
	return if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$url}));
	$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
	$this->{report}->{tested}{$issue_id}{$url} = 1;
	}
	if ($url=~m/($this->{url_filter})/i && ($this->{CrawlBit} ne 'hik' || $data->{guess}>=0))
	{lock $this->{report}->{issue};
		my ($filter, $status) = ($1, $data->{ResponseCode});
		$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
		$this->{report}->{issue}{$issue_id}{$url} = shared_clone({request => $data->get_utf8_request()});
		$this->{report}->{issue}{$issue_id}{$url}{response} = $data->get_utf8_response();
		$this->{report}->{issue}{$issue_id}{$url}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $filter, $status);
		print "<!-- $url found url filter by '$filter', http status $status --!>\n" if ($this->{CrawlBit} ne 'hik');
		#print "<!-- $url found url filter by '$filter', http status $status --!>\n";
	}
}

sub check_internal_directory
{
	my $this = shift;
	my ($url, $referer, $data) = @_;
	my $issue_id = 10005;

	if ($data->{guess}==-2 && substr($url, -1) ne '/' && ($data->{ResponseCode}==301 || $data->{ResponseCode}==302) && $data->{ResponseHead}=~m/^Location:\s*(.+\/)\r$/mi)
	{lock $this->{report}->{tested}; lock $this->{report}->{issue};
		my ($test_url, $location) = ("$url/", $1);
		$test_url=~s/[ +]|%20|%2B//ig;
		$location=~s/[ +]|%20|%2B//ig;
		return if ($test_url ne $location);
		return if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$test_url}));
		$this->{report}->{tested}{$issue_id}{lc($test_url)} = 1;
		$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
		$this->{report}->{issue}{$issue_id}{$test_url} = shared_clone({request => $data->get_utf8_request()});
		$this->{report}->{issue}{$issue_id}{$test_url}{response} = $data->get_utf8_response();
		$this->{report}->{issue}{$issue_id}{$test_url}{analyze} = $this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze};
		print "<!-- $test_url found internal directory leak --!>\n";

		push @{$data->{urls}}, [$test_url, -2];
		return;
	}

	return if (substr($url, -1) ne '/' || ($data->{ResponseCode}!=200 && $data->{ResponseCode}!=403 && $data->{ResponseCode}!=500));
	return if ($url=~m/^https?:\/\/([^\/]+\/){3}/);

	{lock $this->{report}->{tested}; lock $this->{report}->{issue};
		if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{lc($url)}))
		{
			if ($this->{report}->{tested}{$issue_id}{lc($url)}==1 && $data->{guess}==0)
			{
				delete($this->{report}->{issue}{$issue_id}{$url});
				delete($this->{report}->{issue}{$issue_id}) if (keys(%{$this->{report}->{issue}{$issue_id}})==0);
				print "<!!-- $url remove from internal directory leak list --!!>\n";
			}
			return;
		}else
		{
			$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
			$this->{report}->{tested}{$issue_id}{lc($url)} = 0;
		}
	}

	push @{$data->{urls}}, [$url.$_, -2] for (@list_dir);
}

sub check_cms
{
	my $this = shift;
	my ($url, $referer, $data) = @_;
	my $issue_id = 10006;
	if ($url=~/^https?:\/\/([^\/]+\/){1,2}$/ && ($data->{ResponseCode}==200 || $data->{ResponseCode}==401))
	{
		{lock $this->{report}->{tested};
		return if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$url}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$url} = 1;
		}

		my $agent = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
		$agent->request('GET', $url.'robots.txt', $referer);
		my @robots_url = $agent->{ResponseBody}=~/allow:\s*\/+([^\*\#\s]+)/ig;
		push @{$data->{urls}}, [combine_url($url, $_), 0] for (@robots_url);

		my $cms = cms::check_cms($url, $referer, $this->{Proxy}, $data->get_utf8_body(-4096), $agent->get_utf8_body());
		if ($cms ne '')
		{lock $this->{report}->{issue};
			$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
			$this->{report}->{issue}{$issue_id}{$url} = shared_clone({request => $data->get_utf8_request()});
			$this->{report}->{issue}{$issue_id}{$url}{response} = $data->get_utf8_response();
			$this->{report}->{issue}{$issue_id}{$url}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $cms);
			$data->{cms} = $cms;
			print "<!-- $url found $cms CMS --!>\n";
		}
	}
}

sub check_form
{
	my $this = shift;
	my ($url, $data) = @_;
	my @forms = $data->{ResponseBody}=~m/(<form[^>]*>.+?<\/form>)/sig;
	foreach my $form (@forms)
	{
		my $action_url;
		if ($form=~m/<form(?=\s)[^>]*\saction\s*=\s*['"]?([^\s'">]*)[^>]*>/i)
		{
			$action_url = $1;
			$action_url=~s/\\\//\//g;
			$action_url=~s/&nbsp;/ /g;
			$action_url=~s/&lt;/</g;
			$action_url=~s/&gt;/>/g;
			$action_url=~s/&apos;/'/g;
			$action_url=~s/&quot;/"/g;
			$action_url=~s/&amp;/&/g;
			next if ($action_url=~m/^mailto:|^javascript:/i);
			$action_url = combine_url($url, $action_url);
			next if (!is_same_domain($url, $action_url));
		}else
		{
			$action_url = $url;
		}
		my $method = $form=~m/<form(?=\s)[^>]*\smethod\s*=\s*['"]?\s*(get|post)['"\s][^>]*>/i?uc($1):'GET';
		my @input_lab = $form=~m/<input(?=\s)[^>]*\stype\s*=\s*['"]?\s*(?:checkbox|hidden|password|radio|text)['"\s][^>]*>/ig;
		push @input_lab, $form=~m/<textarea\s[^>]+>/ig;
		push @input_lab, $form=~m/<select\s[^>]+>.*?<\/select>/sig;
		my $form_string = '';
		foreach my $input (@input_lab)
		{
			if ($input=~m/\sname\s*=\s*['"]?([^\s'">]+)/i && substr($1, 0, 2) ne '__')
			{
				my $name = stand_encode($1, 0);
				my $value = $input=~m/\svalue\s*=\s*['"]?([^\s'">]+)/i?stand_encode($1, 0):'';
				$form_string .= "$name=$value&";
			}
		}
		substr($form_string, -1) = '' if ($form_string ne '');
		#print "!!$form_string!!\n";
		my $scan_bit = $this->{CrawlBit};
		$this->check_form_sql_injection($action_url, $method, $url, $form_string) if ($scan_bit=~/^(hik|min|comm|all)$/i || $scan_bit=~/^[01]+$/ && substr($scan_bit, 4, 1));
		$this->check_form_xss_attack($action_url, $method, $url, $form_string) if ($scan_bit=~/^(hik|min|comm|all)$/i || $scan_bit=~/^[01]+$/ && substr($scan_bit, 5, 1));
		$this->check_form_app_error($action_url, $method, $url, $form_string) if ($scan_bit=~/^(hik|min|comm|all)$/i || $scan_bit=~/^[01]+$/ &&  substr($scan_bit, 6, 1));
	}
}

sub check_form_app_error
{
	my $this = shift;
	my ($url, $method, $referer, $form) = @_;

	my $issue_id = 20001;
	my @params = split(/&/, $form);
PARAM:  foreach my $param (@params)
	{
		my ($name, $value) = $param=~m/(.+)=(.*)/;
		my ($base_url) = $url=~m/^([^\?]+)/;
		my $target = $base_url." => ".$name;
		{lock $this->{report}->{tested};
		next PARAM if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$target}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$target} = 1;
		}

		my $quote_param = quotemeta($param);
		foreach my $sigid (sort keys %err_sig)
		{
			next if ($value!~/^\d+$/ && $sigid==3);
			my $test_code = $value.$err_sig{$sigid};
			my $test_param = "$name=".stand_encode($test_code, 0);
			my $test_form = $form;
			$test_form=~s/$quote_param/$test_param/;
			my $agent = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
			$method ne 'POST' ? $agent->request('GET', index($url, '?')<0?"$url?$test_form":"$url&$test_form", $referer) : 
					    $agent->request('POST', $url, $referer, $test_form);
			if ($agent->{ResponseCode}==500 || ($agent->{ResponseCode}==200 && ${$agent->get_utf8_body(-1024)}=~/$regex_err/))
			{lock $this->{report}->{issue};
				$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
				$this->{report}->{issue}{$issue_id}{$target} = shared_clone({request => $agent->get_utf8_request()});
				$this->{report}->{issue}{$issue_id}{$target}{response} = $agent->get_utf8_response();
				$this->{report}->{issue}{$issue_id}{$target}{analyze} = $this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze};
				print "<!-- $target found application error by form(sigid=$sigid) --!>\n";
				last;
			}

			$test_form=~s/$quote_param//;
			$test_param=~s/;/%3B/g;
			$agent->addCookie($test_param);
			$method ne 'POST' ? $agent->request('GET', index($url, '?')<0?"$url?$test_form":"$url&$test_form", $referer) : 
					    $agent->request('POST', $url, $referer, $test_form);
			if ($agent->{ResponseCode}==500 || ($agent->{ResponseCode}==200 && ${$agent->get_utf8_body(-1024)}=~/$regex_err/))
			{lock $this->{report}->{issue};
				$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
				$this->{report}->{issue}{$issue_id}{$target} = shared_clone({request => $agent->get_utf8_request()});
				$this->{report}->{issue}{$issue_id}{$target}{response} = $agent->get_utf8_response();
				$this->{report}->{issue}{$issue_id}{$target}{analyze} = $this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze};
				print "<!-- $target found application error from form cookie(sigid=$sigid) --!>\n";
				last;
			}
		}#foreach
	}#foreach
}

sub check_form_sql_injection
{
	my $this = shift;
	my ($url, $method, $referer, $form) = @_;

	my $issue_id = 30001;
	my @params = split(/&/, $form);
PARAM:	foreach my $param (@params)
	{
		my ($name, $value) = $param=~m/(.+)=(.*)/;
		my ($base_url) = $url=~m/^([^\?]+)/;
		my $target = $base_url." => ".$name;
		{lock $this->{report}->{tested};
		next PARAM if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$target}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$target} = 1;
		}

		my $quote_param = quotemeta($param);
		foreach my $sigid (sort keys %inject_sig)
		{
			next if (($value!~/^\d+$/ || $value eq '') && ($sigid==1 || $sigid==3));
			my ($test_code1, $test_code2) = map $value.$_, @{$inject_sig{$sigid}};

			my $agent1 = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
			my $test_form1 = $form;
			my $test_param1 = "$name=".stand_encode($test_code1, 0);
			$test_form1=~s/$quote_param/$test_param1/;
			$method ne 'POST' ? $agent1->request('GET', index($url, '?')<0?"$url?$test_form1":"$url&$test_form1", $referer) : 
					    $agent1->request('POST', $url, $referer, $test_form1);
			goto COOKIE if ($agent1->{ResponseCode}!=200);
			my $length1 = $agent1->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:
								$agent1->{CurlCode}==0 && $agent1->{ResponseBody}!~/HTTP\/\d\.\d 200 OK\r\n/?
									length($agent1->{ResponseBody}):goto COOKIE;
			my $response_body1 = $agent1->{ResponseBody};

			my $agent2 = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
			my $test_form2 = $form;
			my $test_param2 = "$name=".stand_encode($test_code2, 0);
			$test_form2=~s/$quote_param/$test_param2/;
			$method ne 'POST' ? $agent2->request('GET', index($url, '?')<0?"$url?$test_form2":"$url&$test_form2", $referer) : 
					    $agent2->request('POST', $url, $referer, $test_form2);
			goto COOKIE if ($agent2->{ResponseCode}!=200);
			my $length2 = $agent2->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:
								$agent2->{CurlCode}==0 && $agent2->{ResponseBody}!~/HTTP\/\d\.\d 200 OK\r\n/?
									length($agent2->{ResponseBody}):goto COOKIE;
			my $response_body2 = $agent2->{ResponseBody};

			if (abs($length1 - $length2)>20)
			{
				goto COOKIE if (($base_url=~m/search.{0,8}$/i || $name=~m/search|^q$/i) && abs($length1 - $length2)<200);
				my $agent3 = new HTTPAgent();
				$method ne 'POST' ? $agent3->request('GET', index($url, '?')<0?"$url?$test_form1":"$url&$test_form1", $referer) :
						    $agent3->request('POST', $url, $referer, $test_form1);
				goto COOKIE if ($agent3->{ResponseCode}!=200);
				my $length3 = $agent3->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:
									$agent3->{CurlCode}==0 && $agent3->{ResponseBody}!~/HTTP\/\d\.\d 200 OK\r\n/?
										length($agent3->{ResponseBody}):goto COOKIE;
				goto COOKIE if (abs($length1 - $length2) - abs($length1 - $length3)<20);
				goto COOKIE if (($base_url=~m/search.{0,8}$/i || $name=~m/search|^q$/i) && abs($length1 - $length2) - abs($length1 - $length3)<200);

				lock $this->{report}->{issue};

				$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
				my $request1 = $agent1->get_utf8_request();
				my $request2 = $agent2->get_utf8_request();
				my $response_head1 = $agent1->get_utf8_response();
				my $response_head2 = $agent2->get_utf8_response();
				$this->{report}->{issue}{$issue_id}{$target} = shared_clone({request => "注入真条件请求:\n$request1\n注入假条件请求:\n$request2"});
				$this->{report}->{issue}{$issue_id}{$target}{response} = "注入真条件响应:\n$response_head1\n注入假条件响应:\n$response_head2";
				$this->{report}->{issue}{$issue_id}{$target}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $name);
				print "<!-- $target found post sql injection(sigid=$sigid) --!>\n";

				$target=~s/\//\\/g;
				open HFILE, "> $target-postsql-$length1-(1)" or die $!;
				print HFILE $response_body1;
				close HFILE;
				open HFILE, "> $target-postsql-$length2-(2)" or die $!;
				print HFILE $response_body2;
				close HFILE;

				last;
			}#if

COOKIE:			my $test_form = $form;
			$test_form=~s/$quote_param//;

			$agent1 = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
			$test_param1 = "$name=".stand_encode($test_code1, 0);
			$test_param1=~s/;/%3B/g;
			$agent1->addCookie($test_param1);
			$method ne 'POST' ? $agent1->request('GET', index($url, '?')<0?"$url?$test_form":"$url&$test_form", $referer) : 
					    $agent1->request('POST', $url, $referer, $test_form);
			next if ($agent1->{ResponseCode}!=200);
			my $length1 = $agent1->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:
								$agent1->{CurlCode}==0 && $agent1->{ResponseBody}!~/HTTP\/\d\.\d 200 OK\r\n/?
									length($agent1->{ResponseBody}):next;
			$response_body1 = $agent1->{ResponseBody};

			$agent2 = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
			$test_param2 = "$name=".stand_encode($test_code2, 0);
			$test_param2=~s/;/%3B/g;
			$agent2->addCookie($test_param2);
			$method ne 'POST' ? $agent2->request('GET', index($url, '?')<0?"$url?$test_form":"$url&$test_form", $referer) : 
					    $agent2->request('POST', $url, $referer, $test_form);
			next if ($agent2->{ResponseCode}!=200);
			my $length2 = $agent2->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:
								$agent2->{CurlCode}==0 && $agent2->{ResponseBody}!~/HTTP\/\d\.\d 200 OK\r\n/?
									length($agent2->{ResponseBody}):next;
			$response_body2 = $agent2->{ResponseBody};

			if (abs($length1 - $length2)>20)
			{
				next if (($base_url=~m/search.{0,8}$/i || $name=~m/search|^q$/i) && abs($length1 - $length2)<200);
				my $agent3 = new HTTPAgent();
				$test_param1=~s/;/%3B/g;
				$agent3->addCookie($test_param1);
				$method ne 'POST' ? $agent3->request('GET', index($url, '?')<0?"$url?$test_form":"$url&$test_form", $referer) :
						    $agent3->request('POST', $url, $referer, $test_form);
				next if ($agent3->{ResponseCode}!=200);
				my $length3 = $agent3->{ResponseHead}=~/^Content-Length:\s*(\d+)/mi?$1:
									$agent3->{CurlCode}==0 && $agent3->{ResponseBody}!~/HTTP\/\d\.\d 200 OK\r\n/?
										length($agent3->{ResponseBody}):next;
				next if (abs($length1 - $length2) - abs($length1 - $length3)<20);
				next if (($base_url=~m/search.{0,8}$/i || $name=~m/search|^q$/i) && abs($length1 - $length2) - abs($length1 - $length3)<200);

				lock $this->{report}->{issue};

				$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
				my $request1 = $agent1->get_utf8_request();
				my $request2 = $agent2->get_utf8_request();
				my $response_head1 = $agent1->get_utf8_response();
				my $response_head2 = $agent2->get_utf8_response();
				$this->{report}->{issue}{$issue_id}{$target} = shared_clone({request => "注入真条件请求:\n$request1\n注入假条件请求:\n$request2"});
				$this->{report}->{issue}{$issue_id}{$target}{response} = "注入真条件响应:\n$response_head1\n注入假条件响应:\n$response_head2";
				$this->{report}->{issue}{$issue_id}{$target}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $name);
				print "<!-- $target found post cookie sql injection(sigid=$sigid) --!>\n";

				$target=~s/\//\\/g;
				open HFILE, "> $target-postcookiesql-$length1-(1)" or die $!;
				print HFILE $response_body1;
				close HFILE;
				open HFILE, "> $target-postcookiesql-$length2-(2)" or die $!;
				print HFILE $response_body2;
				close HFILE;

				last;
			}#if
		}#foreach
	}#foreach
}

sub check_form_xss_attack
{
	my $this = shift;
	my ($url, $method, $referer, $form) = @_;

	my $issue_id = 30002;
	my @params = split(/&/, $form);
PARAM:	foreach my $param (@params)
	{
		my ($name, $value) = $param=~m/(.+)=(.*)/;
		my ($base_url) = $url=~m/^([^\?]+)/;
		my $target = $base_url." => ".$name;
		{lock $this->{report}->{tested};
		next PARAM if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$target}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$target} = 1;
		}

		my $quote_param = quotemeta($param);
		foreach my $sigid (sort keys %xss_sig)
		{
			my $test_code = $xss_sig{$sigid};
			my $test_param = "$name=".stand_encode($test_code, 0);
			my $test_form = $form;
			$test_form=~s/$quote_param/$test_param/;

			my $agent = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
			$method ne 'POST' ? $agent->request('GET', index($url, '?')<0?"$url?$test_form":"$url&$test_form", $referer) : 
					    $agent->request('POST', $url, $referer, $test_form);
			my $quote_code = quotemeta($test_code);
			$quote_code = $quote_code.'(?![^<>]{0,16}[\'"]\s*[,);+\]}|])' if $sigid==1;
			$quote_code = $quote_code.'[^<>]{0,16}[\'"]' if $sigid==2;
			if (${$agent->get_utf8_body()}=~m/^(.*$quote_code.*)$/mi)
			{lock $this->{report}->{issue};
				my $xss_line = "\n$1";
				$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
				$this->{report}->{issue}{$issue_id}{$target} = shared_clone({request => $agent->get_utf8_request()});
				$this->{report}->{issue}{$issue_id}{$target}{response} = $agent->get_utf8_response();
				$this->{report}->{issue}{$issue_id}{$target}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $name, $xss_line);
				print "<!-- $target found post xss(sigid=$sigid) --!>\n";
				last;
			}

			$test_form = $form;
			$test_form=~s/$quote_param//;
			$test_param=~s/;/%3B/g;
			$agent->addCookie($test_param);
			$method ne 'POST' ? $agent->request('GET', index($url, '?')<0?"$url?$test_form":"$url&$test_form", $referer) : 
					    $agent->request('POST', $url, $referer, $test_form);
			if (${$agent->get_utf8_body()}=~m/^(.*$quote_code.*)$/mi)
			{lock $this->{report}->{issue};
				my $xss_line = "\n$1";
				$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
				$this->{report}->{issue}{$issue_id}{$target} = shared_clone({request => $agent->get_utf8_request()});
				$this->{report}->{issue}{$issue_id}{$target}{response} = $agent->get_utf8_response();
				$this->{report}->{issue}{$issue_id}{$target}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $name, $xss_line);
				print "<!-- $target found post cookie xss(sigid=$sigid) --!>\n";
				last;
			}#if
		}#foreach
	}#foreach
}

sub check_xss_bom_attack
{
	my $this = shift;
	my ($url, $referer, $data) = @_;
	return if ($data->{ResponseCode}!=200 || $data->{Static}==1 || index($url, '?')<0);

	my $issue_id = 30007;
	my ($base_url, $query_string) = $url =~m/^([^\?]+)\?(.+)/;
	my @params = split(/&/, $query_string);

PARAM:	foreach my $param (@params)
	{
		my ($name, $value) = $param =~m/(.+)=(.*)/;
		my $target = $base_url." => ".$name;
		{lock $this->{report}->{tested};
		next PARAM if ( defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$target}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$target} = 1;
		}
# FIXME use real payload?
		my $test_code = "+/v8 ";
		my $test_query = $query_string;
		my $quote_param = quotemeta( $param );
		my $test_param = "$name=".stand_encode($test_code, 0);
		$test_query=~s/$quote_param/$test_param/;

		my $test_url = "$base_url?$test_query";
		my $agent = new HTTPAgent(Proxy => $this->{Proxy}, Cookie => $this->{Cookie});
		$agent->request('GET', $test_url, $referer);
		my $quote_code = quotemeta($test_code);
		if (${$agent->get_utf8_body()}=~m/^($quote_code.*)$/mi)
		{lock $this->{report}->{issue};
			my $xss_line = "\n$1";
			$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
			$this->{report}->{issue}{$issue_id}{$target} = shared_clone({request => $agent->get_utf8_request()});
			$this->{report}->{issue}{$issue_id}{$target}{response} = $agent->get_utf8_response();
			$this->{report}->{issue}{$issue_id}{$target}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $name, $xss_line);
			print "<!-- $target found xss bom --!>\n";
		}
	}
}

sub check_cms_vul
{
	my $this = shift;
	my ($url, $referer, $data) = @_;
	my $issue_id = 30009;

	#print "wocorpse: $url\n";
	if ($url=~/^https?:\/\/([^\/]+\/){1,2}$/ && ($data->{ResponseCode}==200 || $data->{ResponseCode}==401))
	{
		{lock $this->{report}->{tested};
		return if (defined($this->{report}->{tested}{$issue_id}) && defined($this->{report}->{tested}{$issue_id}{$url}));
		$this->{report}->{tested}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{tested}{$issue_id}));
		$this->{report}->{tested}{$issue_id}{$url} = 1;
		}

                my @result = ();
		# If you want to know the following functions' detail, refer to cms_vul.pm
                my $retval = phpcms_v9_poster_click_referer($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_v9_fileext_filename($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_v9_post_key($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_2008_show_referer($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_2008_ads_referer($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_2008_job_genre_list($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_2008_job_genre_applylist($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_2008_web_menu($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_2008_product_pagesize($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_v9_comment_list_commentid($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_v9_ajax_getlist_callback($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_v9_ajax_getpath_callback($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_v9_ajax_gettopparent_callback($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_v9_post_callback($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_v9_add_favorite_callback($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_v9_public_get_suggest_keyword_q($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpcms_2008_product_prowhere($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = dedecms_v56_search_typeArr($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = dedecms_v57sp1_alipay_code($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = dedecms_v57sp1_yeepay_code($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = dedecms_v56_group_id($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = dedecms_v57_group_keyword($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = dedecms_v5x_common_request($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = dedecms_v57_config_admindirhand($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = dedecms_v57sp1_shopcar_membershops($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = dedecms_v57_bshare_uuid($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = dedecms_v5x_ajax_membergroup_membergroup($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = dedecms_v5x_guestbook_msg($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = dedecms_v57_wap_pageBody($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = dedecms_vall_login_gotopage($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = anwsion_index_user_name($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = php168_v6_vote_cid($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = dedecms_vall_index_inLockfile($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = dedecms_v57_dedesql_arrs($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = php168_v6_kindeditor_id($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = php168_v7_count_fid($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = php168_v7_uc_config_UC_KEY($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = php168_v7_s_rpc_queryString($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = ecshop_27x_search_encode($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = ecshop_27x_alipay_order_sn($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = ecshop_27x_flow_goods_number($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = ecshop_27x_calendar_lang($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = ecshop_270_flow_package_info($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = ecshop_27x_category_filter_attr($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = ecshop_27x_category_filter_attr_xss($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = hdwiki_index_doc_summary($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = shopex_485_ctl_product($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = ckeditor_401_xss($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = fckeditor_upload($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = phpmyadmin_auth_bypass($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');
                $retval = fckeditor_upload_null_truncate($url, $referer, $this->{Proxy});
		push @result, $retval if ($$retval{'cms'} ne '');

		{lock $this->{report}->{issue};
		foreach my $el (@result)
		{
			$this->{report}->{issue}{$issue_id} = shared_clone({}) unless (defined($this->{report}->{issue}{$issue_id}));
			$this->{report}->{issue}{$issue_id}{$$el{'url'}} = shared_clone({request => $$el{'req'}});
			$this->{report}->{issue}{$issue_id}{$$el{'url'}}{response} = $$el{'res'};
			$this->{report}->{issue}{$issue_id}{$$el{'url'}}{analyze} = sprintf($this->{report}->{define}{issue_def}{issue}{$issue_id}{analyze}, $$el{'cms'}, $$el{'reason'}, $$el{'vul'}, $$el{'vulnum'});
			$data->{cms} = $$el{'cms'};
			print "<!-- $url found $$el{'cms'} CMS --!>\n";
		}
		}
	}
}

1;
