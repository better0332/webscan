#!/usr/bin/perl -w
use strict;
BEGIN
{
	my $path;
	if ($^O eq 'linux')
	{
		if ($0=~m/^(.+)\//) { $path = $1; } else { $path = readpipe('pwd'); chomp($path); }
		unshift(@INC, $path);
		chdir($path);
	}else
	{
		die("Please run this script on linux.\n");   
	}
}

use database;
use sendmail;

sub usage
{
	print $_[0] if (defined($_[0]));
	print "Usage: ";
	print "\t--id=[submit id]\n";
	exit(0);
}

sub upload_report
{
	my $report = shift;
	my $upload_url = "http://172.16.11.60/webscan/upload_report.php";
	my $upload_cmd = "curl --silent -F report=@".quotemeta($report)." ".quotemeta($upload_url);
	my $try = 5;
	while($try--)
	{
		my $report_url=readpipe($upload_cmd); chomp($report_url);
		return $report_url if($report_url ne '');
		sleep(5);
	}
}

sub send_report
{
	my ($recipient, $url, $report, $report_name) = @_;
	my $date = readpipe("date +'%Y-%m-%d'"); chomp($date);
	my $from = "HikVision Security Labs<no-reply\@hikvision.com.cn>";
	my $toname = "Customer";
	my $subject = "海康威视网站漏洞扫描报告:$url";
	my $body = readpipe("cat email.htm");
	$body=~s/\%URL\%/$url/g;
	$body=~s/\%DATE\%/$date/g;
	send_mail($from, $recipient, $toname, $subject, $body, $report, $report_name);
}

sub scan_task
{
	my $submit_id = shift;
	my $dbh = connect_db();
	my $sql = "select submitter, url, scan_depth, max_scan_time, proxy, subdomain, recipient, reportype from scan_task where submit_id=$submit_id";
	my $sth = my_query($dbh, $sql);
	my ($submitter, $url, $scan_depth, $max_scan_time, $proxy, $subdomain, $recipient, $reportype) = $sth->fetchrow_array();
	$sth->finish(); 
	if (defined($url))
	{
		my $report = '';
		my $status = 'failed';
		my $tempfile = readpipe("mktemp /tmp/rpt_XXXX"); chomp($tempfile);
		die "Can not create temp file.\n" if($tempfile eq '');
		$proxy = ($url=~m/^(http|https):\/\/[^\/:]\.tw/i || lc($proxy) eq 'yes')?'--proxy=172.16.11.60:8081':'';
		my $error_code = system("./webscan.pl --url=".quotemeta($url)." --subdomain=$subdomain --scan_depth=$scan_depth --max_scan_time=$max_scan_time $proxy --report=".quotemeta($tempfile)." --reportype=$reportype")>>8;
		if (!$error_code)
		{ #success
			$status = 'done';
			$report = substr(readpipe("md5sum ".quotemeta($tempfile)), 0, 32);
			system("mv ".quotemeta($tempfile)." report/$report.xml");
			upload_report("report/$report.xml");
			system("php pdf.php ".quotemeta("report/$report.xml")." ".quotemeta("report/$report.pdf"));
			upload_report("report/$report.pdf") if(-f "report/$report.pdf");
			if(-f "report/$report.pdf" && $submitter ne 'system')
			{
				send_report($recipient, $url, "report/$report.pdf", "ScanReport.pdf");
			}
		}
		unlink($tempfile);
		#$status = 'failed resolve' if ($error_code == 6);
		#$status = 'failed connect' if ($error_code == 7);
		$sql = "update scan_task set finish_time=now(), status='$status', report='$report' where submit_id=$submit_id";
		my_update($dbh, $sql);
	}
	$dbh->disconnect();
}

sub main
{
	my $submit_id;
	foreach my $param (@ARGV)
	{
		if ($param=~m/--id=(\d+)/)
		{
			$submit_id = $1;
		}else
		{
			usage("Unknown option: $param\n");
		}
	}
	usage() if (!defined($submit_id));
	scan_task($submit_id);
}

main;
