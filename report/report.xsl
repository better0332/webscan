<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
<style type="text/css">
body
{
	text-align: center;
}
#maindiv
{
	width: 650px;
	margin: 0 auto;
	text-align: left;
	font-size: 14px;
}
h1
{
	text-align: center;
	font-size: 22px;
}
h2
{
	font-size: 20px;
}
h3
{
	font-size: 18px;
	background-color: #b0b0b0;
}
h4
{
	font-size: 16px;
	background-color: #b0b0b0;
}
h5
{
	font-size: 14px;
	font-style: italic;
	background-color: #b0b0b0;
}
.thu
{
	font-size: 14px;
    font-style: italic;
    font-weight: bold;
	background-color: #b0b0b0;
	padding-left: 2px;
}
.th
{
	font-size: 14px;
	background-color: #b0b0b0;
	text-align: left;
	padding-left: 2px;
}
.item
{
	width: 120px;
	background-color: #d0d0d0;
	text-align: right;
	padding-right: 2px;
}
table
{
	width: 100%;
}
td
{
	background-color: #f0f0f0;
	text-align: left;
}

.bar
{
	background-color: #FF0000;
}

.bar_high
{
	background-color: red;
}

.bar_medium
{
	background-color: orange;
} 

.bar_low
{
	background-color: green;
}
pre
{
	word-wrap: break-word;
	word-break: break-all;
	white-space: normal;
}
pre
{
	white-space: pre-wrap;       / css-3 /
	white-space: -moz-pre-wrap;  / Mozilla, since 1999 /
	white-space: -pre-wrap;      / Opera 4-6 /
	white-space: -o-pre-wrap;    / Opera 7 /
}
</style>
<body>
<xsl:variable name="bar">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</xsl:variable>
<div id="maindiv">
	<h1><table border="0" ><tr><td style="background-color: #ffffff"><img src="logo.jpg"/></td><td style="background-color: #ffffff; text-align: left; font-size: 22px; font-weight:bolder"><xsl:value-of select="report/@title"/></td></tr></table></h1>
	<h2><font color="#000000"><xsl:value-of select="report/information/@title"/></font></h2>
	<h4><xsl:value-of select="report/information/scan_info/@title"/></h4>
	<table>
	<xsl:for-each select="report/information/scan_info/item">
		<tr><td class="item"><xsl:value-of select="@title"/></td><td><xsl:value-of select="."/></td></tr>
	</xsl:for-each>
	</table>
	<h4><xsl:value-of select="report/information/server_info/@title"/></h4>
	<table>
	<xsl:for-each select="report/information/server_info/item">
		<tr><td class="item"><xsl:value-of select="@title"/></td><td><xsl:value-of select="."/></td></tr>
	</xsl:for-each>
	</table>
	<h2><font color="#000000"><xsl:value-of select="report/summary/@title"/></font></h2>
	<h4><xsl:value-of select="report/summary/evaluation/@title"/></h4>
	<table>
  <tr>
   <td style="text-align: center; width:120px; background-color: #d0d0d0;"><xsl:value-of select="report/summary/evaluation/risk/@title"/></td>
   <td style="text-align: center; background-color: #d0d0d0;"><xsl:value-of select="report/summary/evaluation/description/@title"/></td>
  </tr>
  <tr>
   <td style="text-align: center;">
    <xsl:value-of select="report/summary/evaluation/risk/."/>
   </td>
   <td><xsl:value-of select="report/summary/evaluation/description"/></td>
  </tr>
	
	</table>
	<h4><xsl:value-of select="report/summary/risk_stat/@title"/></h4>
	<xsl:variable name="total_risk" select="sum(report/summary/risk_stat/item)"/>
	<table>
	<xsl:for-each select="report/summary/risk_stat/item">
		<tr><td class="item"><xsl:value-of select="@title"/></td>
		<xsl:choose> 
		<xsl:when test="@title='高风险'"> 
		<td><span class="bar_high"><xsl:value-of select="substring($bar,1,round(number(.) * string-length($bar) div $total_risk))"/></span><xsl:value-of select="."/></td>
		</xsl:when>
		<xsl:when test="@title='中风险'"> 
			<td><span class="bar_medium"><xsl:value-of select="substring($bar,1,round(number(.) * string-length($bar) div $total_risk))"/></span><xsl:value-of select="."/></td>
		</xsl:when> 
		<xsl:when test="@title='低风险'"> 
			<td><span class="bar_low"><xsl:value-of select="substring($bar,1,round(number(.) * string-length($bar) div $total_risk))"/></span><xsl:value-of select="."/></td>
		</xsl:when>
		</xsl:choose>
		</tr>
	</xsl:for-each>
	</table>
	<h4><xsl:value-of select="report/summary/issue_stat/@title"/></h4>
	<xsl:variable name="total_issue" select="sum(report/summary/issue_stat/item)"/>
	<table>
	<xsl:for-each select="report/summary/issue_stat/item">
		<tr><td class="item"><xsl:value-of select="@title"/></td>
		<xsl:choose> 
		<xsl:when test="@risk='高'"> 
		<td>
<span class="bar_high"><xsl:value-of select="substring($bar,1,round(number(.) * string-length($bar) div $total_risk))"/></span><xsl:value-of select="."/></td>
		</xsl:when>
		<xsl:when test="@risk='中'"> 
			<td><span class="bar_medium"><xsl:value-of select="substring($bar,1,round(number(.) * string-length($bar) div $total_risk))"/></span><xsl:value-of select="."/></td>
		</xsl:when> 
		<xsl:when test="@risk='低'"> 
			<td><span class="bar_low"><xsl:value-of select="substring($bar,1,round(number(.) * string-length($bar) div $total_risk))"/></span><xsl:value-of select="."/></td>
		</xsl:when>
		</xsl:choose>
		</tr>
	</xsl:for-each>
	</table>
	<h4></h4>

	<h4>网站漏洞清单</h4>	
	<xsl:for-each select="report/details/issue">
		<table>
		<tr class="th">
		<xsl:value-of select="@title"/>
		，风险级别：
		<xsl:for-each select="introduction/item">
		<xsl:choose> 
			<xsl:when test="@title='风险'">
				<xsl:value-of select="."/>
			</xsl:when>
		</xsl:choose>
		</xsl:for-each>
		</tr>
		<xsl:for-each select="affected_items/case">
			
			<tr><td><xsl:value-of select="url"/></td></tr>	
			
		</xsl:for-each>
		</table>
	</xsl:for-each> 
	
	<h2><font color="#000000"><xsl:value-of select="report/details/@title"/></font></h2>
	<xsl:for-each select="report/details/issue">
		<h3><xsl:value-of select="@title"/></h3>
		<h4><xsl:value-of select="introduction/@title"/></h4>
		<table>
		<xsl:for-each select="introduction/item">
			<tr><td class="item"><xsl:value-of select="@title"/></td><td>
			<xsl:value-of select="."/>
			</td></tr>
		</xsl:for-each>
		</table>
		<h4><xsl:value-of select="affected_items/@title"/></h4>
		<xsl:for-each select="affected_items/case">
			<table>
			<tr><td class="thu"><xsl:value-of select="url"/></td></tr>
			<tr><td class="th"><xsl:value-of select="request/@title"/></td></tr>
			<tr><td><pre><xsl:value-of select="request"/></pre></td></tr>
			<tr><td class="th"><xsl:value-of select="response/@title"/></td></tr>
			<tr><td><pre><xsl:value-of select="response"/></pre></td></tr>
			<tr><td class="th"><xsl:value-of select="analyze/@title"/></td></tr>
			<tr><td><pre><xsl:value-of select="analyze"/></pre></td></tr>
			</table>
		</xsl:for-each>
	</xsl:for-each>
</div>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
