package cms;
use strict;
use HTTPAgent;
use web;

require Exporter;
our $VERSION = 1.00;
our @ISA = qw(Exporter);
our @EXPORT = qw(check_cms);

sub check_cms
{
	my ($url, $referer, $proxy, $content_ref, $robots_ref) = @_;
	my $agent = new HTTPAgent(Proxy => $proxy);

	my ($cms, $version) = ('', '');
	if ($$content_ref=~/Powered\s+by.{0,128}?>(Discuz!\w*)<(?:.{0,64}?>([^<]+)<\/em>)?/is)
	{
		($cms, $version) = ($1, $2);
	}
	if ($$robots_ref=~/robots\.txt\s+for\s+(Discuz!\w*)/i && $1 eq $cms)
	{
		$version = $1 if ($version eq '' && ($$robots_ref=~/robots\.txt\s+for\s+Discuz!\w*[\f\t ]+(\S+)/i || $$robots_ref=~/#\s*Version[\f\t ]+(\S+)/i));
		return $cms.($version ? " $version" : "");
	}
	$agent->request('GET', $url.'source/admincp/discuzfiles.md5', $referer);
	return $cms.($version ? " $version" : "") if ($agent->{ResponseCode}==200);
}

1;
